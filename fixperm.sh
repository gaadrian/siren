#!/bin/bash

cd "$(dirname "$0")"

chmod +x bin/siren-*
chmod +x ibin/*
chmod +x libs/exabgp/sbin/exa*
chmod +x install.sh
chmod +x kill.sh