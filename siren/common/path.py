import os
import tempfile
import subprocess
import shutil


LOG_PREFIX = 'siren-log'
TEMP_PREFIX = 'siren'

def get_root_path(*parts):
    """Get projects root path

    :param parts: additional path parts
    :return: path
    """
    common_directory = os.path.dirname(os.path.realpath(__file__))
    root_dir = os.path.join(common_directory, '../../')
    return os.path.realpath(os.path.join(root_dir, *parts))


def get_home_path(*parts):
    """Get user's home path

    :param parts: additional path parts
    :return: path
    """
    root_dir = os.path.expanduser('~')
    return os.path.realpath(os.path.join(root_dir, *parts))


def get_bin_path(cmd, args=None):
    """Get the command in bin path

    :param cmd: command
    :param args: arguments of command
    :return: path
    """
    if args is None:
        arguments = ''
    else:
        arguments = ' ' + ' '.join(args)

    return get_root_path('bin', cmd) + arguments


def get_internal_bin_path(cmd, args=None):
    """Get the command in bin path

    :param cmd: command
    :param args: arguments of command
    :return: path
    """
    if args is None:
        arguments = ''
    else:
        arguments = ' ' + ' '.join(args)

    return get_root_path('ibin', cmd) + arguments



def get_package_path(*parts):
    """Get path inside the package

    :param parts: additional path parts
    :return: path
    """
    return get_root_path('siren', *parts)


def get_log_root_path():
    """Get log root path

    :return: path
    """
    return get_home_path(LOG_PREFIX)


def get_log_path(setup, config, run, group, experiment=None, experiment_run=0, filename=None):

    """Get path of a particular log file

    :param setup: setup name
    :param config: configuration string
    :param run: run name
    :param group: group name
    :param experiment: experiment name
    :param experiment_run: number of experiment runs
    :param filename: file name
    :return: path
    """
    if experiment is None:
        if filename is None:
            filename = 'init'
        path = get_home_path(LOG_PREFIX, setup, group, config, run, '%s.log' % (filename,))

    else:
        path = get_home_path(LOG_PREFIX, setup, group, config, run, experiment, '%d.log' % (experiment_run,))
    # create dirs
    create_path(path, is_file=True)

    return path


def get_libs_path(*parts):
    """Get path to external libraries

    :param parts: additional path parts
    :return: path
    """
    return get_root_path('libs', *parts)


def get_data_path(*parts):
    """Get path to data directory

    :param parts: additional path parts
    :return: path
    """
    return get_root_path('data', *parts)


def create_path(path, is_file=False):
    """Create all directories of the provided path

    :param path: path
    :param is_file: True if the path is a file path and not a directory path
    :type is_file bool
    """
    if is_file:
        path = os.path.realpath(os.path.join(path, '..'))

    subprocess.call(['mkdir', '-p', path])


def get_temp_path(*parts, **kwargs):
    """Get path in the temporary file system

    :param parts: additional path parts
    :param kwargs: is_file (default: False): indicate whether the path points to a file, create (default; True):
                    indicate whether you want to create the dictionary structure, trailing_slash (default; True): append a slash
                    at the end.
    :return: path
    """
    is_file = kwargs.setdefault('is_file', False)
    create_tmp_path = kwargs.setdefault('create', True)
    if not is_file:
        trailing_slash = kwargs.setdefault('trailing_slash', True)
    else:
        trailing_slash = False

    temp = tempfile.gettempdir()
    path = os.path.realpath(os.path.join(temp, TEMP_PREFIX, *parts))
    if create_tmp_path:
        create_path(path, is_file=is_file)
    if trailing_slash:
        path += '/'
    return path


def clean_temp():
    """Clean/Remove main temp directory
    """
    path = get_temp_path()
    shutil.rmtree(path)
