import pickle
import networkx as nx

from siren.eventcollector.events import ASEvent, BGPSourceEvent, PeeringEvent, BGPPeeringEvent, FullEventContext, BGPUpdateEvent, SDNUpdateEvent, BGPUpdateCaptureEvent, LinkChangeEvent, ErrorEvent, SDNTopologyChangeEvent, MarkerEvent


class EventReader(object):
    def __init__(self):
        self.first_time = None

    def process_events(self, path, additional_readers=None, context=None):
        previous_time = -1
        if context is None:
            context = FullEventContext()
        with open(path, 'r') as fh:
            pickler = pickle.Unpickler(fh)
            while True:
                try:
                    event = pickler.load()
                    event.set_context(context)

                    if self.first_time is None:
                        self.first_time = event.get_time()

                    if previous_time > event.get_time():
                        pass  # FIXME
                        #print "asdfasdfdsaffffffdddddddddddddddd",previous_time, event.get_time()
                        #raise Exception('Events need to be sorted, but they are not :(')

                    self.process_event(event)
                    if not additional_readers is None:
                        for additional_reader in additional_readers:
                            additional_reader.process_event(event)
                    previous_time = event.get_time()
                except EOFError:
                    break

    def process_event(self, event):
        raise NotImplementedError()


class NetworkReader(EventReader):
    def __init__(self):
        EventReader.__init__(self)
        self.network = nx.Graph()

    def process_event(self, event):
        if isinstance(event, PeeringEvent):
            skip = False
            if isinstance(event, BGPPeeringEvent):
                if event.monitor:
                    skip = True
            if not skip:
                self.network.add_edge(event.get_peer_a_name(), event.get_peer_b_name())

    def get_network(self):
        return self.network


class ASEventReader(EventReader):
    def __init__(self):
        EventReader.__init__(self)
        self.as_numbers = set()

    def process_event(self, event):
        if isinstance(event, ASEvent):
            self.as_numbers.add(event.as_number)

    def get_as_numbers(self):
        return self.as_numbers


class BGPSourceEventReader(EventReader):
    def __init__(self):
        EventReader.__init__(self)
        self.sources = {}

    def process_event(self, event):
        if isinstance(event, BGPSourceEvent):
            self.sources.setdefault(event.as_number, []).append(event.neighbor_ip)

    def get_sources(self):
        return self.sources


class ConvergenceReader(EventReader):
    TYPE_RCPS = 1  #: signaling
    TYPE_RCPSC = 2  #: state change

    def __init__(self, prefix):
        EventReader.__init__(self)
        self.last_rcps = -1
        self.last_rcpsc = -1

        self.prefix = prefix
        self.error = False
        self.start = None
        self.end = None

    def process_event(self, event):
        if self.start is not None and self.start <= event.get_time() and (self.end is None or event.get_time() <= self.end):
            if isinstance(event, BGPUpdateEvent) or isinstance(event, SDNUpdateEvent):
                if self.last_rcpsc < event.get_time() and (self.prefix is None or self.prefix == event.network):
                    self.last_rcpsc = event.get_time()

            if isinstance(event, BGPUpdateCaptureEvent):
                if self.last_rcps < event.get_time() and (self.prefix is None or self.prefix == event.network):
                    self.last_rcps = event.get_time()
                    #print self.last_rcps

        if isinstance(event, MarkerEvent):
            if event.name == 'convergence_start':
                #print event.get_time()
                self.start = event.get_time()
            elif event.name == 'convergence_end':
                self.end = event.get_time()


        if isinstance(event, ErrorEvent) or isinstance(event, SDNTopologyChangeEvent):
            self.error = True
            print 'ERROR', type(event)

    def get_error(self):
        return self.error

    def get_diff(self):
        if self.get_start() is None:
            return None

        if self.get_last() < 0:
            return 0
        #print self.get_last(), self.get_start()
        return self.get_last() - self.get_start()

    def get_start(self):
        return self.start

    def get_last(self, last_type=None):
        return max(self.last_rcpsc, self.last_rcps)  #, self.last_rcps)


class ChurnRateReader(EventReader):
    TYPE_RCPS = 1  #: signaling
    TYPE_RCPSC = 2  #: state change

    def __init__(self, prefix):
        EventReader.__init__(self)
        self.last_rcps = -1
        self.last_rcpsc = -1

        self.prefix = prefix
        self.error = False
        self.counter = 0
        self.already_seen_events = set([])
        self.start = None
        self.end = None

    def process_event(self, event):

        if self.start is not None and self.start <= event.get_time() and (self.end is None or event.get_time() <= self.end):
            if isinstance(event, BGPUpdateEvent) or isinstance(event, SDNUpdateEvent):
                if self.last_rcpsc < event.get_time() and (self.prefix is None or self.prefix == event.network):
                    self.last_rcpsc = event.get_time()

            if isinstance(event, BGPUpdateCaptureEvent):
                if self.last_rcps < event.get_time() and (self.prefix is None or self.prefix == event.network):
                    self.last_rcps = event.get_time()
                    #print self.last_rcps
                event_str_id = str([str(event.src_ip), str(event.dst_ip), str(event.network),
                               str(event.action), str(event.as_sequence), str(event.as_set)])
                #avoid duplicates
                if (event_str_id not in self.already_seen_events):
                    self.counter += 1
                    self.already_seen_events.add(event_str_id)
                else:
                    self.already_seen_events.remove(event_str_id)

        if isinstance(event, MarkerEvent):
            if event.name == 'convergence_start':
                #print event.get_time()
                self.start = event.get_time()
            elif event.name == 'convergence_end':
                self.end = event.get_time()


        if isinstance(event, ErrorEvent) or isinstance(event, SDNTopologyChangeEvent):
            self.error = True
            print 'ERROR', type(event)

    def get_error(self):
        return self.error

    def get_diff(self):
        if self.get_start() is None:
            return None

        if self.get_last() < 0:
            return 0
        #print self.get_last(), self.get_start()
        return self.get_last() - self.get_start()

    def get_start(self):
        return self.start

    def get_last(self, last_type=None):
        return max(self.last_rcpsc, self.last_rcps)  #, self.last_rcps)

    def get_churn_rate(self):
        diff = self.get_diff()
        if (diff is None): return None
        if (diff==0): return None
        return float(self.counter)/float(diff)