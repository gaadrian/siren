import os
import copy

from siren.common.path import get_log_root_path


class FileManager(object):
    def __init__(self):
        self.selectors = []

    def add_selector(self, selector):
        self.selectors.append(selector)

    def get_paths(self):
        result_paths = []

        log_path = get_log_root_path()
        for path in os.walk(log_path):
            # only dirs where are files
            if len(path[2]) > 0:
                inner_path = path[0][len(log_path) + 1:]
                path_parts = inner_path.split('/')
                if len(path_parts) < 4:
                    continue

                file_path = LogPath()
                file_path.setup = path_parts[0]
                file_path.group = path_parts[1]
                file_path.config = path_parts[2]
                file_path.run = path_parts[3]

                if len(path_parts) == 5:
                    file_path.experiment = path_parts[4]

                for fn in path[2]:
                    #tmp_file_path = copy.deepcopy(file_path)
                    file_path.absolute = os.path.join(path[0], fn)
                    file_path.filename = fn
                    not_all = False
                    for selector in self.selectors:
                        if not selector.select(file_path):
                            not_all = True
                            break
                    if not not_all:
                        final_path = os.path.join(path[0], fn)
                        yield file_path
                        #reader.process_events(final_path)


class LogPath(object):
    def __init__(self, setup=None, group=None, config=None, run=None, experiment=None, filename=None, absolute=None):
        self.setup = setup
        self.group = group
        self.config = config
        self.run = run
        self.experiment = experiment
        self.filename = filename
        self.absolute = absolute


class FileSelector(object):
    def select(self, path):
        raise NotImplementedError()


class PartSelector(FileSelector):
    def __init__(self, part):
        self.part = part

    def select(self, path):
        return self.get_part(path) == self.part

    def get_part(self, path):
        raise NotImplementedError()


class SetupSelector(PartSelector):
    def get_part(self, path):
        return path.setup


class GroupSelector(PartSelector):
    def get_part(self, path):
        return path.group


class RunSelector(PartSelector):
    def get_part(self, path):
        return path.run


class ConfigSelector(PartSelector):
    def get_part(self, path):
        return path.config


class ConfigParameterSelector(FileSelector):
    def __init__(self, configstr):
        self.confstr = configstr

    def select(self, path):
        return self.confstr in path.config  # FIXME splitting irgendwo vereinheitlichen


class ExperimentSelector(PartSelector):
    def get_part(self, path):
        return path.experiment
