import pickle
import subprocess

from siren.common.path import get_temp_path, get_internal_bin_path
from siren.eventcollector.events import BGPStateEvent, BGPUpdateEvent, LinkChangeEvent


def get_event_collector_path():
    """ Returns event collector unix socket path
    :return: path
    """
    return get_temp_path('event', 'collector', 'collector.sock', is_file=True)


class PickleLog(object):
    def __init__(self, path, mode):
        self.fh = open(path, mode)
        self.pickle_log = pickle.Pickler(self.fh, 2)

    def __enter__(self):
        return self

    def close(self):
        self.fh.close()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()


def start_collect():
    """Start event collector
    """
    event_process = get_internal_bin_path('eventcollector')
    subprocess.Popen([event_process])


def start_livevis():
    """Start livevis server
    """
    livevis_process = get_internal_bin_path('livevis')
    subprocess.Popen([livevis_process])