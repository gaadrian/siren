import socket
import time
from tornado import websocket
import tornado.ioloop
import tornado.web
import zmq
from zmq.eventloop import ioloop, zmqstream

from siren.eventcollector.events import PeeringEvent, RouterEvent, HostEvent, BGPUpdateCaptureEvent, BGPUpdateEvent, SDNUpdateEvent, ASEvent, ClusterEvent
from siren.common.path import get_temp_path


def get_livevis_event_path():
    return get_temp_path('livevis', 'events.sock', is_file=True)


def get_livevis_cli_path():
    return get_temp_path('livevis', 'cli.sock', is_file=True)


class LiveProcess(object):
    def __init__(self):
        self.as_to_cluster = {}

    def process_event(self, event):
        if isinstance(event, ClusterEvent):
            return 'add_cluster(\'cluster%d\', \'%s\')' % (event.cluster_id, event.cluster_type)

        if isinstance(event, ASEvent):
            self.as_to_cluster[event.as_number] = event.cluster_id
            return None

        if isinstance(event, BGPUpdateEvent) or isinstance(event, SDNUpdateEvent) or isinstance(event, BGPUpdateCaptureEvent):
            return 'update_convergence();'

        if isinstance(event, PeeringEvent):
            print 'addlink'
            return "add_link('r%d.%d','r%d.%d','%s','topo');" % (event.peer_a_as, event.peer_a_router,
                                                                 event.peer_b_as, event.peer_b_router,
                                                                 event.relationship)

        if isinstance(event, RouterEvent):
            # TODO add cluster support
            try:
                cluster = 'cluster%d' % (self.as_to_cluster[event.as_number],)
            except:
                cluster = 'unknowncluster'

            print 'addnode'
            return "add_node('r%d.%d', '%s', 'AS%d');" % (event.as_number, event.router_id, cluster, event.as_number)

        if isinstance(event, HostEvent):
            print 'addnode, host'
            node = "add_node('h%d.%d', '%s','');" % (event.as_number, event.host_id, 'host')
            link = "add_link('r%d.%d','h%d.%d','', 'topo');" % (event.as_number, event.router_id, event.as_number, event.host_id)
            return '%s\n%s' % (node, link)

        return None


class LiveVisualization(websocket.WebSocketHandler):
    stream_pull = None

    def __init__(self, a, b):

        LiveVisualization.stream_pull.on_recv(self.events_parse)
        super(LiveVisualization, self).__init__(a, b)

        context = zmq.Context()
        self.cli = context.socket(zmq.REQ)

    def events_parse(self, msg):
        self.write_message('%s\n' % (msg[0],))
        self.cli.connect('ipc://%s' % (get_livevis_cli_path(),))

    def check_origin(self, origin):
        # accept all cross-origin traffic
        # http://tornado.readthedocs.org/en/latest/_modules/tornado/websocket.html#WebSocketHandler.check_origin
        return True

    def open(self):
        print "WebSocket opened"

    def on_message(self, message):
        print 'Received:', message
        try:
            self.cli.send(str(message))
            print self.cli.recv()
        except:
            import sys
            print 'ERROR', sys.exc_info()

    def on_close(self):
        print "WebSocket closed"


def launch():
    print 'Livevis server started'
    print 'IMPORTANT: this server is not secure, please only use it in protected areas'
    print ''
    ioloop.install()

    application = tornado.web.Application([
        (r"/livevis", LiveVisualization),
        ])

    context = zmq.Context()
    socket_pull = context.socket(zmq.PULL)
    socket_pull.bind('ipc://%s' % (get_livevis_event_path(),))
    LiveVisualization.stream_pull = zmqstream.ZMQStream(socket_pull)
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
