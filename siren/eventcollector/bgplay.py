import json
from netaddr import IPNetwork
from twisted.web import server, resource, static
from twisted.internet import reactor
import webbrowser

from siren.common.path import get_libs_path
from siren.eventcollector.events import BGPUpdateEvent
from siren.eventcollector.reader import EventReader, BGPSourceEventReader, ASEventReader

TIMESTAMP_FORMAT = '%.3f'


class InitialBGPlayReader(EventReader):
    def __init__(self, path, network_resource):
        EventReader.__init__(self)
        if network_resource is None:
            self.resource = None
        else:
            self.resource = IPNetwork(network_resource)
        self.updates = {}
        self.targets = set()

        self.source_reader = BGPSourceEventReader()
        self.as_reader = ASEventReader()

        self.process_events(path, additional_readers=[self.source_reader, self.as_reader])

    def process_event(self, event):
        if isinstance(event, BGPUpdateEvent):
            if self.resource is None or self.resource in IPNetwork(event.network):
                if event.action == BGPUpdateEvent.ACTION_ANNOUNCE:
                    # Assumption: no overlapping prefixes
                    self.updates[event.neighbor_ip] = event
                elif event.action == BGPUpdateEvent.ACTION_WITHDRAW:
                    del self.updates[event.neighbor_ip]
                else:
                    raise AssertionError('Unknown action')
                self.targets.add(event.network)

    def get_initial_data(self):
        paths = []
        for event in self.updates.itervalues():
                event = {'community': [],
                         'path': event.as_sequence,
                         'source_id': event.neighbor_ip,
                         'target_prefix': event.network}
                paths.append(event)
        return paths

    def get_sources(self):
        sources_data = []
        for as_number, neighbor_ips in self.source_reader.get_sources().iteritems():
            for neighbor_ip in neighbor_ips:
                source_data = {'as_number': as_number,
                               'id': neighbor_ip,
                               'ip': neighbor_ip,
                               'rrc': '01'}
                sources_data.append(source_data)
        return sources_data

    def get_as_numbers(self):
        as_numbers_data = []
        for as_number in self.as_reader.get_as_numbers():
            as_number_data = {'as_number': as_number,
                              'owner': 'AS%d' % (as_number,)}
            as_numbers_data.append(as_number_data)
        return as_numbers_data

    def get_targets(self):
        return self.targets


class ExperimentBGPlayReader(EventReader):
    def __init__(self, path, network_resource):
        EventReader.__init__(self)
        if network_resource is None:
            self.resource = None
        else:
            self.resource = IPNetwork(network_resource)
        self.updates = []
        self.update_timestamps = []
        self.targets = set()

        self.process_events(path)

    def process_event(self, event):
        if isinstance(event, BGPUpdateEvent):
            if self.resource is None or self.resource in IPNetwork(event.network):
                update = {'attrs': {'community': [],
                                    'path': event.as_sequence,
                                    'source_id': event.neighbor_ip,
                                    'target_prefix': event.network},
                          'timestamp': TIMESTAMP_FORMAT % (event.time,),
                          'type': event.action[:1].upper()}
                self.updates.append(update)
                self.update_timestamps.append(event.time)
                self.targets.add(event.network)

    def get_events(self):
        return self.updates

    def get_min_max_timestamps(self):
        if len(self.update_timestamps) == 0:
            return 0, 0

        return min(self.update_timestamps), max(self.update_timestamps)

    def get_targets(self):
        return self.targets


class BGPlayData(object):
    def __init__(self, network_resource, init_path, exp_path):
        initial_reader = InitialBGPlayReader(init_path, network_resource)
        experiment_reader = ExperimentBGPlayReader(exp_path, network_resource)
        query_starttime, query_endtime = experiment_reader.get_min_max_timestamps()
        targets = initial_reader.get_targets() | experiment_reader.get_targets()

        targets_data = []
        for target in targets:
            target_data = {'prefix': target}
            targets_data.append(target_data)

        self.data = {'events': experiment_reader.get_events(),
                     'sources': initial_reader.get_sources(),
                     'nodes': initial_reader.get_as_numbers(),
                     'resource': network_resource,
                     'targets': targets_data,
                     'initial_state': initial_reader.get_initial_data(),
                     'query_starttime': query_starttime,
                     'query_endtime': query_endtime}

    def get_json(self):
        return {'data': self.data}


# adapted from https://twistedmatrix.com/documents/12.3.0/web/howto/using-twistedweb.html
class BGPlayDataResource(resource.Resource):
    isLeaf = True

    def __init__(self, init_path, exp_path):
        resource.Resource.__init__(self)
        self.init_path = init_path
        self.exp_path = exp_path

    def render_GET(self, request):
        # see http://twistedmatrix.com/documents/13.0.0/api/twisted.web.http.Request.html

        requested_resource = request.args.setdefault('resource', None)
        if not requested_resource is None:
            requested_resource = requested_resource[0]

        try:
            callback = request.args['callback'][0]
        except KeyError:
            request.setResponseCode(400)
            return 'Not all parameters set'

        bgplay_data = BGPlayData(requested_resource, self.init_path, self.exp_path)
        json_data = bgplay_data.get_json()

        request.setHeader('Content-Type', 'text/javascript')
        return '%s(%s);' % (callback, json.dumps(json_data))


def launch_webserver(init_path, exp_path):
    root = static.File(get_libs_path('BGPlay.js'))
    site = server.Site(root)
    root.putChild('data', BGPlayDataResource(init_path, exp_path))
    reactor.listenTCP(8080, site)
    webbrowser.open('http://localhost:8080/run_bgplay.html')
    reactor.run()