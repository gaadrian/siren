import zmq

from siren.eventcollector import get_event_collector_path


class EventClient(object):
    def __init__(self):
        path = get_event_collector_path()
        context = zmq.Context()
        self.sock = context.socket(zmq.PUSH)
        self.sock.connect('ipc://%s' % (path,))

    def send(self, event):
        """Send event to event collector

        :param event: event to send to collector
        """
        self.sock.send_pyobj(event)