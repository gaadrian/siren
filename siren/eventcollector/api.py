import time
import zmq

from siren.common.path import get_temp_path
from siren.eventcollector.events import BGPUpdateEvent, SDNUpdateEvent, BGPUpdateCaptureEvent


def get_event_api_path():
    """Returns Event API unix socket path
    :return: path
    """
    return get_temp_path('event', 'collector', 'api.sock', is_file=True)


class EventAPIClient(object):
    """Client class to access Event API

    """
    def __init__(self):
        context = zmq.Context()
        self.sock = context.socket(zmq.REQ)
        path = get_event_api_path()
        self.sock.connect('ipc://%s' % (path,))

    def _send_cmd(self, command):
        """Send command to server

        :param command: command to send to server
        :return: response to command
        """
        self.sock.send_string(command)
        return self.sock.recv_pyobj()

    def _last_rcp(self, command):
        last_change = self._send_cmd(command)
        if last_change < 0:
            return -1, -1
        return last_change, time.time() - last_change

    def last_rcp_state_change(self):
        """Get last RCP state change timestamp

        :return: tuple with timestamp of last change and difference to current time
        """
        return self._last_rcp(EventServerAPI.CMD_LAST_RCP_STATE_CHANGE)

    def last_rcp_signaling(self):
        """Get last RCP signaling timestamp

        :return: tuple with timestamp of last change and difference to current time
        """
        return self._last_rcp(EventServerAPI.CMD_LAST_RCP_SIGNALING)

    def noop(self):
        """Send 'do-nothing' command

        :return: True
        """
        return self._send_cmd(EventServerAPI.CMD_NOOP)


class EventServerAPI(object):
    CMD_LAST_RCP_SIGNALING = 'last-routing-control-plane-signaling'
    CMD_LAST_RCP_STATE_CHANGE = 'last-routing-control-plane-state-change'
    CMD_NOOP = 'noop'

    def __init__(self):
        self.last_rcp_signaling = -1
        self.last_rcp_state_change = -1

    def process_event(self, event):
        """Process event for API

        :param event: event to process
        """
        if isinstance(event, BGPUpdateEvent) or isinstance(event, SDNUpdateEvent):
            if event.get_time() > self.last_rcp_state_change:
                self.last_rcp_state_change = event.get_time()
        if isinstance(event, BGPUpdateCaptureEvent):
            if event.get_time() > self.last_rcp_signaling:
                self.last_rcp_signaling = event.get_time()

    def response(self, cmd):
        """Returns response to a API command

        :param cmd: command for API
        :return: response of API command
        """
        if cmd == EventServerAPI.CMD_LAST_RCP_STATE_CHANGE:
            return self.last_rcp_state_change
        elif cmd == EventServerAPI.CMD_LAST_RCP_SIGNALING:
            return self.last_rcp_signaling
        elif cmd == EventServerAPI.CMD_NOOP:
            return True
