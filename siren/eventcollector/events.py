import time
import uuid


class PassEventContext(object):
    """Empty context class.
    """
    def process(self, event):
        """Process event to create and modify the context

        :param event: Event to process
        """
        pass

    def i2n(self, ip):
        """ Returns name which corresponds to IP

        :param ip: IP
        """
        pass

    def d2n(self, dpid):
        """Returns name which corresponds to DPID

        :param dpid: DPID
        """
        pass


class FullEventContext(PassEventContext):
    """Create a full context which tries to save all IP to name and DPID to name translations based on previous events
    """
    def __init__(self):
        super(FullEventContext, self).__init__()

        self.ip_addresses = {}
        self.dpids = {}

    def process(self, event):
        if isinstance(event, IPHostnameMatchingEvent):
            self.ip_addresses[event.ip] = event.hostname
        elif isinstance(event, DpidHostnameMatchingEvent):
            self.dpids[event.dpid] = event.hostname

    def i2n(self, ip):
        return self.ip_addresses.get(ip, ip)

    def d2n(self, dpid):
        return self.dpids.get(dpid, dpid)


class EventAbstract(object):

    def __init__(self, **params):
        self.uid = str(uuid.uuid4())
        self.time = time.time()
        self.notes = params.get('notes', None)
        self.context = None

    def get_title(self):
        """Returns a title of the log entry

        :raise NotImplementedError:
        """
        raise NotImplementedError()

    def get_detail(self):
        """Get a more detailed description of the log entry


        :return: detailed description
        """
        lines = []
        for name, value in self.__dict__.iteritems():
            lines.append('%s: %s' % (name, value))

        return '\n'.join(lines)

    def get_time(self):
        return self.time

    def get_timestamp(self, **params):
        event_time = self.time
        if not params.get('time_correction', None) is None:
            event_time -= params['time_correction']
        time_tuple = time.gmtime(event_time)
        return '%s.%03d' % (time.strftime('%H:%M:%S', time_tuple), (event_time % 1) * 1000)

    def get_uid(self, **params):
        return self.uid.split('-')[-1]

    def get_line(self, **params):
        """Get a single line about the log entry

        :param params: timestamp parameter
        :return: a line about the log entry
        """
        return '%s %s %s: %s' % (self.get_timestamp(**params), self.get_uid(), self.__class__.__name__, self.get_title())

    def set_context(self, context):
        """
        Do not call this method before you already saved the event, as we do not want to save the context.

        Set the context to this event.

        :param context: context to use
        :type context: PassEventContext
        """

        self.context = context
        self.context.process(self)

    def get_context(self):
        if self.context is None:
            return PassEventContext()
        else:
            return self.context


class BGPUpdateCaptureEvent(EventAbstract):
    ACTION_ANNOUNCE = 'announce'
    ACTION_WITHDRAW = 'withdraw'

    def __init__(self, src_ip=None, dst_ip=None, network=None, action=None, as_sequence=None, as_set=None, **params):
        super(BGPUpdateCaptureEvent, self).__init__(**params)

        self.src_ip = src_ip
        self.dst_ip = dst_ip
        self.network = network
        self.action = action
        self.as_sequence = as_sequence
        self.as_set = as_set

    def get_title(self):
        if self.action == BGPUpdateEvent.ACTION_ANNOUNCE:
            sequence = ' '.join([str(_) for _ in self.as_sequence])
        else:
            sequence = ''

        return '%s -> %s: %s %s Path: %s' % (
            self.get_context().i2n(self.src_ip), self.get_context().i2n(self.dst_ip), self.action, self.network, sequence)


class UpdateEventAbstract(EventAbstract):

    ACTION_ANNOUNCE = 'announce'
    ACTION_WITHDRAW = 'withdraw'

    def __init__(self, network=None, action=None, as_sequence=None, as_set=None, **params):
        super(UpdateEventAbstract, self).__init__(**params)
        self.network = network
        self.action = action
        self.as_sequence = as_sequence
        self.as_set = as_set

    def get_title(self):
        if isinstance(self.as_sequence, list):
            sequence = ' '.join([str(_) for _ in self.as_sequence])
        else:
            sequence = ''

        return '%s %s Path: %s' % (self.action, self.network, sequence)


class BGPUpdateEvent(UpdateEventAbstract):

    def __init__(self, neighbor_ip=None, **params):
        super(BGPUpdateEvent, self).__init__(**params)
        self.neighbor_ip = neighbor_ip

    def get_title(self):
        return '%s:  %s' % (self.get_context().i2n(self.neighbor_ip), super(BGPUpdateEvent, self).get_title())


class SDNUpdateEvent(UpdateEventAbstract):

    def __init__(self, dpid=None,  **params):
        super(SDNUpdateEvent, self).__init__(**params)
        self.dpid = dpid

    def get_title(self):
        return 'Switch %s:  %s' % (self.get_context().d2n(self.dpid), super(SDNUpdateEvent, self).get_title())


class BGPStateEvent(EventAbstract):
    def __init__(self, neighbor_ip=None, state=None, **params):
        super(BGPStateEvent, self).__init__(**params)

        self.neighbor_ip = neighbor_ip
        self.state = state

    def get_title(self):
        return '%s: %s' % (self.get_context().i2n(self.neighbor_ip), self.state)


class BGPSourceEvent(EventAbstract):
    def __init__(self, neighbor_ip=None, as_number=None, **params):
        super(BGPSourceEvent, self).__init__(**params)
        self.neighbor_ip = neighbor_ip
        self.as_number = as_number

    def get_title(self):
        return 'BGP monitoring source added (%s, AS%d)' % (self.neighbor_ip, self.as_number)


class RouterEvent(EventAbstract):
    def __init__(self, as_number=None, router_id=None, **params):
        super(RouterEvent, self).__init__(**params)
        self.as_number = as_number
        self.router_id = router_id

    def get_title(self):
        return 'new router r%d.%d' % (self.as_number, self.router_id)


class HostEvent(EventAbstract):
    def __init__(self, as_number=None, host_id=None, router_id=None, **params):
        super(HostEvent, self).__init__(**params)
        self.as_number = as_number
        self.host_id = host_id
        self.router_id = router_id

    def get_title(self):
        return 'new host h%d.%d' % (self.as_number, self.host_id)


class ASEvent(EventAbstract):

    def __init__(self, as_number=None, cluster_id=None, **params):
        super(ASEvent, self).__init__(**params)
        self.as_number = as_number
        self.cluster_id = cluster_id

    def get_title(self):
        return 'AS%d added' % (self.as_number,)


class ClusterEvent(EventAbstract):
    CLUSTER_TYPE_SDN = 'sdn'
    CLUSTER_TYPE_BGP = 'bgp'
    CLUSTER_TYPE_MONITOR = 'monitor'

    def __init__(self, cluster_id=None, cluster_type=None, **params):
        super(ClusterEvent, self).__init__(**params)
        self.cluster_id = cluster_id
        self.cluster_type = cluster_type

    def get_title(self):
        return 'Cluster %d [%s] added' % (self.cluster_id, self.cluster_type)


class PeeringEvent(EventAbstract):
    def __init__(self, peer_a_as=None, peer_a_router=None, peer_b_as=None, peer_b_router=None, delay=None, relationship=None, **params):
        super(PeeringEvent, self).__init__(**params)
        self.peer_a_as = peer_a_as
        self.peer_a_router = peer_a_router
        self.peer_b_as = peer_b_as
        self.peer_b_router = peer_b_router
        self.delay = delay
        self.relationship = relationship

    def get_title(self):
        return '%s <-> %s, %s' % (self.get_peer_a_name(), self.get_peer_b_name(), self.get_relationship())

    def _get_peer_name(self, as_number, router):
        return 'AS%d.%d' % (as_number, router)

    def get_peer_a_name(self):
        return self._get_peer_name(self.peer_a_as, self.peer_a_router)

    def get_peer_b_name(self):
        return self._get_peer_name(self.peer_b_as, self.peer_b_router)

    def get_relationship(self):
        if (self.relationship is None) or (self.relationship=='uc'):
            return 'unconstrained'
        elif (self.relationship=='c2p'):
            return 'customer-to-provider'
        elif (self.relationship=='p2c'):
            return 'provider-to-customer'
        elif (self.relationship=='p2p'):
            return 'peer-to-peer'
        return None


class BGPPeeringEvent(PeeringEvent):
    def __init__(self, peer_a_ip=None, peer_b_ip=None, monitor=None, **params):
        super(BGPPeeringEvent, self).__init__(**params)
        self.peer_a_ip = peer_a_ip
        self.peer_b_ip = peer_b_ip
        self.monitor = monitor


class SDNPeeringEvent(PeeringEvent):
    pass

# FIXME: ip to host event


class NetworkSetupEvent(EventAbstract):
    def __init__(self, setup_name=None, **params):
        super(NetworkSetupEvent, self).__init__(**params)
        self.setup_name = setup_name
        # FIXME add SHA1

    def get_title(self):
        return 'Loaded setup: %s' % (self.setup_name,)


class LinkChangeEvent(EventAbstract):
    def __init__(self, interface_a=None, interface_b=None, action=None, **params):
        super(LinkChangeEvent, self).__init__(**params)

        self.interface_a = interface_a
        self.interface_b = interface_b
        self.action = action

    def get_title(self):
        return '%s %s %s' % (self.interface_a, self.interface_b, self.action)


class MeasureStartEvent(EventAbstract):
    def __init__(self, source=None, target=None, **params):
        super(MeasureStartEvent, self).__init__(**params)

        self.source = source
        self.target = target

    def get_title(self):
        return 'Measurement started: %s -> %s' % (self.source, self.target)


class MeasureResultEvent(EventAbstract):
    def __init__(self, source=None, source_name=None, target=None, target_name=None, start_event=None, start_time=None,
                 end_time=None, packets_sent=None, packets_success=None, packets_discarded_buffer=None,
                 packets_lost=None, send_interval=None, buffer_time=None, loss_start=None, loss_end=None, **params):
        super(MeasureResultEvent, self).__init__(**params)

        self.source = source
        self.target = target
        self.start_event = start_event
        self.start_time = start_time
        self.end_time = end_time
        self.packets_sent = packets_sent
        self.packets_discarded_buffer = packets_discarded_buffer
        self.packets_lost = packets_lost
        self.send_interval = send_interval
        self.buffer_time = buffer_time
        self.loss_start = loss_start
        self.loss_end = loss_end

    def get_packets_error(self):
        return self.packets_discarded_buffer + self.packets_lost

    def get_packets_success(self):
        return self.packets_sent - self.get_packets_error()

    def get_loss_time(self):
        if not self.loss_start is None and not self.loss_end is None:
            return self.loss_end - self.loss_start
        else:
            return None

    def get_title(self):
        return 'Measurement result: %s -> %s,  %d/%d, err %d [lost %d, buffer %d], %s loss' % (
            self.source, self.target, self.get_packets_success(), self.packets_sent, self.get_packets_error(),
            self.packets_lost, self.packets_discarded_buffer, self.get_loss_time())


class IPHostnameMatchingEvent(EventAbstract):
    def __init__(self, ip=None, hostname=None, **params):
        super(IPHostnameMatchingEvent, self).__init__(**params)

        self.ip = ip
        self.hostname = hostname

    def get_title(self):
        return '%s == %s' % (self.ip, self.hostname)


class DpidHostnameMatchingEvent(EventAbstract):
    def __init__(self, dpid=None, hostname=None, **params):
        super(DpidHostnameMatchingEvent, self).__init__(**params)

        self.dpid = dpid
        self.hostname = hostname

    def get_title(self):
        return '%s == %s' % (self.dpid, self.hostname)


class CommentEvent(EventAbstract):
    def get_title(self):
        return self.notes


class ExperimentStartEvent(EventAbstract):
    def get_title(self):
        return 'Experiment started'


class ExperimentEndEvent(EventAbstract):
    def get_title(self):
        return 'Experiment ended'


class EmulationConfigEvent(EventAbstract):
    def __init__(self, config=None, group=None, run=None, experiments=None, seed=None, **params):
        super(EmulationConfigEvent, self).__init__(**params)

        self.config = config
        self.group = group
        self.run = run
        self.experiments = experiments
        self.seed = seed

    def get_title(self):
        return 'config %s group %s run %s experiments %s seed %s' % (self.config, self.group, self.run,
                                                                     self.experiments, self.seed,)


class MarkerEvent(EventAbstract):
    def __init__(self, name=None, **params):
        super(MarkerEvent, self).__init__(**params)

        self.name = name

    def get_title(self):
        return str(self.name)


class CliCommandEvent(EventAbstract):
    def __init__(self, line=None, **params):
        super(CliCommandEvent, self).__init__(**params)

        self.line = line

    def get_title(self):
        return str(self.line)


class SDNTopologyChangeEvent(EventAbstract):
    pass


class SDNConnectionChangeEvent(SDNTopologyChangeEvent):
    def __init__(self, dpid=None, added=None, **params):
        super(SDNConnectionChangeEvent, self).__init__(**params)

        self.dpid = dpid
        self.added = added

    def get_title(self):
        if self.added:
            action = 'up'
        else:
            action = 'down'

        return '%s %s' % (self.dpid, action)


class SDNLinkChangeEvent(SDNTopologyChangeEvent):
    def __init__(self, dpid1=None, port1=None, dpid2=None, port2=None, added=None, **params):
        super(SDNLinkChangeEvent, self).__init__(**params)

        self.dpid1 = dpid1
        self.port1 = port1
        self.dpid2 = dpid2
        self.port2 = port2
        self.added = added

    def get_title(self):
        if self.added:
            action = 'added'
        else:
            action = 'removed'

        return '%s:%d -> %s:%d, %s' % (self.get_context().d2n(self.dpid1), self.port1,
                                       self.get_context().d2n(self.dpid2), self.port2, action)


class SDNToBGPPortIsDownEvent(SDNTopologyChangeEvent):
    def __init__(self, dpid=None, port=None, **params):
        super(SDNToBGPPortIsDownEvent, self).__init__(**params)

        self.dpid = dpid
        self.port = port

    def get_title(self):
        #return 'Affected port %s:%d'  % (self.dpid, self.port)
        return 'BGP-facing port %s:%d is down' % (self.dpid, self.port)


class ErrorEvent(EventAbstract):
    def get_title(self):
        return self.notes


class MeasureErrorEvent(ErrorEvent):
    def __init__(self, start_event=None, **params):
        super(MeasureErrorEvent, self).__init__(**params)

        self.start_event = start_event

    def get_title(self):
        return '%s: %s' % (self.start_event, self.notes)


class HostPingErrorEvent(ErrorEvent):
    pass


class HostPingPassEvent(EventAbstract):
    def get_title(self):
        return 'Host pings passed'


class CommandAbstract(EventAbstract):
    pass


class LogOpenCommand(CommandAbstract):
    def __init__(self, path=None, **params):
        super(LogOpenCommand, self).__init__(**params)
        self.path = path

    def get_title(self):
        return 'Open logfile %s' % (self.path,)


class LogCloseCommand(CommandAbstract):
    def get_title(self):
        return 'Close logfile'