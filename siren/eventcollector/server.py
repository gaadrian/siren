import zmq
import pickle
import traceback
import time

from siren.eventcollector import get_event_collector_path
from siren.eventcollector.api import EventServerAPI, get_event_api_path
from siren.eventcollector.events import LogOpenCommand, LogCloseCommand, FullEventContext
from siren.eventcollector.livevis import get_livevis_event_path, LiveProcess


class EventServer(object):

    def __init__(self):
        self.collect_path = get_event_collector_path()
        self.api_path = get_event_api_path()
        self.livevis_path = get_livevis_event_path()
        self.fh = None
        self.pickle_log = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close_file()

    def close_file(self):
        if not self.fh is None:
            self.fh.close()
            self.fh = None
            self.pickle_log = None

    def main(self):
        # see:
        # http://zguide.zeromq.org/page:all#Handling-Multiple-Sockets
        # http://zguide.zeromq.org/py:mspoller

        zcontext = zmq.Context()

        livevis = zcontext.socket(zmq.PUSH)
        livevis.connect('ipc://%s' % (self.livevis_path,))

        collector = zcontext.socket(zmq.PULL)
        collector.bind('ipc://%s' % (self.collect_path,))

        event_api = zcontext.socket(zmq.REP)
        event_api.bind('ipc://%s' % (self.api_path,))

        poller = zmq.Poller()
        poller.register(collector, zmq.POLLIN)
        poller.register(event_api, zmq.POLLIN)
        poller.register(livevis, zmq.PUSH)

        es_api = EventServerAPI()
        live_process = LiveProcess()
        context = FullEventContext()
        while True:
            try:
                sockets = dict(poller.poll())
            except KeyboardInterrupt:
                break

            if collector in sockets:
                event = collector.recv_pyobj()

                es_api.process_event(event)

                # save event to log file
                if not self.pickle_log is None:
                    self.pickle_log.dump(event)

                event.set_context(context)
                print event.get_line()

                # close log file
                if isinstance(event, LogCloseCommand):
                    self.close_file()

                # open log file
                if isinstance(event, LogOpenCommand):
                    self.close_file()
                    self.fh = open(event.path, 'w')
                    self.pickle_log = pickle.Pickler(self.fh, 2)

                livevis_cmd = live_process.process_event(event)
                if not livevis_cmd is None:
                    livevis.send(livevis_cmd)

            if event_api in sockets:
                command = event_api.recv_string()
                event_api.send_pyobj(es_api.response(command))


def launch():
    try:
        with EventServer() as es:
            es.main()
            time.sleep(10)
    except:
        print traceback.format_exc()
        time.sleep(10)