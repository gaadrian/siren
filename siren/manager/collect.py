import subprocess

from siren.manager import get_servers
from siren.common.path import get_home_path, LOG_PREFIX


def collect(server_file):
    servers = get_servers(server_file)
    for server in servers:
        cmd = 'rsync -av root@%s:%s/ %s/ ' % (server, LOG_PREFIX, get_home_path(LOG_PREFIX))
        print '*' * 20
        print cmd
        try:
            subprocess.check_call(cmd, shell=True)
        except subprocess.CalledProcessError:
            print 'ERROR: failed to sync'
            return False
    print '** collect complete **'
    return True