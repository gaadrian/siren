INSTALL_PATH = '/siren'


def get_servers(server_file):
    servers = []
    with open(server_file, 'r') as fh:
        while True:
            server = fh.readline()
            if not server:
                break

            servers.append(server.strip())

    return servers


def get_experiments(experiment_file):
    experiments = []
    with open(experiment_file, 'r') as fh:
        while True:
            experiment = fh.readline()
            if not experiment:
                break

            experiments.append(experiment)

    return experiments
