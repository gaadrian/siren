import random
from math import ceil
import tempfile
import time
import subprocess
import os

from siren.manager import INSTALL_PATH, get_servers, get_experiments


def run(server_file, experiment_file):
    servers = get_servers(server_file)
    experiments = get_experiments(experiment_file)

    # shuffle experiments to distribute long tasks
    random.shuffle(experiments)

    chunk_size = int(ceil(float(len(experiments))/len(servers)))
    print 'Each Server gets %d experiments' % (chunk_size,)

    for i in range(len(servers)):
        server = servers[i]
        # upload experiments
        _, path = tempfile.mkstemp()
        with open(path, 'w') as fh:
            fh.writelines(experiments[i*chunk_size:(i+1)*chunk_size])
        client_path = '/tmp/sirenexper_%s.%f' % (server, float(time.time()))
        cmd = 'scp %s root@%s:%s' % (path, server, client_path)
        print cmd
        try:
            subprocess.check_call(cmd, shell=True)
            os.unlink(path)
        except subprocess.CalledProcessError:
            print 'ERROR Uploading'
            return False

        cmd = 'ssh root@%s screen -d -m %s/ibin/client %s' % (server, INSTALL_PATH, client_path)
        print cmd
        try:
            subprocess.check_call(cmd, shell=True)
        except subprocess.CalledProcessError:
            print 'ERROR Starting'
            return False

    return True