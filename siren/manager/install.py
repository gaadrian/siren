import subprocess

from siren.manager import get_servers, INSTALL_PATH
from siren.manager.sync import sync


def install(server_file):
    print 'Sync Files'
    if not sync(server_file):
        print 'Error sync: Abort installation'
        return False

    servers = get_servers(server_file)
    for server in servers:
        cmd = 'ssh root@%s %s/install.sh' % (server, INSTALL_PATH)
        try:
            subprocess.check_call(cmd, shell=True)
        except subprocess.CalledProcessError:
            print 'Installation failed on %s' % (server,)
            print 'Abort installation'
            return False
    return True