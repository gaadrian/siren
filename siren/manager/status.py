from siren.manager import get_servers
from siren.manager.client import read_status


def print_status(server_file):
    servers = get_servers(server_file)
    for server in servers:
        finished, total = read_status(server)
        print '%s: %d of %d experiments (%0.2f%%)' % (server, finished, total, float(finished)/total*100)