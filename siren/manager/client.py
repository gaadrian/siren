import subprocess

from siren.common.path import get_bin_path, get_temp_path
from siren.manager import get_experiments

STATUS_FILE = get_temp_path('siren-status.tmp', is_file=True)  # requires that all instances run the same system


def write_status(finished, total):
    with open(STATUS_FILE, 'w+') as fh:
        fh.writelines(['%d %d' % (finished, total)])


def read_status(server):
    cmd = 'ssh root@%s "cat %s"' % (server, STATUS_FILE)
    line = subprocess.check_output(cmd, shell=True)

    finished, total = line.strip().split(' ')
    finished = int(finished)
    total = int(total)

    return finished, total


def run(file_name):
    experiments = get_experiments(file_name)
    i = 0
    for experiment in experiments:
        write_status(i, len(experiments))
        i += 1
        cmd = '%s %s' % (get_bin_path('siren-network'), experiment)
        print cmd
        try:
            subprocess.check_call(cmd, shell=True)
        except subprocess.CalledProcessError:
            print 'Emulation Error'
            return
    write_status(len(experiments), len(experiments))
