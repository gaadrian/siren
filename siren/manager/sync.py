import subprocess

from siren.manager import get_servers, INSTALL_PATH
from siren.common.path import get_root_path


def sync(server_file):
    servers = get_servers(server_file)
    for server in servers:
        cmd = 'rsync -av %s/ root@%s:%s --exclude=".git/" --exclude="*.pyc"' % (get_root_path(), server, INSTALL_PATH)
        print '*' * 20
        print cmd
        try:
            subprocess.check_call(cmd, shell=True)
        except subprocess.CalledProcessError:
            print 'ERROR: failed to sync'
            return False
    print '** sync complete **'
    return True