import time
import zmq

from siren.routemonitor.map import load_linkmap, wait_linkmap
from siren.eventcollector.livevis import get_livevis_event_path

class TableAbstract(object):
    def __init__(self, hostname, ip):
        self.ip = ip
        self.value = None
        self.hostname = hostname

    def get_output(self):
        pass

    def get_interface(self, table):
        pass

    def main(self):
        zcontext = zmq.Context()
        livevis = zcontext.socket(zmq.PUSH)
        livevis.connect('ipc://%s' % get_livevis_event_path())
        wait_linkmap()
        lm = load_linkmap()
        while True:
            table = self.get_output()
            value = lm.get_target(self.get_interface(table))

            if not value == self.value:
                oldvalue = self.value
                self.value = value


                if not oldvalue is None:
                    livevis.send('remove_link(\'%s\',\'%s\',\'route\');' % (self.hostname, oldvalue))

                if not self.value is None:
                    livevis.send('add_link(\'%s\',\'%s\',\'route\');' % (self.hostname, self.value))

                print 'new path', self.value

            time.sleep(1)