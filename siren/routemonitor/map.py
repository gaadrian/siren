import pickle
import time
import os.path

from siren.common.path import get_temp_path


def get_linkmap_path():
    return get_temp_path('linkmap.pickle', is_file=True)


def load_linkmap():
    with open(get_linkmap_path()) as fh:
        return pickle.load(fh)


def wait_linkmap():
    while True:
        if os.path.isfile(get_linkmap_path()):
            return
        print 'LinkMap does not exist yet'
        time.sleep(2)


class LinkMap(object):
    def __init__(self):
        self.map = {}

    def add_link(self, interface, target):
        print interface, ' ', target, '---------------'
        self.map[interface] = target

    def get_target(self, interface):
        if interface is None or interface not in self.map:
            return None

        return self.map[interface]

    def save(self):
        with open(get_linkmap_path(), 'w') as fh:
            pickle.dump(self, fh)