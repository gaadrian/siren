import re
import subprocess
import SubnetTree

from siren.routemonitor.table import TableAbstract


class Dpctl(TableAbstract):
    def get_output(self):
        cmd = 'ovs-ofctl dump-flows %s' % (self.hostname,)
        try:
            content = subprocess.check_output(cmd, shell=True)
        except subprocess.CalledProcessError:
            content = ''
        return content

    def get_interface(self, output):
        t = SubnetTree.SubnetTree()
        for nw, port in re.findall('nw_dst=([\S^,]+).*output:([\d]+)', output):
            t[nw] = int(port)

        try:
            port = t[self.ip]
        except KeyError:
            return None

        cmd = 'ovs-ofctl dump-ports-desc %s' % (self.hostname,)
        ports_output = subprocess.check_output(cmd, shell=True)

        port_match = re.search("\s%d\(([\S]+)\)" % (port,), ports_output)
        if port_match is None:
            return None
        interface = port_match.group(1)

        return interface