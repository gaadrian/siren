import subprocess

from siren.routemonitor.table import TableAbstract


class Vtysh(TableAbstract):
    def get_output(self):
        cmd = 'vtysh -c "show ip route %s"' % (self.ip,)
        return subprocess.check_output(cmd, shell=True)

    def get_interface(self, output):
        lines = output.split('\n')
        for line in lines:
            line = line.strip()
            try:
                if line[0] == '*':
                    cols = line.split(' ')
                    return cols[-1]
            except IndexError:
                pass
        return None