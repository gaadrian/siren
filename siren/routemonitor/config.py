from siren.common.path import get_temp_path


def get_subnet_path():
    return get_temp_path('monitor', 'ip.txt', is_file=True)


def write_ip(ip):
    with open(get_subnet_path(), 'w') as fh:
        fh.write(ip)


def read_ip():
    with open(get_subnet_path(), 'r') as fh:
        return str(fh.readline()).strip()