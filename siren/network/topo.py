from siren.network.cluster import BGPCluster
from siren.network.internet import InterNetwork
from siren.network.peering import Peering

UNKNOWN_DELAY = -9999  # negative values are unknown (in iplane). Delay will be replaced with average in Peering()


class ASTopology(object):
    def __init__(self, iplane_inter_pop_links_path, as_graph, policy=True):
        self.as_graph = as_graph
        self.iplane_inter_pop_links_path = iplane_inter_pop_links_path
        self.added_links = set()
        self.policy = policy

        self.net = InterNetwork()

        self._build()

    def get_relationship(self, as_a, as_b):

        relationship = None

        edge_ab = self.as_graph.has_edge(as_a, as_b)
        edge_ba = self.as_graph.has_edge(as_b, as_a)

        if edge_ab and edge_ba:
            # peering
            relationship = Peering.REL_PEER
            self.as_graph.remove_edge(as_a, as_b)
            self.as_graph.remove_edge(as_b, as_a)
        elif edge_ab:
            # a -> b
            relationship = Peering.REL_UPSTREAM_CUSTOMER
            self.as_graph.remove_edge(as_a, as_b)
        elif edge_ba:
            # b -> a
            relationship = Peering.REL_CUSTOMER_UPSTREAM
            self.as_graph.remove_edge(as_b, as_a)

        return relationship

    def add_link(self, as_a, as_b, delay):
        if as_a < as_b:
            key = (as_a, as_b)
        else:
            key = (as_b, as_a)

        if as_a in self.as_graph.nodes() and as_b in self.as_graph.nodes():
            if not as_a == as_b and not key in self.added_links:
                self.added_links.add(key)

                relationship = self.get_relationship(as_a, as_b)

                if not relationship is None:
                    if not self.policy:
                        relationship = None

                    self.net.add_peering(as_a, as_b, delay=delay, relationship=relationship)

    def _build(self):
        bgp_cluster = BGPCluster()
        for as_number in self.as_graph.nodes():
            self.net.add_as(as_number, bgp_cluster)

        with open(self.iplane_inter_pop_links_path, 'r') as fh:
            for line in fh:
                columns = line.split(' ')
                as_a = int(columns[1])
                as_b = int(columns[3])
                delay = int(columns[4])

                self.add_link(as_a, as_b, delay)

        for as_a, as_b in self.as_graph.edges():
            self.add_link(as_a, as_b, UNKNOWN_DELAY)

    def get_network(self):
        return self.net


class DefaultSelector(object):
    pass


class DistanceSelector(object):
    def __init__(self, cluster, node, max_distance):
        pass


class StaticSelector(object):
    def __init__(self, cluster, *as_numbers):
        pass

    def match(self, graph, node):
        pass