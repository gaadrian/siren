from mininet.link import Link, TCIntf


class TCLink(Link):
    def __init__(self, node1, node2, **params):
        Link.__init__(self, node1, node2, cls1=TCIntf, cls2=TCIntf, **params)