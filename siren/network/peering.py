import random
import netaddr
from siren.eventcollector.client import EventClient
from siren.eventcollector.events import IPHostnameMatchingEvent, BGPSourceEvent, BGPPeeringEvent, SDNPeeringEvent
from siren.network.link import TCLink

BGP_PEERING_NET = '2.0.0.0/8'
DEFAULT_DELAY = 24  # average link latency


class Peering(object):
    TYPE_SDN = 0
    TYPE_BGP = 1

    REL_UNCONSTRAINED = 'uc' #-1
    REL_PEER = 'p2p' #0
    REL_UPSTREAM_CUSTOMER = 'p2c' #1
    REL_CUSTOMER_UPSTREAM = 'c2p' #2

    """
    FIXME, no support for Route Servers
    """

    used_peering_ip_rand = []
    #bgp_peering_counter = 0

    def __init__(self, peer_a, peer_b, router_a=0, router_b=0, delay=None,
                 advertisement_interval_a=None, advertisement_interval_b=None, relationship=None,
                 monitor=False, event_client=None, path_prepending_a=None, path_prepending_b=None):
        """

        :param peer_a: peer A
        :type peer_a: AS
        :param peer_b: peer B
        :type peer_b: AS
        :param params:
        :raise AssertionError:
        """
        if event_client is None:
            event_client = EventClient()

        self.peer_a = peer_a
        self.peer_b = peer_b

        self.router_a = router_a
        self.router_b = router_b

        self.params_a = None
        self.params_b = None

        common_types = self.peer_a.get_supported_peering() & self.peer_b.get_supported_peering()
        if len(common_types) == 0:
            raise Exception('Peering not possible between %s and %s' % (str(peer_a), str(peer_b)))
        self.type = min(common_types)

        if self.type == Peering.TYPE_BGP:
            peering_net = self.get_peering_net()
            ip_a = str(peering_net + 1)
            ip_b = str(peering_net + 2)

            # swap IP addresses for randomness
            if random.randint(0, 1) == 1:
                ip_a, ip_b = ip_b, ip_a

            event = IPHostnameMatchingEvent()
            event.ip = ip_a
            event.hostname = self.peer_a.get_router_name(self.router_a)
            event_client.send(event)

            event = IPHostnameMatchingEvent()
            event.ip = ip_b
            event.hostname = self.peer_b.get_router_name(self.router_b)
            event_client.send(event)

            if monitor:
                source_event = BGPSourceEvent()
                source_event.neighbor_ip = ip_a
                source_event.as_number = peer_a.get_as_number()
                event_client.send(source_event)

            if not delay is None:
                if delay < 0:
                    delay = DEFAULT_DELAY

                params_default = {'delay': '%dms' % (delay,)}
            else:
                params_default = {}
            self.params_a = params_default.copy()
            self.params_b = params_default.copy()

            if not self.peer_a.is_switch():
                self.params_a['ip'] = '%s/30' % (ip_a,)

            if not self.peer_b.is_switch():
                self.params_b['ip'] = '%s/30' % (ip_b,)


            if (not relationship is None) and (relationship!='uc'):
                if relationship == Peering.REL_PEER:
                    peer_group_a = BGPPeeringConfig.PG_PEER
                    peer_group_b = BGPPeeringConfig.PG_PEER
                elif relationship == Peering.REL_CUSTOMER_UPSTREAM:
                    peer_group_a = BGPPeeringConfig.PG_UPSTREAM
                    peer_group_b = BGPPeeringConfig.PG_CUSTOMER
                elif relationship == Peering.REL_UPSTREAM_CUSTOMER:
                    peer_group_a = BGPPeeringConfig.PG_CUSTOMER
                    peer_group_b = BGPPeeringConfig.PG_UPSTREAM
                else:
                    raise AssertionError('Unknown relationship')
            else:
                peer_group_a = None
                peer_group_b = None

            event = BGPPeeringEvent()
            event.peer_a_ip = ip_a
            event.peer_b_ip = ip_b
            event.monitor = monitor

            config_a = BGPPeeringConfig(ip_a, ip_b, 30, self.peer_b.get_as_number(),
                                        advertisement_interval=advertisement_interval_a,
                                        peer_group=peer_group_a, path_prepending=path_prepending_a)
            config_b = BGPPeeringConfig(ip_b, ip_a, 30, self.peer_a.get_as_number(),
                                        advertisement_interval=advertisement_interval_b,
                                        peer_group=peer_group_b, path_prepending=path_prepending_b)
        elif self.type == Peering.TYPE_SDN:
            # SDN Configuration is automatic
            config_a = None
            config_b = None

            event = SDNPeeringEvent()
        else:
            raise AssertionError('Unknown peering type')

        self.peer_a.register_peering(config_a)
        self.peer_b.register_peering(config_b)

        event.peer_a_as = self.peer_a.get_as_number()
        event.peer_a_router = 0
        event.peer_b_as = self.peer_b.get_as_number()
        event.peer_b_router = 0
        event.delay = delay
        event.relationship = relationship
        event_client.send(event)

    @staticmethod
    def get_peering_net():
        """
        Returns a /30
        """
        net_size = 4
        num_networks = len(netaddr.IPNetwork(BGP_PEERING_NET))/net_size
        rand_num = None

        while rand_num is None or rand_num in Peering.used_peering_ip_rand:
            rand_num = random.randint(0, num_networks - 1)

        network = netaddr.IPNetwork(BGP_PEERING_NET).network + rand_num*net_size
        Peering.used_peering_ip_rand.append(rand_num)
        return network

    def build(self, topo):
        router_a = self.peer_a.get_router_name(self.router_a)
        router_b = self.peer_b.get_router_name(self.router_b)

        topo.addLink(router_a, router_b, params1=self.params_a, params2=self.params_b, cls=TCLink)


class BGPPeeringConfig(object):

    TYPE = Peering.TYPE_BGP
    PG_PEER = 'peer'
    PG_CUSTOMER = 'customer'
    PG_UPSTREAM = 'upstream'

    def __init__(self, local_ip, neighbor_ip, prefix_length, neighbor_as_number, advertisement_interval=None, peer_group=None, path_prepending=None):
        self.local_ip = local_ip
        self.neighbor_ip = neighbor_ip
        self.prefix_length = prefix_length
        self.neighbor_as_number = neighbor_as_number
        self.advertisement_interval = advertisement_interval
        self.peer_group = peer_group
        self.path_prepending = path_prepending