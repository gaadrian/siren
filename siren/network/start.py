import random
import os
import hashlib
import time
from datetime import datetime

from mininet.log import setLogLevel
from mininet.net import Mininet

from siren.common.path import clean_temp, get_log_path
from siren.eventcollector import start_collect, start_livevis
from siren.network.cli import SirenCLI, AutoCLI, WebCLI
from siren.network.mn import BuildTopo
from siren.eventcollector.client import EventClient
from siren.eventcollector.events import LogOpenCommand, LogCloseCommand, EmulationConfigEvent
from siren.routemonitor.map import LinkMap
from siren.ofcontroller.config import write_config

from mininet.clean import cleanup as mn_cleanup


def start_emulation(setup, config='default', group='default', run=None, experiments=None, log=None, seed=None,
                    livevis=False):
    setLogLevel('info')

    # mininet clean as some switches (and others) can survive reboots
    mn_cleanup()
    # clean our tmp dir
    clean_temp()

    # start Event Collector terminal
    start_collect()
    time.sleep(1) # damit event log socket ready

    run_name = run
    if run_name is None:
        run_name = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')

    # start log file
    event_client = EventClient()
    event = LogOpenCommand()
    event.path = get_log_path(setup, config, run_name, group)
    event_client.send(event)
    time.sleep(1)

    if seed is None:
        rand_seed = hashlib.sha1(os.urandom(100)).hexdigest()
    else:
        rand_seed = seed

    random.seed(rand_seed)
    event = EmulationConfigEvent()
    event.config = config
    event.group = group
    event.run = run
    event.experiments = experiments
    event.seed = rand_seed
    event_client.send(event)

    write_config(config)

    network_info = {'setup': setup, 'config': config, 'group': group, 'run': run_name}

    # http://stackoverflow.com/questions/11108628/python-dynamic-from-import
    # http://docs.python.org/2/library/functions.html#__import__
    setup_import = __import__('nwsetup.%s' % (setup,), globals(), locals(), ['NetworkSetup'])
    setup = setup_import.NetworkSetup(config)
    net = setup.get_network()

    if livevis:
        start_livevis()
        print 'Press Enter as soon as you are connected'
        raw_input()

    topo = BuildTopo(net)
    mn = Mininet(topo=topo, build=False)

    for controller in topo.get_controllers():
        mn.addController(*controller[0], **controller[1])

    mn.build()

    print 'Build LinkMap'
    # LinkMap
    lm = LinkMap()

    for host in list(set(mn.hosts) | set(mn.switches)):
        print 'host', host.name
        for intfname, intf in host.nameToIntf.iteritems():
            print intfname
            # copied from mininet
            if intf.link:
                if intf.link.intf1.name == intfname:
                    neighbor_name = intf.link.intf2.node.name
                elif intf.link.intf2.name == intfname:
                    neighbor_name = intf.link.intf1.node.name
                else:
                    pass
                    # FIXME

                lm.add_link(intfname, neighbor_name)
    lm.save()

    try:
        mn.start()

        if experiments is None:
            print "None experiment specified"
            if not livevis:
                SirenCLI(mn, network_info)
            else:
                WebCLI(mn, network_info)
        else:
            AutoCLI(mn, network_info, setup.get_experiments(experiments))
    finally:
        # close log file
        event = LogCloseCommand()
        event_client.send(event)

        mn.stop()
        clean_temp()