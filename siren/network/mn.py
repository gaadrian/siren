import random

from mininet.topo import Topo


class BuildTopo(Topo):
    def __init__(self, obj, **opts):
        super(BuildTopo, self).__init__(**opts)
        self.obj = obj
        self.obj.build_topology(self)

    def get_controllers(self):
        return self.obj.get_controllers()

    def hosts(self, sort=True):
        hosts = Topo.hosts(self)
        random.shuffle(hosts)
        return hosts

    def switches(self, sort=True):
        switches = Topo.switches(self)
        random.shuffle(switches)
        return switches

    def links(self, sort=True):
        links = Topo.links(self)
        random.shuffle(links)
        return links