import netaddr
import pickle

from siren.eventcollector.client import EventClient
from siren.eventcollector.events import DpidHostnameMatchingEvent, ClusterEvent
from siren.network.node import QuaggaHost, ExabgpHost, ScSwitch, PoxController
from siren.network.peering import Peering
from siren.ofcontroller import get_ofcontroller_config_path

class Cluster(object):
    cluster_counter = 0

    def __init__(self):
        self.event_client = EventClient()

        self.cluster_id = Cluster.cluster_counter

        event = ClusterEvent()
        event.cluster_id = self.cluster_id
        if type(self) is BGPCluster:
            event.cluster_type = ClusterEvent.CLUSTER_TYPE_BGP
        elif type(self) is SDNCluster:
            event.cluster_type = ClusterEvent.CLUSTER_TYPE_SDN
        elif type(self) is MonitorCluster:
            event.cluster_type = ClusterEvent.CLUSTER_TYPE_MONITOR

        self.event_client.send(event)

        Cluster.cluster_counter += 1

    def get_id(self):
        return self.cluster_id

    def pre_build(self, topo):
        pass

    def post_build(self, topo):
        pass

    def get_controller(self):
        return None

    def build_as(self, topo, as_instance):
        raise NotImplementedError

    def get_as_router_name(self, as_num, router_id):
        raise NotImplementedError

    def is_switch(self, as_instance):
        raise NotImplementedError


class BGPCluster(Cluster):
    def get_supported_peering(self):
        return {Peering.TYPE_BGP}

    def build_as(self, topo, as_instance):
        for router_id in as_instance.get_router_ids():
            params = {}
            params['autoSetIp'] = False # FIXME: das gehoert zu QuaggaHost, geht da aber nicht, siehe net...addHost()
            # FIXME ip muss erstellt werden, bevor Class erstellt wird
            h = topo.addHost(as_instance.get_router_name(router_id), cls=QuaggaHost, **params)
            bh = RouterConfig(as_instance.get_router_name(router_id), as_instance.get_as_number(),
                            as_instance.get_prefixes(), as_instance.get_bgp_router_id(router_id),
                            as_instance.get_bgp_peerings(router_id))

            QuaggaHost.write_config(bh)

    def get_as_router_name(self, as_num, router_id):
        return 'r%d.%d' % (as_num, router_id)

    def is_switch(self, as_instance):
        return False


class MonitorCluster(BGPCluster):
    bgp_monitor_counter = 1

    def __init__(self):
        super(MonitorCluster, self).__init__()
        self.bgp_routers = []

        self.name = 'm%d' % (MonitorCluster.bgp_monitor_counter,)
        MonitorCluster.bgp_monitor_counter += 1

    def build_as(self, topo, as_instance):
        for router_id in as_instance.get_router_ids():

            bh = RouterConfig(as_instance.get_router_name(router_id), as_instance.get_as_number(),
                           as_instance.get_prefixes(), as_instance.get_bgp_router_id(router_id),
                           as_instance.get_bgp_peerings(router_id))
            self.bgp_routers.append(bh)

    def pre_build(self, topo):

        params = {}
        params['autoSetIp'] = False # FIXME
        h = topo.addHost(self.name, cls=ExabgpHost, **params)

    def post_build(self, topo):
        ExabgpHost.write_config(self.name, self.bgp_routers, ExabgpHost.PROCESS_MONITOR)

    def get_as_router_name(self, as_num, router_id):
        return self.name


class SDNCluster(Cluster):
    bgp_speaker_counter = 1

    def __init__(self):
        super(SDNCluster, self).__init__()
        self.router_configs = {}
        self.dpid_to_as_num = {}
        self.controller_name = 'c%d' % (SDNCluster.bgp_speaker_counter,)
        self.bgp_speaker_name = 'vb%d' % (SDNCluster.bgp_speaker_counter,)
        SDNCluster.bgp_speaker_counter += 1

    def get_supported_peering(self):
        return {Peering.TYPE_BGP, Peering.TYPE_SDN}

    def build_as(self, topo, as_instance):
        for router_id in as_instance.get_router_ids():
            name = as_instance.get_router_name(router_id)
            dpid_hum = as_instance.get_as_number()*1000+router_id

            event = DpidHostnameMatchingEvent()
            event.hostname = name
            event.dpid = dpid_hum
            self.event_client.send(event)

            dpid = hex(dpid_hum)[2:] # FIXME, move to Switch
            dpid = '0' * ( 16 - len(dpid)) + dpid
            topo.addSwitch(name, dpid=dpid, cls=ScSwitch)
            bh = RouterConfig(name, as_instance.get_as_number(), as_instance.get_prefixes(),
                           as_instance.get_bgp_router_id(router_id), as_instance.get_bgp_peerings(router_id))
            self.router_configs[dpid_hum] = bh
            self.dpid_to_as_num[dpid_hum] = as_instance.get_as_number()

    def get_as_router_name(self, as_num, router_id):
        return 'r%d.%d' % (as_num, router_id)

    def post_build(self, topo):
        params = {'autoSetIp': False}
        topo.addHost(self.bgp_speaker_name, cls=ExabgpHost, **params)
        ExabgpHost.write_config(self.bgp_speaker_name, self.router_configs.itervalues())


        of_config = {'socket_path': ExabgpHost.get_socket_path(self.bgp_speaker_name),
            'bgp_sessions': {},
            'as_numbers': self.dpid_to_as_num,
            'connected_networks': {}}

        for dpid, bgp_config in self.router_configs.iteritems():
            for network in bgp_config.networks:
                local_ip = netaddr.IPNetwork(network).network + 1
                of_config['connected_networks'].setdefault(dpid, []).append({'network': network,
                                                                             'local_ip': str(local_ip) })

            ips = []

            for link in bgp_config.links:
                ips.append('%s/%s' % (link.local_ip, link.prefix_length))
                of_config['bgp_sessions'].setdefault(dpid, []).append({'local_ip': link.local_ip,
                                                                       'neighbor_ip': link.neighbor_ip,
                                                                       'neighbor_as_num': link.neighbor_as_number})

            if len(ips) == 0:
                continue
            topo.addLink(self.bgp_speaker_name, bgp_config.hostname, params1={'ips': ips})



        with open(get_ofcontroller_config_path(self.bgp_speaker_name), 'w') as fh:
            pickle.dump(of_config, fh)

    def is_switch(self, as_instance):
        return True

    def get_controller(self):
        return [self.controller_name], {'controller': PoxController, 'bgp_name': self.bgp_speaker_name}


class RouterConfig(object):
    def __init__(self, hostname, as_number, networks, loopback, links):
        self.hostname = hostname
        self.as_number = as_number
        self.networks = networks
        self.loopback = loopback
        self.links = links