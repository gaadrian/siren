import networkx as nx


class ASRelationship(object):
    REL_UPSTREAM = -1
    REL_PEER = 0

    def __init__(self, filename):
        self.filename = filename
        self.selectors = []
        self.graph = nx.DiGraph()
        self._load_graph()

    def add_selector(self, selector):
        self.selectors.append(selector)

    def remove_selector(self, selector):
        self.selectors.remove(selector)

    def clear_selectors(self):
        self.selectors = []

    def _load_graph(self):
        with open(self.filename, 'r') as fh:
            for line in fh:
                line = line.strip()
                if not line[0] == '#':
                    columns = line.split('|')
                    a = int(columns[0])
                    b = int(columns[1])
                    rel = int(columns[2])

                    if rel == ASRelationship.REL_UPSTREAM:
                        self.graph.add_edge(b, a, rel=ASRelationship.REL_UPSTREAM)
                    elif rel == ASRelationship.REL_PEER:
                        self.graph.add_edge(a, b, rel=ASRelationship.REL_PEER)
                        self.graph.add_edge(b, a, rel=ASRelationship.REL_PEER)
                    else:
                        raise AssertionError(rel)

    def get_as_numbers(self):
        as_numbers_selected = set()
        for selector in self.selectors:
            as_numbers_selected.update(selector.select(self.graph))
        return as_numbers_selected

    def get_graph(self):
        as_numbers_selected = self.get_as_numbers()
        return self.graph.subgraph(list(as_numbers_selected))

    def shortest_path_length(self, source, targets, current_distance=0):
        # FIXME!
        min_distance = None

        neighbors = self.graph.neighbors(source)
        for neighbor in neighbors:
            peering = self.graph.has_edge(neighbor, source)

            if neighbor in targets:
                pass


class ASSelector(object):
    def select(self, graph):
        raise NotImplementedError()


class ASRelationshipSelector(ASSelector):
    def __init__(self, relationships, as_numbers):
        self.relationships = relationships
        self.as_numbers = as_numbers

    def select(self, graph):
        selected_as_numbers = set()
        for as_number in self.as_numbers:
            selected_as_numbers.update(self.select_successors(graph, as_number, set()))
        return selected_as_numbers

    def add_condition(self, graph, as_number):
        return True

    def select_successors(self, graph, as_number, selected_as_numbers, depth=0):
        if self.add_condition(graph, as_number):
            selected_as_numbers.add(as_number)

        for successor in graph.successors(as_number):
            if successor not in selected_as_numbers and graph[as_number][successor]['rel'] in self.relationships:
                selected_as_numbers.update(self.select_successors(graph, successor, selected_as_numbers, depth=depth+1))
        return selected_as_numbers


class UpstreamASSelector(ASRelationshipSelector):
    def __init__(self, start_as_number):
        ASRelationshipSelector.__init__(self, {ASRelationship.REL_UPSTREAM}, {start_as_number})


class MajorASSelector(ASRelationshipSelector):
    """
    Selects networks with no upstream provider
    """
    def __init__(self):
        seeds = {3356, 3549, 1, 2828, 7018, 1239, 6461, 3257, 2914}
        ASRelationshipSelector.__init__(self, {ASRelationship.REL_UPSTREAM}, seeds)

    def add_condition(self, graph, as_number):
        for successor in graph.successors(as_number):
            if graph[as_number][successor]['rel'] == ASRelationship.REL_UPSTREAM:
                return False

        return True


class AllASSelector(ASSelector):
    def select(self, graph):
        return graph.nodes()
