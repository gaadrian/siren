import random

import netaddr
from siren.eventcollector.client import EventClient
from siren.eventcollector.events import ASEvent, RouterEvent, \
    HostEvent
from siren.network.cluster import BGPCluster, MonitorCluster
from siren.network.node import ASHost
from siren.network.peering import Peering, BGPPeeringConfig
from siren.routemonitor.config import write_ip


BGP_ROUTER_ID_NET = '1.0.0.0/8'
BGP_COLLECTOR_NET = '3.0.0.0/8'
PREFIX_NET = '8.0.0.0/8'


class AS(object):

    used_router_id_rand = []

    # use cluster to build network
    def __init__(self, as_number, cluster, event_client=None):
        self.as_number = as_number
        self.cluster = cluster
        #self.policy = policy
        self.bgp_peerings = []
        self.routers = []
        self.prefixes = []

        if not event_client is None:
            self.event_client = event_client
        else:
            self.event_client = EventClient()

        event = ASEvent()
        event.as_number = self.as_number
        event.cluster_id = self.cluster.get_id()
        self.event_client.send(event)

        self.add_router()

        self._prefix_counter = 0
        self._host_counter = 0

    def get_router_name_by_nh(self, next_hop):
        for config in self.bgp_peerings:
            if config.local_ip == next_hop:
                return self.get_router_name(0)

        return None

    def get_supported_peering(self):
        return self.cluster.get_supported_peering()

    def get_as_number(self):
        return self.as_number

    def register_peering(self, peering):
        if peering is None:
            return
        elif isinstance(peering, BGPPeeringConfig):
            self.bgp_peerings.append(peering)
        else:
            raise AssertionError('Peering unknown')

    def build(self, topo):
        self.cluster.build_as(topo, self)

        for network in self.prefixes:
            if network[1]:
                net = netaddr.IPNetwork(network[0])
                netid = net.network
                ip = '%s/%d' % (str(netid+2), net.prefixlen)
                ip_router = '%s/%d' % (str(netid+1), net.prefixlen)
                params = {'defaultRoute': 'via ' + str(netid+1), 'autoSetIp': False}
                name = 'h%d.%d' % (self.as_number, self._host_counter)

                event = HostEvent()
                event.as_number = self.as_number
                event.host_id = self._host_counter
                event.router_id = 0
                self.event_client.send(event)

                topo.addHost(name, cls=ASHost, **params)
                if self.is_switch():
                    params2 = None
                else:
                    params2 = {'ip': ip_router}
                topo.addLink(name, self.get_router_name(0), params1={'ip': ip}, params2=params2)
                self._host_counter += 1

    def get_bgp_peerings(self, router_id):
        return self.bgp_peerings

    def get_bgp_router_id(self, router_id):
        #network = netaddr.IPNetwork(BGP_ROUTER_ID_NET).network + self.as_number*256 + router_id

        # random
        num_addresses = len(netaddr.IPNetwork(BGP_ROUTER_ID_NET))
        rand_int = None
        while rand_int is None or rand_int in AS.used_router_id_rand:
            rand_int = random.randint(1, num_addresses - 1)

        AS.used_router_id_rand.append(rand_int)
        network = netaddr.IPNetwork(BGP_ROUTER_ID_NET).network + rand_int
        return str(network)

    def add_router(self):
        event = RouterEvent()
        event.as_number = self.as_number
        event.router_id = 0
        self.event_client.send(event)
        self.routers.append([])

    def get_prefixes(self):
        return [net[0] for net in self.prefixes]

    def add_prefix(self, add_host=False, track=False):
        if self._prefix_counter > 31:
            raise AssertionError('No subnets left')

        prefix = netaddr.IPNetwork(PREFIX_NET).network + self.as_number*256 + self._prefix_counter*8
        net = str(prefix) + '/29'
        self.prefixes.append((net, add_host))

        if track:
            write_ip(str(prefix))

        self._prefix_counter += 1
        return net

    def get_router_ids(self):
        return range(len(self.routers))

    def get_router_name(self, router_id):
        return self.cluster.get_as_router_name(self.as_number, router_id)

    def get_router_names(self):
        routers = set()
        routers.add(self.get_router_name(0))
        return routers

    def __str__(self):
        return 'AS%d' % (self.as_number,)

    def is_switch(self):
        return self.cluster.is_switch(self)


class InterNetwork(object):
    def __init__(self):
        self.ases = {} # in this class, as we want to be able to switch ASes easily
        self.peerings = []
        self.clusters = []

        self.event_client = EventClient()

        self.monitor_as = 65000
        self.add_as(self.monitor_as, MonitorCluster())

    def add_as(self, as_number, cluster):
        if as_number > 65535:
            raise AssertionError('Only 16bit AS numbers allowed')
        self.ases[as_number] = AS(as_number, cluster, event_client=self.event_client)

        if cluster not in self.clusters:
            self.clusters.append(cluster)

        if not as_number == self.monitor_as:
            if isinstance(cluster, BGPCluster):
                self.add_peering(as_number, self.monitor_as, advertisement_interval_a=1, monitor=True)

    def add_prefix(self, as_num, **kwargs):
        return self.get_as(as_num).add_prefix(**kwargs)

    def add_peering(self, as_num_a, as_num_b, **params):  # , peer_a_device=None, peer_b_device=None):

        assert as_num_a in self.ases and as_num_b in self.ases, 'At least one peer does not exist'

        peer_a = self.get_as(as_num_a)
        peer_b = self.get_as(as_num_b)
        params['event_client'] = self.event_client

        peering = Peering(peer_a, peer_b, **params)
        self.peerings.append(peering)

    def get_as(self, as_num):
        assert as_num in self.ases
        return self.ases[as_num]

    def get_router_name(self, next_hop):
        for as_inst in self.ases:
            name = as_inst.get_router_name_by_nh(next_hop)
            if name is not None:
                return name
        return None

    def get_router_names(self):
        router_names = {}
        for as_inst in self.ases:
            router_names.update(as_inst.get_router_names())
        return router_names

    def build_topology(self, topo):
        for cluster in self.clusters:
            cluster.pre_build(topo)

        for as_instance in self.ases.itervalues():
            as_instance.build(topo)

        for peering in self.peerings:
            peering.build(topo)

        for cluster in self.clusters:
            cluster.post_build(topo)

    def get_controllers(self):
        controllers = []
        for cluster in self.clusters:
            controller = cluster.get_controller()
            if not controller is None:
                controllers.append(controller)
        return controllers