from mako.template import Template
from mininet.node import Host, Controller, Host, OVSKernelSwitch, HostWithPrivateDirs
import os
import subprocess
import time

from siren.common.path import get_temp_path, get_bin_path, get_libs_path, get_package_path, get_internal_bin_path


class ScSwitch(OVSKernelSwitch):
    def __init__(self, name, *args, **kwargs):
        OVSKernelSwitch.__init__(self, name, *args, **kwargs)
        self.cmd('%s %s openflow &' % (get_internal_bin_path('routemonitor'), name))


class QuaggaHost(HostWithPrivateDirs):
    def __init__(self, name, *args, **kwargs):
        privateDirs = ['/var/log', '/var/run', '/etc/quagga']
        kwargs['privateDirs'] = privateDirs
        HostWithPrivateDirs.__init__(self, name, *args, **kwargs)

        # set important kernel settings
        self.cmd('sysctl -w net.ipv4.conf.all.rp_filter=0')
        self.cmd('sysctl -w net.ipv4.conf.default.rp_filter=0')
        self.cmd('sysctl -w net.ipv4.ip_forward=1')


        self.cmd('mkdir -p /var/log/quagga')
        self.cmd('chmod -R 777 /var/log')

        # copy template files
        self.cmd('cp -p /tmp/siren/quagga-template/' + name + '/* /etc/quagga/')
        self.cmd('chown -R quagga:quagga /etc/quagga')

        # /etc/environment set VTYSH_PAGER to cat or more

        # start quagga
        self.cmd('/etc/init.d/quagga start')

        # start network traffic capture
        self.cmd('%s &' % get_internal_bin_path('ntcapture'))

        # start route monitor
        self.cmd('%s %s quagga &' % (get_internal_bin_path('routemonitor'), name))

    def terminate(self):
        self.cmd('/etc/init.d/quagga stop')
        # as quagga is a daemon, it has a different pid than the host, therefore it is not killed
        # when terminated
        #sself.cmd('bash -c "cat /var/run/quagga/*.pid | xargs kill"')
        #time.sleep(0.5)
        HostWithPrivateDirs.terminate(self)

    @staticmethod
    def write_config(node):
        path = get_temp_path('quagga-template', node.hostname)
        #call('rm -R %s' % (path,), shell=True)
        #call('mkdir -p %s' % (path,), shell=True)
        subprocess.call('cp -p %s/* %s' % (get_package_path('network', 'templates', 'quagga'), path), shell=True)
        files = ['bgpd.conf', 'zebra.conf', 'vtysh.conf']
        for file_name in files:
            fn = path + file_name
            config = Template(filename=fn)
            with open(fn, 'w') as f:
                f.write(config.render(node=node))


class ExabgpHost(HostWithPrivateDirs):
    PROCESS_MONITOR = 1
    PROCESS_SDN = 2

    def __init__(self, name, *args, **kwargs):
        private_dirs = ['/var/log', '/var/run']
        kwargs['privateDirs'] = private_dirs
        HostWithPrivateDirs.__init__(self, name, *args, **kwargs)

        cmd_path = get_libs_path('exabgp', 'sbin', 'exabgp')
        config_path = ExabgpHost.get_config_path(name)
        # start ExaBGP
        env = 'env exabgp.daemon.user=root exabgp.daemon.pid=/var/run/exabgp.pid exabgp.daemon.daemonize=true'  # exabgp.log.destination=/tmp/%s.log' % (self.name,)
        log_redirect = '1>/tmp/%s.out.log 2>/tmp/%s.error.log' % (self.name, self.name)
        self.cmd('%s %s %s %s &' % (env, cmd_path, config_path, log_redirect))
        print '%s %s %s %s &' % (env, cmd_path, config_path, log_redirect) # FIXME

    def terminate(self):
        # ExaBGP's child processes have a different process group id (this solution only supports one child)
        self.cmd('bash -c "echo `cat /var/run/exabgp.pid` `cat /var/run/exabgp.pid | xargs pgrep -P` | xargs kill -9"')
        time.sleep(0.5)
        HostWithPrivateDirs.terminate(self)

    @staticmethod
    def get_config_path(name):
        return get_temp_path('exabgp', 'config', 'exa_%s.conf' % (name,), is_file=True)

    @staticmethod
    def get_socket_path(name):
        os.chmod(get_temp_path('exabgp', 'sockets'), 0777)
        return get_temp_path('exabgp', 'sockets', name, is_file=True)

    @staticmethod
    def write_config(name, routers, process=None):
        if process is None:
            process = ExabgpHost.PROCESS_SDN

        input_path = get_package_path('network', 'templates', 'exabgp', 'exabgp.conf')
        output_path = ExabgpHost.get_config_path(name)
        config = Template(filename=input_path)

        if process == ExabgpHost.PROCESS_SDN:
            process_path = '/bin/nc -l -U %s' % (ExabgpHost.get_socket_path(name),)
            #process_path = '%s %s %s' % (get_package_path('network', 'templates', 'exabgp', 'nc.sh'),
            #                             ExabgpHost.get_socket_path(name), '/tmp/exacmd.%s.log' % (name,))
        elif process == ExabgpHost.PROCESS_MONITOR:
            process_path = get_internal_bin_path('bgpcollector')

        print 'writing exabgp config ', output_path
        with open(output_path, 'w') as f:
            f.write(config.render(name=name, routers=routers, process_path=process_path))


class ASHost(Host):
    def __init__(self, name, *args, **kwargs):
        Host.__init__(self, name, *args, **kwargs)
        self.cmd('%s &' % get_internal_bin_path('measure-server'))
        self.cmd('%s quagga &' % get_internal_bin_path('routemonitor'))


class PoxController(Controller):
    def __init__(self, name, **kwargs):
        kwargs.setdefault('command', get_internal_bin_path('ofcontroller', [kwargs['bgp_name']]))
        kwargs['cargs'] = '--port=%d'
        #print get_bin_path('siren-ofcontroller', [kwargs['bgp_name']])
        Controller.__init__(self, name, **kwargs)