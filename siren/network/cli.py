import argparse
import matplotlib.pyplot as plt
from mininet.cli import CLI
import networkx as nx
import time
import re
import shlex
import os
import signal
import zmq

from siren.common.path import get_log_path, get_internal_bin_path
from siren.eventcollector.client import EventClient
from siren.eventcollector.events import LinkChangeEvent, LogOpenCommand, LogCloseCommand, CommentEvent, HostPingErrorEvent, HostPingPassEvent, ExperimentStartEvent, ExperimentEndEvent, MarkerEvent, CliCommandEvent
from siren.eventcollector.api import EventAPIClient


# adapted from http://stackoverflow.com/questions/287871/print-in-terminal-with-colors-using-python
# http://en.wikipedia.org/wiki/ANSI_escape_code
class ANSIColors(object):
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'

    CURSOR_UP = '\033[F'
    CLEAR_LINE = '\033[K'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''


class StopExperimentException(Exception):
    pass


class SirenCLI(CLI):

    prompt = ANSIColors.BOLD + ANSIColors.OKBLUE + 'siren> ' + ANSIColors.ENDC

    def __init__(self, mininet, network_info):
        self.network_info = network_info
        self.event_client = EventClient()
        self.event_api = EventAPIClient()
        self.next_hop_states = {}
        CLI.__init__(self, mininet)  # starts loop, so everything has to be on top of this command

    def onecmd(self, line):
        event = CliCommandEvent()
        event.line = line
        self.event_client.send(event)
        return CLI.onecmd(self, line)

    def cmdloop(self, intro=None):
        print '** Check connection to event API'
        self.event_api.noop()
        print 'OK'

        print '** Waiting until network has converged'
        time.sleep(10)
        self.onecmd('pingHosts')
        self.onecmd('waitconverged')
        self.onecmd('pingHosts')
        time.sleep(10)
        self.onecmd('waitconverged')
        self.onecmd('pingHosts')
        time.sleep(10)
        self.onecmd('waitconverged')
        self.onecmd('checkHosts')
        if self._pre_start():
            CLI.cmdloop(self)
        else:
            self.onecmd('exit')

    def _close_log(self):
        event = LogCloseCommand()
        self.event_client.send(event)

    def _open_log(self, filename=None, experiment=None, experiment_run=0):
        event = LogOpenCommand()
        event.path = get_log_path(self.network_info['setup'], self.network_info['config'], self.network_info['run'],
                                  self.network_info['group'], filename=filename, experiment=experiment, experiment_run=experiment_run)
        self.event_client.send(event)

    def _pre_start(self):
        self._close_log()
        self._open_log(filename='manual')

        return True

    def do_exit( self, _line ):
        # we are not interested in the deconstruction of the network; it would be confusing
        event = LogCloseCommand()
        self.event_client.send(event)
        return CLI.do_exit(self, _line)

    def do_comment(self, line):
        event = CommentEvent()
        event.notes = line
        self.event_client.send(event)

    def do_marker(self, line):
        event = MarkerEvent()
        event.name = line.strip()
        self.event_client.send(event)

    def get_node_name(self, interface_name):
        """
        Returns node name based on interface name
        """
        interface_parts = interface_name.split('-')
        return interface_parts[0]

    def get_neighbor_name(self, interface_name):
        if not self.is_mininet_interface(interface_name):
            return None
        node = self.mn[self.get_node_name(interface_name)]

        intf = node.nameToIntf[interface_name]

        # copied from mininet
        if intf.link:
            interfaces = {intf.link.intf1.name, intf.link.intf2.name}
            interfaces.remove(interface_name)
            other_node_name = self.get_node_name(interfaces.pop())
            return other_node_name

        return None

    def get_as_number(self, nodename):
        match = re.search('^r(\d+)\.', nodename)
        if match is None:
            return None
        return int(match.group(1))

    def do_bgpannounce(self, line):
        parser = argparse.ArgumentParser(prog='bgpannounce', description='Announce prefix on a Quagga node')
        parser.add_argument('node')
        parser.add_argument('prefix')
        commands = shlex.split(line)
        try:
            namespace = parser.parse_args(args=commands)
        except SystemExit:
            return

        try:
            node = self.mn[namespace.node]
        except KeyError:
            print 'Node does not exist: %s' % (namespace.node,)
            return

        command = get_internal_bin_path('announce', [str(self.get_as_number(namespace.node)), namespace.prefix])
        node.cmdPrint(command)

    def do_waitconverged(self, line):
        self.do_lastrcps('-w 90')
        self.do_lastrcpsc('-w 90')

    def do_lastrcps(self, line):
        print 'Last rcp signaling'
        parser = argparse.ArgumentParser(prog='lastrcps', description='Prints last routing control plane signaling')

        self._last_change(parser, self.event_api.last_rcp_signaling, line)

    def do_lastrcpsc(self, line):
        print 'Last rcp state change'
        parser = argparse.ArgumentParser(prog='lastrcpsc', description='Prints last routing control plane state change')

        self._last_change(parser, self.event_api.last_rcp_state_change, line)

    def _last_change(self, parser, func, line):
        parser.add_argument('-w', type=int, default=-1)
        parser.add_argument('--quiet', '-q', action='store_true')
        commands = shlex.split(line)
        try:
            namespace = parser.parse_args(args=commands)
        except SystemExit:
            return

        print ' '
        while True:
            last_change = int(func()[1])

            if not namespace.quiet:
                print ANSIColors.CURSOR_UP + ANSIColors.CLEAR_LINE + 'Last change %d seconds ago' % (last_change,)

            if last_change >= namespace.w:
                break

            time.sleep(1)
            self.event_api.noop()

    def name_ip_substitution(self, host_name, verbose=False):
        if host_name.startswith('h'):
            ip = self.mn[host_name].IP()
            if verbose:
                print '%s has IP %s' % (host_name, ip)
            return ip

    def do_ip(self, line):
        # return all ip addresses of a node
        parser = argparse.ArgumentParser(prog='ip', description='Prints ip addresses of a node')
        parser.add_argument('node')
        parser.add_argument('action', choices=['show'])

        commands = shlex.split(line)
        try:
            namespace = parser.parse_args(args=commands)
        except SystemExit:
            return

        node = self.mn[namespace.node]

        for intf in node.intfList():
            print intf.IP()

    def do_wait(self, line):
        parser = argparse.ArgumentParser(prog='wait', description='Timer')
        parser.add_argument('seconds', type=int)

        commands = shlex.split(line)
        try:
            namespace = parser.parse_args(args=commands)
        except SystemExit:
            return

        print 'Sleep for %d seconds\n' % (namespace.seconds,)
        time.sleep(namespace.seconds)
        self.event_api.noop()

    def do_nodecmd(self, line):
        parser = argparse.ArgumentParser(prog='nodecmd', description='Run command on a node')
        parser.add_argument('node')
        parser.add_argument('command')

        commands = shlex.split(line) # TODO catch Exceptions! e.g. nodecmd h3.0 "ifconfig bla'

        try:
            namespace = parser.parse_args(args=commands)
        except SystemExit: # TODO catch SystemExit in mininet/cli
            return

        try:
            node = self.mn[namespace.node]
        except KeyError:
            print 'Node does not exist: %s' % (namespace.node,)
            return

        node.cmdPrint(namespace.command)

    def do_interface(self, line):
        parser = argparse.ArgumentParser(prog='interface', description='Print interface names between two nodes')
        parser.add_argument('node_a')
        parser.add_argument('node_b')

        commands = shlex.split(line)
        try:
            namespace = parser.parse_args(args=commands)
        except SystemExit:
            return

        print str(self.find_interface(namespace.node_a, namespace.node_b))
        print str(self.find_interface(namespace.node_b, namespace.node_a))

    def do_hping(self, line):
        parser = argparse.ArgumentParser(prog='hping', description='Ping another host')
        parser.add_argument('host_a')
        parser.add_argument('host_b')

        commands = shlex.split(line)
        try:
            namespace = parser.parse_args(args=commands)
        except SystemExit:
            return

        node = self.mn[namespace.host_a]
        target = self.name_ip_substitution(namespace.host_b)

        node.cmdPrint('ping %s' % (target,))

    def do_neighbors(self, line):
        parser = argparse.ArgumentParser(prog='neighbors', description='Print neighbors of a node')
        parser.add_argument('node')

        commands = shlex.split(line)
        try:
            namespace = parser.parse_args(args=commands)
        except SystemExit:
            return

        node = self.mn[namespace.node]

        neighbors = []
        for intf in node.intfList():
            current_neighbor_name = self.get_neighbor_name(intf.name)
            neighbors.append(current_neighbor_name)

        print ' '.join(neighbors)

    def is_mininet_interface(self, interface_name):
        """
        To filter interfaces such as 'lo' or 'eth0' on hosts without its own namespace
        :param interface_name:
        :return:
        """
        return '-' in interface_name

    def find_interface(self, node_name, neighbor_name):
        node_a = self.mn[node_name]

        for intf in node_a.intfList():
            if not self.is_mininet_interface(intf.name):
                continue
            current_neighbor_name = self.get_neighbor_name(intf.name)
            if current_neighbor_name == neighbor_name:
                return intf
        return None

    def do_link(self, line):
        parser = argparse.ArgumentParser(prog='link', description='Modify links within the network')
        parser.add_argument('node_a')
        parser.add_argument('node_b')
        parser.add_argument('action', choices=['down', 'up', 'reset'])

        commands = shlex.split(line)
        try:
            namespace = parser.parse_args(args=commands)
        except SystemExit:
            return

        # FIXME: does not work with hosts, as they are removing the default route!
        if namespace.action == 'up' and (namespace.node_a.startswith('h') or namespace.node_b.startswith('h')):
            print 'Warning: hosts have lost default route'

        #FIXME check whether nodes exist
        self.change_interface(namespace.node_a, namespace.node_b, namespace.action)

    def change_interface(self, node_a, node_b, action):
        intf_a = self.find_interface(node_a, node_b)
        intf_b = self.find_interface(node_b, node_a)

        if intf_a is None or intf_b is None:
            print 'Can not find all interfaces. Sorry'
            return

        print 'Interface %s: %s (IP: %s)' % (node_a, str(intf_a), str(intf_a.IP()))
        print 'Interface %s: %s (IP: %s)' % (node_b, str(intf_b), str(intf_b.IP()))

        if action == 'down' or action == 'reset':
            intf_a.ifconfig('down')
            intf_b.ifconfig('down')

        if action == 'reset':
            time.sleep(0.2)

        if action == 'up' or action == 'reset':
            intf_a.ifconfig('up')
            intf_b.ifconfig('up')

        event = LinkChangeEvent()
        event.interface_a = intf_a.name
        event.interface_b = intf_b.name
        event.action = action

        self.event_client.send(event)

    @staticmethod
    def quagga_route(output):
        lines = output.split('\n')
        for line in lines:
            line = line.strip()
            try:
                if line[0] == '*':
                    cols = line.split(' ')
                    return cols[-1]
            except IndexError:
                pass
        return None

    def ping_hosts(self):
        hosts = []
        for host in self.mn.hosts:
            if re.match(r'h\d+', host.name) is not None:
                hosts.append(host)

        if len(hosts) == 0:
            return None, 0

        packet_loss = self.mn.ping(hosts=hosts, timeout='1')
        if packet_loss == 0:
            return True, packet_loss
        else:
            return False, packet_loss

    def do_pingHosts(self, _line):
        self.ping_hosts()

    def do_checkHosts(self, line):
        parser = argparse.ArgumentParser(prog='checkHosts', description='Check connectivity between all hosts')
        parser.add_argument('--stop', '-s', action='store_true')

        commands = shlex.split(line)
        try:
            namespace = parser.parse_args(args=commands)
        except SystemExit:
            return

        result, packet_loss = self.ping_hosts()
        if result is False:
            event = HostPingErrorEvent()
            print 'Host connectivity: FAIL'
            if namespace.stop:
                raise StopExperimentException()
        else:
            event = HostPingPassEvent()
            print 'Host connectivity: OK'
        event.notes = str(packet_loss)
        self.event_client.send(event)


class ExperimentCommands(object):
    """ Contains all commands which an experiment can run. """

    def __init__(self, cli):
        """
        :param cli: CLI instance, where the experiment commands should be executed on
        """
        self._cli = cli

    def cli_cmd(self, command):
        """ Run a command like in the manual mininet CLI

        :param command: command to run
        :type command: str
        :return: output of the called function
        :rtype: str
        """
        print ANSIColors.BOLD + '** Automated command:' + ANSIColors.ENDC, command
        return self._cli.onecmd(command)

    def measure_start(self, node_src, node_dst):
        """Start measurement between two nodes

        :param node_src: source node
        :type node_src: str
        :param node_dst: destination node
        :type node_dst: str
        :return: PID of the measurement process
        :rtype: int
        """
        cmd = get_internal_bin_path('measure-client', [self._cli.name_ip_substitution(node_dst)])
        pid = int(self._cli.mn[node_src].cmd('%s &>/tmp/meas.tb & echo $!' % (cmd,)).strip())  # dev/null
        return pid

    def measure_stop(self, pid):
        """ Stops a previously started measurement

        :param pid: PID of the measurement process
        :type pid: int
        """
        os.kill(pid, signal.SIGTERM)


class AutoCLI(SirenCLI):
    """ A CLI class to automate experiments """

    prompt = '#-#AUTOCLI#-#'

    def __init__(self, mininet, network_info, experiments=None):
        self.experiments = experiments

        SirenCLI.__init__(self, mininet, network_info)  # starts loop, so everything has to be on top of this command

    def _pre_start(self):
        self._close_log()
        try:
            self._do_experiments()
        except StopExperimentException:
            # go back to CLI to debug
            #return True
            return False #exception will be logged, but don't stop experiment!
        return False

    def _do_experiments(self):
        """
        Iterate over all experiments and start the experiments
        """
        cmd = ExperimentCommands(self)
        for name, experiment in self.experiments.iteritems():
            self._open_log(experiment=name, experiment_run=0)
            self.onecmd('checkHosts -s')
            self.event_client.send(ExperimentStartEvent())
            experiment.do(cmd)
            self.event_client.send(ExperimentEndEvent())
            self.onecmd('checkHosts')
            time.sleep(10)  # wait some seconds to decrease the probability to miss an event
            self._close_log()
            # FIXME reset environment using snapshots


class WebCLI(SirenCLI):
    def __init__(self, mininet, network_info):
        from siren.eventcollector.livevis import get_livevis_cli_path
        context = zmq.Context()
        self.socket = context.socket(zmq.REP)
        self.socket.bind("ipc://%s" % (get_livevis_cli_path(),))

        SirenCLI.__init__(self, mininet, network_info)  # starts loop, so everything has to be on top of this command

    def _pre_start(self):
        while True:
            command = self.socket.recv()
            print 'Received command:', command
            self.onecmd(command)
            self.socket.send('OK')

        return False
