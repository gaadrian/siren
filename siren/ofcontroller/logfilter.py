from pox.core import core
import logging


class HidePacket(logging.Filter):
    def filter(self, record):
        if record.name == 'packet' and record.getMessage().startswith('(dns) parsing'):
            return 0
        return 1


def launch():
    log = core.getLogger('packet')
    log.addFilter(HidePacket())
