from siren.common.path import get_temp_path


def get_ofcontroller_config_path(name):
    return get_temp_path('ofcontroller', 'config', name + '.pickle', is_file=True)