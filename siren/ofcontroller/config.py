from siren.common.path import get_temp_path


def get_ofcontroller_config_path():
    return get_temp_path('ofcontroller', 'conf.txt', is_file=True)


def write_config(ip):
    with open(get_ofcontroller_config_path(), 'w') as fh:
        fh.write(ip)


def get_compute_queue_delay():
    config = read_config()
    confs = config.split(',')
    for conf in confs:
        parts = conf.split('-', 1)
        if parts[0] == 'cqd':
            return int(parts[1])
    #return 10
    return 1


def read_config():
    with open(get_ofcontroller_config_path(), 'r') as fh:
        return str(fh.readline()).strip()
