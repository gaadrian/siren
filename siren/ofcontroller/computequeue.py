import time
from pox.lib.recoco import Timer
from siren.eventcollector.events import CommentEvent
from siren.ofcontroller.config import get_compute_queue_delay
from siren.ofcontroller.common import log, event_client


class ComputeQueue(object):
    delay = get_compute_queue_delay()

    def __init__(self, routing_graph):
        event = CommentEvent()
        event.notes = 'ComputeQueue.delay: %d' % (ComputeQueue.delay,)
        event_client.send(event)

        self.queue = []
        self.routing_graph = routing_graph

        self.started = False

    def start_timer(self, next_process):
        self.started = True
        Timer(next_process, self.process)

    def add(self, task):
        networks = task.networks
        log.debug('ComputeQueue: queue %s', str(networks))
        for queued_task in self.queue:
            networks = queued_task.new_networks(networks)

            if len(networks) == 0:
                break

        if len(networks) == 0:
            log.debug('ComputeQueue: Duplicate or empty task, drop')
            return

        task.networks = networks
        self.queue.append(task)
        log.debug('ComputeQueue: Tasked networks %s', str(task.networks))
        if not self.started:
            self.start_timer(ComputeQueue.delay)

    def process(self):
        if len(self.queue) > 0:
            log.debug('ComputeQueue: Tasks in queue')
            behind = self.queue[0].created_on + ComputeQueue.delay - time.time()
            task = self.queue.pop(0)
            log.debug('ComputeQueue: compute networks %s [%f, %f]', str(task.networks), time.time(), behind)
            self.routing_graph.compute_flows(task.networks)

        # because length of queue can be changed in the meantime
        if len(self.queue) > 0:
            next_process = self.queue[0].created_on + ComputeQueue.delay - time.time()
            log.debug('ComputeQueue: Scheduling next task in %f seconds', next_process)

            if next_process < 0:
                log.debug('ComputeQueue: next compute task delayed')
                next_process = 0
            self.start_timer(next_process)
        else:
            log.debug('ComputeQueue: No compute task')
            self.started = False


class ComputeTask(object):
    def __init__(self, networks):
        self.created_on = time.time()
        self.networks = networks

    def __contains__(self, task):
        if self.networks is None:
            return True
        elif task.networks is None:
            return False

        return set(task.networks).issubset(set(self.networks))

    def new_networks(self, networks):
        return list(set(networks) - set(self.networks))