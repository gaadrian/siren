import netaddr
import logging
from pox.core import core
from siren.eventcollector.client import EventClient

event_client = EventClient()
log = core.getLogger('controller')
log.setLevel(logging.DEBUG)


class ASPath(object):
    """
    Structure to save an AS Path (AS sequence, AS set)
    """
    def __init__(self, as_sequence, as_set):
        self.as_sequence = as_sequence
        self.as_set = as_set

    def __contains__(self, item):
        return item in self.as_sequence or item in self.as_set

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.as_sequence == other.as_sequence and self.as_set == other.as_set
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __str__(self):
        return '<ASPath: %s %s>' % (str(self.as_sequence), str(self.as_set))

    def __repr__(self):
        return self.__str__()


def network_address(network):
    net = netaddr.IPNetwork(network)
    return'%s/%d' % (net.network, net.prefixlen)