import pprint
import time
import networkx as nx

from pox.core import core
from pox.lib import util as poxutil, util
from pox.lib.revent import EventMixin
from siren.eventcollector.events import SDNConnectionChangeEvent, SDNLinkChangeEvent
from siren.ofcontroller.bgplistener import BGPListener
from siren.ofcontroller.common import ASPath, log, event_client
from siren.ofcontroller.computequeue import ComputeQueue, ComputeTask
from siren.ofcontroller.switch import SendQueue, BorderSwitch


class RoutingGraph(EventMixin):

    EDGE_TYPE_CLUSTER = 'cluster'
    EDGE_TYPE_LEARNED = 'learned'

    NODE_TYPE_SWITCH = 'switch'
    NODE_TYPE_NETWORK = 'network'

    _eventMixin_events = set([
        #RoutingGraphChange,
    ])

    def __init__(self, bgp_listener_socket_path, bgp_sessions, as_numbers, connected_networks):
        EventMixin.__init__(self)

        self.send_queue = SendQueue()

        self.switch_graph = nx.DiGraph()  # to see where to send the packets to
        self.switches = {}
        self.networks = {}  # keeps track of all network addresses in our network, the value of the dict are
                            # the dpids of the switches currently connected (FIXME, remove this values
                            # as they are redundant with the graph)

        log.debug('BGP Listener path %s', bgp_listener_socket_path)
        self.bgp_listener = BGPListener(bgp_listener_socket_path, self)
        self.bgp_sessions = bgp_sessions
        self.connected_networks = connected_networks
        self.compute_queue = ComputeQueue(self)

        self.as_numbers = as_numbers  # dpid->AS number mapping; key: dpid, value: AS number
        self.as_numbers_set = set(as_numbers.values())  # set with all AS numbers of the cluster

        core.openflow.addListeners(self)
        core.listen_to_dependencies(self, ['openflow_discovery'])

    def get_all_switches(self):
        return self.switches.values()

    def _get_all_switch_ids(self):
        return self.switches.keys()

    def get_all_networks(self):
        return self.networks.values()

    def _get_all_network_ids(self):
        return self.networks.keys()

    def _add_network(self, switch_dpid, network, metric, as_path):
        #metric_before = None
        if network in self.networks:
            if switch_dpid in self.networks[network]:
                #metric_before = self.switch_graph[switch_dpid][network]['metric']
                pass
            else:
                self.networks[network].append(switch_dpid)
        else:
            self.networks[network] = [switch_dpid]
            self.switch_graph.add_node(network, type=RoutingGraph.NODE_TYPE_NETWORK)

        ## only update graph, when metric changed, we need >= not > as the as path might has changed
        #if metric_before is None or metric_before >= metric:
        self._add_edge(switch_dpid, network, metric, as_path=as_path)
        self.queue_compute_flows(networks=[network])

    def _remove_network(self, switch_dpid, network):

        log.info('Remove network %s on switch %s', network, switch_dpid)
        if not network in self.networks:
            log.info('Network not present anymore')
            return

        if switch_dpid in self.networks[network]:
            self.networks[network].remove(switch_dpid)
        self._remove_edge(switch_dpid, network, compute=False)

        if len(self.networks[network]) == 0:
            log.info('network now unreachable')
            self.switch_graph.remove_node(network)
            del self.networks[network]
            self._clear_flows(networks=[network])
        else:
            log.info('network still reachable')
            self.queue_compute_flows(networks=[network])

    def xyz_handle_NetworkUpdate(self, event):
        self._add_network(event.switch_dpid, event.network, event.metric, event.as_path)

    def xyz_handle_NetworkWithdraw(self, event):
        self._remove_network(event.switch_dpid, event.network)

    def get_switch(self, dpid):
        if not dpid in self.switches:
            return None
        return self.switches[dpid]

    def _add_switch(self, dpid, connection):
        log.info('Switch dpid=%s up', dpid)
        self.switch_graph.add_node(dpid, type=RoutingGraph.NODE_TYPE_SWITCH)
        self.send_queue.add_connection(dpid, connection)
        self.switches[dpid] = BorderSwitch(self.send_queue.get_send_helper(dpid), dpid, self.bgp_listener, self,
                                           self.connected_networks.get(dpid, []))

        # add BGP sessions
        if dpid in self.bgp_sessions:
            for session in self.bgp_sessions[dpid]:
                log.debug('add BGP session local %s neighbor %s', session['local_ip'], session['neighbor_ip'])
                self.switches[dpid].add_bgp_session(session['local_ip'], session['neighbor_ip'], session['neighbor_as_num'])
        else:
            log.debug('No BGP sessions :( %s %s', dpid, pprint.pformat(self.bgp_sessions))

        #self.get_switch(dpid).addListeners(self) # FIXME wirklich noetig?
        #self.get_switch(dpid).add_mock_network()

        # add connected networks
        #if dpid in self.connected_networks:
        #    for network in self.connected_networks[dpid]:
        #        log.debug('add connected network %s', network['network'])
                # FIXME
                #self.switches[dpid].add_connected_network(network_address(network['network'], event.port, packet.src)


        self.switches[dpid].init_flows()
        #self._clear_flows(switches=[dpid])
        self.queue_compute_flows()

    def _remove_switch(self, dpid):
        log.debug('Switch dpid=%s down', dpid)
        for edge in self.switch_graph.edges(dpid):
            self._remove_edge(*edge, compute=False)

        del self.switches[dpid]
        self.switch_graph.remove_node(dpid)
        self.queue_compute_flows()

    def _check_switch(self, dpid):
        return dpid in self.switches

    def _compute_flows_edge(self, node1, node2):
        """
        Computes all necessary flows when a edge between node1 and node2 has been changed

        :param node1: first node of changed edge
        :param node2: second node of changed edge
        """
        nodes = [node1, node2]
        # check whether one node is a network, then we only have to compute flows for this particular network
        # as all
        for node in nodes:
            if self.switch_graph.node[node]['type'] == RoutingGraph.NODE_TYPE_NETWORK:
                self.queue_compute_flows(networks=[node])
                return
        # recompute all flows
        self.queue_compute_flows()

    def _add_edge(self, node1, node2, metric, as_path=None):
        log.debug('Add edge %s %s', node1, node2)

        assert metric is not None
        # FIXME check that both nodes exist, because of POX bug #108

        if as_path is None:
            as_path = ASPath([], ())
        # prevent multiple edges (same direction) between nodes
        #try:
        if self.switch_graph.has_edge(node1, node2):
            log.debug('Edge exists')
            if not (self.switch_graph[node1][node2]['weight'] == metric and self.switch_graph[node1][node2]['as_path'] == as_path):
                log.debug('Metric or AS path changed')
                self.switch_graph[node1][node2]['weight'] = metric
                self.switch_graph[node1][node2]['as_path'] = as_path
                self._compute_flows_edge(node1, node2)
        else:
        #except KeyError:
            log.debug('Edge does not exist')
            self.switch_graph.add_edge(node1, node2, weight=metric, as_path=as_path)
            self._compute_flows_edge(node1, node2)

    def _remove_edge(self, node1, node2, compute=True):
        if not self.switch_graph.has_edge(node1, node2):
            return
        log.error('Remove edge between %s and %s', node1, node2)
        self.switch_graph.remove_edge(node1, node2)
        if compute:
            self._compute_flows_edge(node1, node2)

    # links: between switches
    def _add_link(self, dpid1, dpid2):
        self._add_edge(dpid1, dpid2, 1)

    def _remove_link(self, dpid1, dpid2):
        self._remove_edge(dpid1, dpid2)

    def _handle_ConnectionUp(self, event):
        log.info('received ConnectionUp')
        if not self._check_switch(event.dpid):
            self._add_switch(event.dpid, event.connection)
        else:
            log.warn('Possible dpid (%s) conflict', poxutil.dpid_to_str(event.dpid))

        e = SDNConnectionChangeEvent()
        e.dpid = event.dpid
        e.added = True
        event_client.send(e)

        log.debug("Switch %s has come up.", poxutil.dpid_to_str(event.dpid))

    def _handle_ConnectionDown(self, event):
        self._remove_switch(event.dpid)

        e = SDNConnectionChangeEvent()
        e.dpid = event.dpid
        e.added = False
        event_client.send(e)

        log.debug('Switch %s down', poxutil.dpid_to_str(event.dpid))

    def _draw_graph(self):
        plt.clf()
        nx.draw(self.switch_graph)
        filename = time.time()
        log.debug('draw graph: %d.png', filename)
        plt.savefig('/home/adrian/maplot/%d.png' % (filename,))

    def _handle_openflow_discovery_LinkEvent(self, event):
        link = event.link
        log.info('LinkEvent: dpid1 %d port %d, dpid2 %d port %d added %d',
                 link.dpid1, link.port1, link.dpid2, link.port2, event.added)
        self.get_switch(link.dpid1).xyz_handle_openflow_discovery_LinkEvent(event) # wichtig, dass es zuerst ist, sonst kann er link gar nicht kennen

        e = SDNLinkChangeEvent()
        e.dpid1 = link.dpid1
        e.port1 = link.port1
        e.dpid2 = link.dpid2
        e.port2 = link.port2
        e.added = event.added
        event_client.send(e)

        if event.added:
            self._add_link(link.dpid1, link.dpid2)
        else:
            self._remove_link(link.dpid1, link.dpid2)

    def _clear_flows(self, switches=None, networks=None):
        if switches is None:
            switches = self._get_all_switch_ids()

        for switch_id in switches:
            self.get_switch(switch_id).clear_flows(networks)

    def _get_base_as_graph(self):
        as_graph = nx.DiGraph()

        # add all AS numbers to the graph
        for dpid, as_number in self.as_numbers.iteritems():
            # skip switches which are not up yet
            if not self.switch_graph.has_node(dpid):
                continue

            if not as_graph.has_node(as_number):
                as_graph.add_node(as_number, dpids=[dpid])
            else:
                as_graph.node[as_number]['dpids'].append(dpid)

        # add internal links
        for as_number, data in as_graph.nodes(data=True):
            for dpid in data['dpids']:
                for neighbor in self.switch_graph.neighbors_iter(dpid):
                    #log.debug('Node %s type %s', dpid, self.switch_graph.node[dpid]['type'])
                    if self.switch_graph.node[neighbor]['type'] == RoutingGraph.NODE_TYPE_SWITCH:
                        neighbor_as_number = self.as_numbers[neighbor]
                        as_graph.add_edge(as_number, neighbor_as_number, weight=1, type=RoutingGraph.EDGE_TYPE_CLUSTER)

        return as_graph

    def _get_as_graph(self, base_as_graph, network):
        as_graph = base_as_graph.copy()

        for predecessor in self.switch_graph.predecessors_iter(network):
            predecessor_as_number = self.as_numbers[predecessor]
            as_path = self.switch_graph[predecessor][network]['as_path']

            current_as_path = ASPath([], ())
            for as_number in as_path.as_sequence:


                # if one as number is in our cluster we stop adding eges, because this node should then know its path, if any
                if as_number in self.as_numbers_set:
                    # use 'concatenated' link, to avoid "merged" external paths
                    # +1, because when AS seq has 2 AS, 3 edges are between the source and target of the 'parent' edge
                    path_weight = len(current_as_path.as_sequence) + 1
                    # do only replace the edge, when the weight of the new path is better. because this external links
                    # should always have at least one AS between, we are not preferring internal links
                    if not as_graph.has_edge(predecessor_as_number, as_number) or path_weight < as_graph[predecessor_as_number][as_number]['weight']:
                        as_graph.add_edge(predecessor_as_number, as_number, as_path=current_as_path,
                                          type=RoutingGraph.EDGE_TYPE_LEARNED, weight=path_weight)

                    break

                current_as_path.as_sequence.append(as_number)
            else:
                # no AS in the path is our cluster

                # use 'concatenated' link, to avoid "merged" external paths, to avoid announcing paths
                # we never learned from neighbors
                as_graph.add_edge(predecessor_as_number, network, as_path=as_path, weight=len(as_path.as_sequence),
                                  type=RoutingGraph.EDGE_TYPE_LEARNED)

        return as_graph

    def _get_policy_graph(self):
        pass

    def _get_dijkstra_path(self, as_graph, source_dpid, network):
        source_as_number = self.as_numbers[source_dpid]

        try:
            # returned list contains source and target node
            as_number_path = nx.dijkstra_path(as_graph, source_as_number, network, weight='weight')
        except nx.NetworkXNoPath:
            path_length = -1
            next_hop = None  # will remove flow
            as_path = None

        # path exists
        else:
            as_path = ASPath([], ())
            previous_as_number = None
            for as_number in as_number_path:
                if not previous_as_number is None:
                    edge = as_graph[previous_as_number][as_number]
                    if edge['type'] == RoutingGraph.EDGE_TYPE_LEARNED:
                        for edge_as_number in edge['as_path'].as_sequence:
                            as_path.as_sequence.append(edge_as_number)

                # we are at our destination network, don't add network CIDR to AS sequence
                if as_number == network:
                    break

                as_path.as_sequence.append(as_number)
                previous_as_number = as_number

            # to check whether we learned that path, we need as_number_path, because that's the path
            # dijkstra calculated and is the path in the graph. (there are no external AS numbers which are not in
            # the graph [as nodes])
            next_as_in_graph = as_number_path[1]
            if as_graph[source_as_number][next_as_in_graph]['type'] == RoutingGraph.EDGE_TYPE_LEARNED:
                # dpid of next hop switch, set to its own dpid, when the network is connected (or bgp) to that switch
                next_hop = source_dpid
            else:
                # we are inside the clusters, therefore the following is the same
                next_as = next_as_in_graph
                assert len(as_graph.node[next_as]['dpids']) == 1, 'No support for multiple routers per AS yet'
                next_hop = as_graph.node[next_as]['dpids'][0]

            path_length = len(as_path.as_sequence)

        return source_dpid, next_hop, as_path, path_length

    def queue_compute_flows(self, networks=None):
        if networks is None:
            networks = self._get_all_network_ids()

        compute_task = ComputeTask(networks)
        self.compute_queue.add(compute_task)

    def compute_flows(self, networks):
        switches = self._get_all_switch_ids()
        base_as_graph = self._get_base_as_graph()

        for network in networks:
            # skip computation when network has been removed
            if not self.switch_graph.has_node(network):
                log.debug('Network %s not in our graph anymore', network)
                continue
            as_graph = self._get_as_graph(base_as_graph, network)

            switch_paths = []
            # calculate all paths to the destination
            for switch_id in switches:
                switch_paths.append(self._get_dijkstra_path(as_graph, switch_id, network))

            # sort by the path length
            switch_paths = sorted(switch_paths, key=lambda item: item[3])

            for switch_id, next_hop, as_path, num_hops in switch_paths:
                flows = []
                log.debug('path from %d to %s: %s', switch_id, network, pprint.pformat(as_path))
                log.debug('successors of %s: %s', switch_id, str(self.switch_graph.successors(switch_id)))
                flows.append((network, next_hop, as_path))
                self.get_switch(switch_id).install_flows(flows)

    def bgp_update(self, event):
        for switch in self.get_all_switches():
            switch.xyz_handle_BGPUpdate(event)

    def bgp_neighbor(self, event):
        for switch in self.get_all_switches():
            switch.xyz_handle_BGPNeighborState(event)