import json
import socket
from pox.lib.ioworker import RecocoIOLoop
from pox.lib.revent import EventMixin

from siren.ofcontroller.events import BGPUpdate, BGPNeighborState
from siren.ofcontroller.common import log


class BGPListener(EventMixin):
    """
    Listens for BGP updates on the unix socket from ExaBGP (json encoder)
    exabgp has to be "freshly" started

    Restrictions ExaBGP:
    - Peerings are identified by neighbor ip only -> impossible to have multiple session to the same neighbor ip

    see: pox / pox / lib / recoco / examples.py
    """
    EXABGP_EOL = '\n'
    WAIT_TIME = 5

    _eventMixin_events = set([
        BGPUpdate,
        BGPNeighborState,
    ])

    def __init__(self, socket_filename, graph):
        EventMixin.__init__(self)

        log.debug('Start BGPListener')

        self.read_buffer = ''
        self.graph = graph

        loop = RecocoIOLoop()
        #self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        # FIXME pox should be server and exabgp should try to reconnect and restart?
        #self.sock.connect(socket_filename)
        #self.sock.sendall('restart\n')

        #import time


        #log.info('ExaBGP restarted, waiting %d seconds', BGPListener.WAIT_TIME)
        #time.sleep(BGPListener.WAIT_TIME)
        #self.sock.close()
        #log.info('ExaBGP reconnecting')
        log.debug('create socket')
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        log.debug('connect')
        self.sock.connect(socket_filename)

        log.debug('create worker')
        self.worker = loop.new_worker(self.sock)
        self.worker.rx_handler = self._receive
        log.debug('start loop')
        loop.start()
        log.debug('loop started')

    def announce(self, local_ip, neighbor_ip, network, add, as_path):

        # https://github.com/Exa-Networks/exabgp/blob/ca46461313abbfed26dfa599265d1d8878b6f086/lib/exabgp/configuration/validation.py
        # https://github.com/Exa-Networks/exabgp/blob/64b120311b6a8bf4385871a360ea38666babd580/etc/exabgp/processes/tcp-server
        if add:
            command = 'announce'
            if len(as_path.as_sequence) > 0:  # FIXME add as_set
                as_sequence = ' '.join([str(_) for _ in as_path.as_sequence])
            else:
                as_sequence = ' self '
            cmd_post = ' as-path [%s]' % (as_sequence,)
        else:
            command = 'withdraw'
            cmd_post = ''

        message = 'neighbor %s %s route %s next-hop %s%s' % (neighbor_ip, command, network, local_ip, cmd_post)
        log.debug('Send announcement: %s', message)
        self._send(message)

    def gracefully_restart_session(self): #TODO
        pass

    def _send(self, message):
        self.worker.send(message + '\n')

    def _process_message(self, message_encoded):
        log.debug('%s', message_encoded)
        # FIXME: json only returns unicode! convert to str! (pox has problems with unicode)
        try:
            message = json.loads(message_encoded)
        except ValueError:
            if message_encoded in ['restart in progress']:
                return

            log.error('Unknown message from ExaBGP: %s', message_encoded)
            return
        neighbor = message['neighbor']
        local_ip = '0.0.0.0'  # FIXME
        neighbor_ip = neighbor['ip']

        # for ExaBGP encoding see exabgp/lib/exabgp/reactor/api/encoding.py

        if 'state' in neighbor:
             #{u'exabgp': u'2.0',
             # u'neighbor': {u'ip': u'11.2.0.1', u'state': u'up'},
             # u'time': 123123123}

            self.graph.bgp_neighbor(BGPNeighborState(str(local_ip), str(neighbor_ip), str(neighbor['state'])))
            log.debug('recv neighbor state local %s, neighbor %s, state %s', str(local_ip), str(neighbor_ip),
                      str(neighbor['state']))
        elif 'update' in neighbor:
            #{u'exabgp': u'2.0',
            # u'neighbor': {u'ip': u'11.2.0.1',
            #               u'update': {u'announce': {u'ipv4 unicast': {u'12.0.3.0/24': {u'next-hop': u'11.2.0.1'}}},
            #                           u'attribute': {u'as-path': [[1, 4], []],
            #                                          u'atomic-aggregate': False,
            #                                          u'origin': u'igp'}}},
            # u'time': 123123123}

            update = neighbor['update']
            if 'announce' in update:
                next_hops = update['announce']
                attributes = update['attribute']
                for next_hop, nets in next_hops.iteritems():
                    for network, attr in nets.iteritems():
                        log.debug('recv announce %s, local %s, neighbor %s, next-hop %s, as-path %s', str(network), str(local_ip), str(neighbor_ip), str(next_hop), str(
                            attributes['as-path']))
                        # FIXME, add ARP support to add different next hop (we have to know the port of the ip address)
                        assert next_hop == neighbor_ip, 'next-hop %s, neighbor %s' % (next_hop, neighbor_ip)
                        self.graph.bgp_update(BGPUpdate(str(local_ip), str(neighbor_ip), str(network), True,
                                                        attributes['as-path'], set(attributes.get('as-set', []))))

            elif 'withdraw' in update:
                nets = update['withdraw']['ipv4 unicast']
                for network, attr in nets.iteritems():

                    log.debug('recv announce %s, local %s, neighbor %s', str(network), str(local_ip), str(neighbor_ip))
                    self.graph.bgp_update(BGPUpdate(str(local_ip), str(neighbor_ip), str(network), False))

            else:
                log.warn('Unknown BGP update action: %s', message_encoded)
        else:
            log.warn('Unknown BGP message: %s', message_encoded)


    def _receive(self, worker):
        log.info('received sth')
        self.read_buffer += worker.read()
        # messages are terminated by \n\r\n
        messages = self.read_buffer.split(self.EXABGP_EOL)

        # in case one messages is incomplete
        self.read_buffer = messages.pop()

        for message in messages:
            self._process_message(message)