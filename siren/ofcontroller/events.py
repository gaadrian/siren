from pox.lib.revent import Event
from siren.ofcontroller.common import log


class BGPEvent(Event):
    def __init__(self, local_ip, neighbor_ip):
        Event.__init__(self)

        # to be sure (as pox does not support utf8)
        assert isinstance(local_ip, basestring) and isinstance(neighbor_ip, basestring), 'No unicode because of POX'

        self.local_ip = local_ip
        self.neighbor_ip = neighbor_ip


class BGPUpdate(BGPEvent):
    def __init__(self, local_ip, neighbor_ip, network, added, as_sequence=None, as_set=None):
        BGPEvent.__init__(self, local_ip, neighbor_ip)

        assert isinstance(local_ip, basestring) and isinstance(neighbor_ip, basestring) \
            and isinstance(network, basestring), 'No unicode because of POX'

        # Assumption!
        self.next_hop = neighbor_ip

        self.network = network
        self.added = added

        # http://packetlife.net/blog/2008/sep/19/bgp-route-aggregation-part-1/
        # http://wiki.nil.com/BGP_route_aggregation/Preserving_AS_numbers
        self.as_sequence = as_sequence
        self.as_set = as_set


class BGPNeighborState(BGPEvent):
    """
    This event is raised when a BGP Neighbor changes the state
    """

    STATE_UP = 'up'
    STATE_DOWN = 'down'
    STATE_CONNECTED = 'connected'

    def __init__(self, local_ip, neighbor_ip, state):
        BGPEvent.__init__(self, local_ip, neighbor_ip)

        assert isinstance(state, basestring), 'No unicode because of POX'

        assert state == BGPNeighborState.STATE_UP or state == BGPNeighborState.STATE_DOWN \
            or state == BGPNeighborState.STATE_CONNECTED, 'Unknown state: %s' % (str(state),)

        self.state = state


class BGPAnnounce(BGPEvent):
    """
    Raise this event to announce a network through BGP
    """
    def __init__(self, local_ip, neighbor_ip, network):
        BGPEvent.__init__(self, local_ip, neighbor_ip)

        self.network = network


class NetworkEvent(Event):
    # FIXME: move to appropriate place
    SOURCE_CONNECTED = 0
    SOURCE_BGP = 1

    def __init__(self, switch_dpid, network, added):
        Event.__init__(self)
        self.switch_dpid = switch_dpid
        self.network = network
        #self.hops = hops
        self.added = added


class NetworkUpdate(NetworkEvent):
    """
    Used to track changes, also used for Network Updates
    """
    def __init__(self, switch_dpid, network, metric, as_path):
        NetworkEvent.__init__(self, switch_dpid, network, True)
        self.metric = metric
        self.as_path = as_path
        log.debug('NetworkUpdate %s', network)


class NetworkWithdraw(NetworkEvent):
    def __init__(self, switch_dpid, network):
        NetworkEvent.__init__(self, switch_dpid, network, False)
        log.debug('NetworkWithdraw %s', network)