import pprint
import sys
import networkx as nx
from pox.lib.addresses import EthAddr
from pox.lib.recoco import Timer
from pox.lib.revent import EventMixin
from pox.openflow import libopenflow_01 as of, libopenflow_01

from siren.eventcollector.events import ErrorEvent, SDNUpdateEvent
from siren.ofcontroller.common import ASPath, log, event_client, network_address
from siren.ofcontroller.events import BGPAnnounce, NetworkEvent, BGPNeighborState, NetworkUpdate, NetworkWithdraw


class BorderSwitch(EventMixin):
    """
    Assumption: BGP only announces paths through itself (neighbor_ip == next_hop) (the port is important)
    NO BGP MULTIPATH

    """

    _eventMixin_events = set([
        BGPAnnounce,
    ])

    def __init__(self, send, dpid, bgp_listener, routing_graph, connected_networks):
        EventMixin.__init__(self)

        self.send = send
        self.dpid = dpid
        self.bgp_listener = bgp_listener
        self.routing_graph = routing_graph
        self.connected_networks = connected_networks

        # networks which originate at this switch (cluster-wise)
        self.networks = NetworkTable(self.dpid, routing_graph)
        self.networks.addListeners(self.routing_graph)

        self.addListeners(self.bgp_listener)

        # all bgp sessions
        self.bgp_sessions = {}
        self._bgp_session_id_counter = 0

        # links to other switches
        self.links = {}  # key: dpid, value: ports[]

        self._ports_to_switches = []

        self.flows = {}  # key: arp [boolean]
        self._flow_cookie_counter = 0

        self.bgp_port = None

        self.all_ports = {} #added dict with all ports and features

    def get_preset_connected_network(self, local_ip):
        for network in self.connected_networks:
            if network['local_ip'] == local_ip:
                return network['network']

        return None

    def send_name(self):
        pkt = of.ethernet()
        pkt.src = EthAddr('00:00:00:00:00:00')
        pkt.dst = EthAddr('FF:FF:FF:AA:AA:12')

        pass

    def handle_arp_request(self, event, packet):
        log.debug('handle_arp_request on switch %s', self.dpid)
        network = self.get_preset_connected_network(str(packet.next.protodst))
        log.debug('connected_network %s (local ip %s) %s', network, str(packet.next.protodst), str(
            self.connected_networks))
        if not network is None and not self.is_port_to_switch(event.port):
            # copied from POX wiki
            arp_reply = of.arp()
            arp_reply.hwsrc = EthAddr('00:00:00:12:34:56')
            arp_reply.hwdst = packet.src
            arp_reply.opcode = of.arp.REPLY
            arp_reply.protosrc = packet.payload.protodst
            arp_reply.protodst = packet.payload.protosrc
            ether = of.ethernet()
            ether.type = of.ethernet.ARP_TYPE
            ether.dst = packet.src
            ether.src = EthAddr('00:00:00:12:34:56')
            ether.payload = arp_reply
            self.send_packet(ether, event.port)
            log.debug('it is a router address')
            self.add_connected_network(network, event.port, packet.src)
            return True
        log.debug('sth different')
        return False

    def add_connected_network(self, network, port, mac):
        log.debug('Add connected network %s', network)
        path = ConnectedNetworkTablePath(network, port, mac)
        self.networks.add_path(path)

    def remove_connected_network(self, network, port=None):
        selector = PathSelector(network=network, source=NetworkEvent.SOURCE_CONNECTED, port=port)
        self.networks.remove_path(selector)

    def add_bgp_session(self, local_ip, neighbor_ip, neighbor_as_num):
        self.bgp_sessions[self._bgp_session_id_counter] = {'local_ip': local_ip,
                                                           'neighbor_ip': neighbor_ip,
                                                           'neighbor_as_num': neighbor_as_num,
                                                           'port': None,
                                                           'neighbor_mac': None}
        self._bgp_session_id_counter += 1

    def get_bgp_session_by_neighbor_ip(self, neighbor_ip, include_session_id=False):
        return self._get_bgp_session_by('neighbor_ip', neighbor_ip, include_session_id=include_session_id)

    def get_bgp_session_by_local_ip(self, local_ip, include_session_id=False):
        return self._get_bgp_session_by('local_ip', local_ip, include_session_id=include_session_id)

    def get_bgp_session_by_port(self, port, include_session_id=False):
        return self._get_bgp_session_by('port', port, include_session_id=include_session_id)

    def _get_bgp_session_by(self, key, value, include_session_id=False):
        for session_id, session in self.bgp_sessions.iteritems():
            if session[key] == value:
                if include_session_id:
                    return session_id, session
                else:
                    return session

        if include_session_id:
            return None, None
        else:
            return None

    def _add_bgp_path(self, network, neighbor_ip, as_sequence, as_set):
        session_id, session = self.get_bgp_session_by_neighbor_ip(neighbor_ip, include_session_id=True)
        log.debug('Add BGP path: %s %s port %d', self.dpid, network, session['port'])
        assert session['port'] is not None, 'BGP session port unknown: ' + str(session)
        path = BGPNetworkTablePath(network, session['port'], session['neighbor_mac'], session_id, as_sequence, as_set)
        self.networks.add_path(path)

    def _remove_bgp_path(self, neighbor_ip, network=None):
        session_id, session = self.get_bgp_session_by_neighbor_ip(neighbor_ip, include_session_id=True)
        if session_id is None:
            event = ErrorEvent()
            event.notes = 'Neighbor IP can not be found when removing networks'
            event_client.send(event)
            return
        selector = PathSelector(network=network, source=NetworkEvent.SOURCE_BGP, bgp_session_id=session_id)
        self.networks.remove_path(selector)

    def xyz_handle_BGPNeighborState(self, event):
        # FIXME check whether ExaBGP sends all routes after neighbor has been down
        # remove all routes to a neighbor if there's a state change (up or down)
        session_id, session = self.get_bgp_session_by_neighbor_ip(event.neighbor_ip, include_session_id=True)
        if session_id is not None:
            if event.state == BGPNeighborState.STATE_DOWN:
                log.debug('BGPNeighborState changed: %s %s at %s', event.neighbor_ip, event.state, self.dpid)
                self._remove_bgp_path(event.neighbor_ip)
                return
            elif event.state == BGPNeighborState.STATE_UP:
                self._bgp_announce_all_neighbor(session)
                return

    def xyz_handle_BGPUpdate(self, event):
        """

        :type event: sc.ofcontroller.events.BGPUpdate
        :param event: Event to process
        """
        if self.get_bgp_session_by_neighbor_ip(event.neighbor_ip) is not None:
            if event.added:
                self._add_bgp_path(event.network, event.neighbor_ip, event.as_sequence, event.as_set)
            else:
                self._remove_bgp_path(event.neighbor_ip, event.network)

    def init_flows(self):
        self.clear_flows(None)

        for session in self.bgp_sessions.itervalues():
            self.attract_ip(session['local_ip'])
            self.attract_ip(session['neighbor_ip'])

        # Drop IPv6 traffic
        mod = of.ofp_flow_mod()
        mod.match.dl_type = 0x86DD
        # OFPP_NONE not supported on older openvswitch
        #mod.actions.append(of.ofp_action_output(port=of.OFPP_NONE))
        self.send(mod)

    def attract_ip(self, ip, delete=False):
        log.info('Attract IP %s on dpid %s', ip, self.dpid)
        self._attract_ip(ip, True, delete)
        self._attract_ip(ip, False, delete)

    def _attract_ip(self, ip, arp, delete):
        mod = of.ofp_flow_mod()
        if delete:
            mod.command = of.OFPFC_DELETE_STRICT

        if arp:
            mod.match.dl_type = 0x806
        else:
            mod.match.dl_type = 0x800
        mod.priority = 1000  # important rule
        mod.match.nw_src = ip
        mod.actions.append(of.ofp_action_output(port=of.OFPP_CONTROLLER))
        self.send(mod)

    def install_flows(self, flows):
        log.debug('Flows to install on switch dpid %d: %s', self.dpid, pprint.pformat(flows))
        for network, next_hop, as_path in flows:
            if next_hop is None:

                # no path to this network
                self._remove_flow(network=network)

            else:
                if next_hop is not self.dpid:
                    # packet needs to be sent to a different switch
                    log.debug('link to port')
                    port = self.get_link_port(next_hop)
                    log.debug("current links %s", pprint.pformat(self.links))
                    mac = None
                else:
                    # switch is connected to that network (directly or BGP)
                    log.debug('connected')
                    port, mac = self.networks.get_network(network).get_best_hop()
                log.debug('switch dpid %d neighbor dpid %d port %s', self.dpid, next_hop, str(port))
                flow_installed = self._install_flow(network, port, mac=mac, as_path=as_path, bgp=True)

                # only announce when a new flow has been installed or the AS path has changed
                if flow_installed or as_path != self.flows[False][network][2]:
                    self._bgp_announce(network, True, next_hop=port, as_path=as_path)

    def clear_flows(self, networks=None):
        log.debug('clear_flows on switch %s', self.dpid)
        if networks is None:
            # flush all flows
            self._remove_flow()
        else:
            for network in networks:
                try:
                    arp = False
                    self.flows[arp][network]
                except KeyError:
                    log.debug('no flows to this network (not arp)')

                self._remove_flow(network=network)

    def _bgp_announce(self, network, add, next_hop=None, as_path=None):
        """
        Announce or withdraw a route to all neighbors of the switch

        :param network: network in CIDR notation
        :param add: True for announce, False for withdraw
        :type add bool
        :param next_hop: port of the next hop
        :param as_path: AS path of this path
        :type as_path ASPath
        """

        for bgp_session in self.bgp_sessions.itervalues():
            self._bgp_announce_network(bgp_session, network, add, as_path)

        event = SDNUpdateEvent()
        event.dpid = self.dpid
        event.network = str(network)
        if add:
            event.action = SDNUpdateEvent.ACTION_ANNOUNCE
        else:
            event.action = SDNUpdateEvent.ACTION_WITHDRAW
        if isinstance(as_path, ASPath):
            event.as_sequence = as_path.as_sequence
            event.as_set = as_path.as_set

        event_client.send(event)

    def _bgp_announce_network(self, bgp_session, network, add, as_path):
        # check that the next hop is not the bgp router to avoid loops
        if not as_path is None and bgp_session['neighbor_as_num'] in as_path:
            add = False

        self.bgp_listener.announce(bgp_session['local_ip'], bgp_session['neighbor_ip'], network, add, as_path)

    def _bgp_announce_all_neighbor(self, bgp_session):
        """
        Announces all routes of the

        :param bgp_session: bgp session dict
        """
        log.debug('BGP announce all routes to neighbor %s', bgp_session['neighbor_ip'])

        for network, flow in self.flows.setdefault(False, {}).iteritems():
            as_path = flow[2]
            bgp = flow[3]

            if bgp:
                log.debug('announce %s', network)
                self._bgp_announce_network(bgp_session, network, True, as_path)

    def _bgp_withdraw_all(self):
        """
        Withdraw all networks on all neighbors
        """
        log.debug('BGP withdraw everything')
        for bgp_session in self.bgp_sessions.itervalues():
            for network, flow in self.flows.setdefault(False, {}).iteritems():
                bgp = flow[3]
                if bgp:
                    self._bgp_announce_network(bgp_session, network, False, None)

    def _install_flow(self, network, port, mac=None, arp=False, as_path=None, bgp=False):
        """
        does *NOT* take care of BGP announcements, as we also use this function to redirect bgp traffic


        :param network: network in CIDR notation
        :param port: port of next hop
        :param mac:
        :param arp:
        :param as_path: AS path belonging to this path if there is any
        :return: returns whether a new flow had to be installed on the switch (this also checks the out port)
        """
        assert port is not None, 'port is None: %s' % (network,)

        replacement = False
        log.debug('swdpid %d install flow for %s on port %d', self.dpid, network, port)
        if network in self.flows.setdefault(arp, {}):
            replacement = True
            if self.flows[arp][network][0] == port:
                # the same flow already installed
                log.debug('flow already installed arp=%d', int(arp))
                #log.debug(pprint.pformat(self.flows[arp]))
                return False

        mod = of.ofp_flow_mod()
        if replacement:
            mod.command = of.OFPFC_MODIFY_STRICT
        else:
            mod.command = of.OFPFC_ADD
        mod.cookie = self._flow_cookie_counter
        # smaller networks should have higher priority (to work with BGP)
        mod.priority = int(network[network.index('/')+1:])
        mod.idle_timeout = of.OFP_FLOW_PERMANENT
        mod.hard_timeout = of.OFP_FLOW_PERMANENT
        if not arp:
            mod.match.dl_type = 0x800
        else:
            mod.match.dl_type = 0x806
        assert isinstance(network, basestring), 'Only basestring, POX does not support unicode'
        mod.match.nw_dst = network

        if mac is not None:
            mod.actions.append(of.ofp_action_dl_addr.set_dst(EthAddr(mac)))
        mod.actions.append(of.ofp_action_output(port=port))

        self.send(mod)
        log.debug('flow installed')
        self.flows[arp][network] = (port, self._flow_cookie_counter, as_path, bgp)
        self._flow_cookie_counter += 1
        return True

    def _remove_flow(self, network=None):
        """
        *does* take care of BGP Withdraws

        :param network: network in CIDR notation; if None delete all networks
        """
        arp = False
        log.debug('_remove_flow %s on %s', str(network), self.dpid)

        # unfortunately openflow 1.0 does not support cookie mask
        mod = of.ofp_flow_mod()
        mod.command = of.OFPFC_DELETE
        if network is not None:
            if network not in self.flows.setdefault(arp, {}):
                return
            mod.match.dl_type = 0x800
            mod.match.nw_dst = network
            del self.flows[arp][network]
            if not arp:
                self._bgp_announce(network, False)
        else:
            self._bgp_withdraw_all()
            self.flows.setdefault(arp, {}).clear()

        self.send(mod)

    def send_packet(self, data, port):
        msg = of.ofp_packet_out(data=data, action=of.ofp_action_output(port=port))
        self.send(msg)

    def flood_packet(self, data):
        return self.send_packet(data, of.OFPP_ALL)

    def is_port_to_switch(self, port):
        try:
            self._ports_to_switches.index(port)
            return True
        except ValueError:
            return False

    def try_learn_bgp_port(self, src_ip, port, mac, packet, flood=True):
        log.debug('try learn bgp port ip %s dpid %d port %d', src_ip, self.dpid, port )

        session = self.get_bgp_session_by_neighbor_ip(src_ip)
        if session is not None:
            log.debug('Found in neighbor')
            self.attract_ip(src_ip, delete=True)
            session['port'] = port
            session['neighbor_mac'] = mac
            log.debug('learned bgp port rip %s dpid %d port %d', src_ip, self.dpid, port)
            # we only install
            self._install_flow(src_ip + '/32', port)
            self._install_flow(src_ip + '/32', port, arp=True)
            if flood:
                self.flood_packet(packet)
            return True


        session = self.get_bgp_session_by_local_ip(src_ip)
        if session is not None:
            log.debug('BGP found in local')
            self.attract_ip(src_ip, delete=True)
            if flood:
                self.flood_packet(packet)
            self.bgp_port = port
            for session in self.bgp_sessions.itervalues():
                self._install_flow(session['local_ip'] + '/32', self.bgp_port)
                self._install_flow(session['local_ip'] + '/32', self.bgp_port, arp=True)

            return True

        log.debug('found nothing %s', pprint.pformat(self.bgp_sessions))
        return False


    def get_link_port(self, dpid):
        #FIXME mutlipath
        if dpid in self.links:
            return self.links[dpid][0]
        return None

    def xyz_handle_openflow_discovery_LinkEvent(self, event):
        log.debug('-----------------------------------------------------------------------------------x0')
        link = event.link
        log.debug('hallo2: %s', pprint.pformat(link))
        if link.dpid1 == self.dpid:
            if event.added:
                self._ports_to_switches.append(link.port1)
                if link.dpid2 not in self.links:
                    self.links[link.dpid2] = []

                self.links[link.dpid2].append(link.port1)
            else:
                self._ports_to_switches.remove(link.port1)
                self.links[link.dpid2].remove(link.port1)

                if len(self.links[link.dpid2]) == 0:
                    del self.links[link.dpid2]


class NetworkTable(EventMixin):
    """
    Keeps track of paths to all destinations of a switch
    """

    SOURCE_CONNECTED = 0
    SOURCE_BGP = 1

    _eventMixin_events = set([
        NetworkUpdate,
        NetworkWithdraw,
    ])

    def __init__(self, switch_dpid, routing_graph):
        EventMixin.__init__(self)

        self.networks = {}
        self.switch_dpid = switch_dpid
        self.routing_graph = routing_graph

    def get_network(self, network):
        try:
            return self.networks[network]
        except:
            raise Exception(pprint.pformat(self.networks))

    def add_path(self, table_path):
        log.debug('Add Path (dpid %d): to %s, %s', self.switch_dpid, table_path.network, pprint.pformat(self.networks))
        if table_path.network not in self.networks:
            self.networks[table_path.network] = NetworkTablePaths()

        need_update, path_update, as_path = self.networks[table_path.network].add_path(table_path)

        if need_update:
            self.routing_graph.xyz_handle_NetworkUpdate(NetworkUpdate(self.switch_dpid, table_path.network, path_update, as_path))

    def remove_path(self, path_select):
        log.debug('Remove Path (dpid %s): dst %s bgp %s, %s', self.switch_dpid, path_select.network, str(path_select.bgp_session_id), pprint.pformat(self.networks))

        # when selector has no network specified, iterate over all networks and try the selector
        if path_select.network is None:
            for network, paths in self.networks.iteritems():
                if paths.remove_path(path_select):
                    self.routing_graph.xyz_handle_NetworkWithdraw(NetworkWithdraw( self.switch_dpid, network))

            return

        if path_select.network not in self.networks:
            return

        log.debug('remove %s', path_select.network)
        if self.networks[path_select.network].remove_path(path_select):
            del self.networks[path_select.network]
            log.debug('raise NetworkWithdraw event %s', path_select.network)
            self.routing_graph.xyz_handle_NetworkWithdraw(NetworkWithdraw(self.switch_dpid, path_select.network))
        else:
            # FIXME only send an update when metric or as path changed
            metric, next_hop, as_path = self.networks[path_select.network].get_best()
            log.debug('raise NetworkUpdate to update metric of %s (path might changed)', path_select.network)
            self.routing_graph.xyz_handle_NetworkUpdate(NetworkUpdate(self.switch_dpid, path_select.network, metric, as_path))


class NetworkTablePaths(EventMixin):
    """
    Restrictions: only one port per Network! (also for connected networks)
    """

    def __init__(self):
        self.paths = []

    def get_best(self):
        best_metric = sys.maxint
        best_hop = None
        best_as_path = None

        for path in self.paths:
            path_metric = path.get_metric()
            if path_metric < best_metric:
                best_metric = path_metric
                best_hop = path.get_next_hop()
                best_as_path = path.get_as_path()

        return best_metric, best_hop, best_as_path

    def get_best_metric(self):
        return self.get_best()[0]

    def get_best_hop(self):
        return self.get_best()[1]

    def get_best_path(self):
        return self.get_best()[2]

    def add_path(self, path):
        """
        :rtype Integer or None
        :param path:
        :return: first item: new best metric or None if the best metric has not changed
        """
        log.debug('##########################Add path %s', str(path))

        # dublicate path
        if path in self.paths:
            log.debug('path already known')
            return False, None, None


        # when we receive a BGP announce with a new path, then this is a withdrawal to the old path
        # therefore we remove all paths of the bgp neighbor to the network
        bgp_path = False
        if isinstance(path, BGPNetworkTablePath):
            path_select = PathSelector()
            path_select.network = path.network
            path_select.source = NetworkEvent.SOURCE_BGP
            path_select.bgp_session_id = path.bgp_session_id
            self.remove_path(path_select)
            bgp_path = True

        metric_before = self.get_best_metric()

        self.paths.append(path)

        new_path_metric = path.get_metric()

        # when the metric of the new path is better than we had or it is a BGP path, because we deleted
        # a path, we have to send an update, could be improved by better tracking the deleted path
        if new_path_metric < metric_before or bgp_path:
            need_update = True
        else:
            need_update = False

        return need_update, self.get_best_metric(), self.get_best_path()

    def remove_path(self, path_select):
        """


        :rtype : Boolean
        :param path_select:
        :return: True if no path is available anymore, otherwise False
        """
        #for path in self.paths:
        #    print path.as_sequence

        self.paths = [path for path in self.paths if not path_select.matches(path)]
        #print len(self.paths)

        if len(self.paths) == 0:
            return True
        else:
            return False


class PathSelector(object):
    """
    Selector to select paths in NetworkTable
    """
    def __init__(self, network=None, source=None, port=None, bgp_session_id=None):
        self.network = network
        self.source = source
        self.port = port
        self.bgp_session_id = bgp_session_id

    def matches(self, path):
        network_match = (self.network is None or self.network == path.network)
        source_match = (self.source is None or self.source == path.SOURCE)
        port_match = (self.port is None or self.port == path.port)
        bgp_session_match = (self.bgp_session_id is None or (path.SOURCE == NetworkEvent.SOURCE_BGP
                                                             and self.bgp_session_id == path.bgp_session_id))
        #print self.network, network_match, self.source, source_match, self.bgp_session_id, path.bgp_session_id, bgp_session_match
        return network_match and source_match and bgp_session_match


class NetworkTablePath(object):

    def __init__(self, network, port, mac):
        self.network = network_address(network)
        self.port = port
        self.mac = mac

    def get_metric(self):
        raise NotImplementedError()

    def get_next_hop(self):
        return self.port, self.mac

    def get_as_path(self):
        return ASPath([], ()) # FIXME ???? oder noch eigene AS nummer einfuegen?


class BGPNetworkTablePath(NetworkTablePath):

    SOURCE = NetworkEvent.SOURCE_BGP

    def __init__(self, network, port, mac, bgp_session_id, as_sequence, as_set):
        NetworkTablePath.__init__(self, network, port, mac)

        self.bgp_session_id = bgp_session_id  # needed for filtering
        self.as_sequence = as_sequence
        self.as_set = as_set

    def get_metric(self):
        if len(self.as_set) > 0:
            as_set_weight = 1
        else:
            as_set_weight = 0

        return len(self.as_sequence) + as_set_weight

    def get_as_path(self):
        return ASPath(self.as_sequence, self.as_set)

    def __str__(self):
        return '<BGPNetworkTablePath %s %s>' % (self.network, str(self.as_sequence))

    def __eq__(self, other):
        if not isinstance(other, BGPNetworkTablePath):
            return False

        return self.bgp_session_id == other.bgp_session_id and self.as_sequence == other.as_sequence \
            and self.as_set == other.as_set

    def __ne__(self, other):
        return not self.__eq__(other)


class ConnectedNetworkTablePath(NetworkTablePath):

    SOURCE = NetworkEvent.SOURCE_CONNECTED

    def get_metric(self):
        return 0

    def __eq__(self, other):
        return isinstance(other, ConnectedNetworkTablePath)

    def __str__(self):
        return '<ConnectedNetworkTablePath: %s>' % (self.network,)


class SendQueue(object):
    def __init__(self):
        self.queue = []
        self.connections = {}
        self.wait = False
        self.current_xid = 100
        self.timer = None

    def add_connection(self, dpid, connection):
        log.info('add connection dpid %s', dpid)
        self.connections[dpid] = connection
        connection.addListeners(self)

    def send(self, dpid, msg):
        # we just send the message out, because otherwise the queue gets to long
        self.connections[dpid].send(msg)
        return

        self.add_to_queue(dpid, msg)

        if not self.wait:
            self._send_next()

    def get_send_helper(self, dpid):
        return lambda msg: self.send(dpid, msg)

    def add_to_queue(self, dpid, msg):
        self.queue.append((dpid, msg))

    def _send_next(self):
        self.wait = True

        if len(self.queue) == 0:
            log.debug('Send queue empty :)')
            self.wait = False
            return

        log.debug('Send a msg')
        dpid, msg = self.queue.pop(0)
        log.debug('msg: %s, %s', dpid, msg.show())
        self.connections[dpid].send(msg)
        barrier = of.ofp_barrier_request()
        self.current_xid += 1
        barrier.xid = self.current_xid
        log.debug('send xid %d (queue len %d)', barrier.xid, len(self.queue))
        self.connections[dpid].send(barrier)
        self.timer = Timer(5, self._timer_expired)

    def _stop_timer(self):
        if isinstance(self.timer, Timer):
            self.timer.cancel()
            self.timer = None

    def _handle_BarrierIn(self, event):
        if not event.dpid in self.connections:
            log.error('Unknown BarrierIn dpid %s', event.dpid)
            return

        if self.current_xid == event.xid:
            log.debug('correct xid %d', event.xid)
            self._stop_timer()
            self._send_next()
        else:
            # a wrong xid
            log.debug('Wrong xid: recv %d expected %d', event.xid, self.current_xid)

    def _handle_ErrorIn(self, event):
        log.error('sendqueue: received ErrorIn')
        event = ErrorEvent()
        event.notes = 'received ErrorIn'
        event_client.send(event)

    def _timer_expired(self):
        log.error('sendqueue: timer expired')
        event = ErrorEvent()
        event.notes = 'timer expired'
        event_client.send(event)


