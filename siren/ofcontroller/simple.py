"""
Important:
- NO MultiPath (per direction) -> e.g. only one link between switches


- zuerst einmal anpingen bevor in connected networks aufnehmen

Some parts copied or adapted from Vasileios
"""

import pickle
import pprint
import sys
import copy

from pox.core import core                     # Main POX object
import pox.openflow.libopenflow_01 as of      # OpenFlow 1.0 library
import pox.lib.packet as pkt                  # Packet parsing/construction
import pox.lib.util as poxutil              # Various util functions
from pox.lib.revent import *
from pox.lib.recoco import *

from siren.eventcollector.events import ErrorEvent, SDNToBGPPortIsDownEvent
from siren.ofcontroller.graph import RoutingGraph
from siren.ofcontroller.common import log, event_client
from siren.ofcontroller import get_ofcontroller_config_path


BGP_PORT = 179

def _go_up(event):
    log.info("BGP application ready")


class ClusterController(EventMixin):

    def __init__(self, bgp_name):
        log.info('Controller started, bgp_name=%s', bgp_name)
        #import pydevd
        #pydevd.settrace('localhost', port=30000, stdoutToServer=True, stderrToServer=True)


        config_filename = get_ofcontroller_config_path(bgp_name)
        with open(config_filename) as fh:
            config = pickle.load(fh)

        assert 'socket_path' in config
        assert 'bgp_sessions' in config
        assert 'as_numbers' in config
        assert 'connected_networks' in config

        EventMixin.__init__(self)

        # full packets needed!
        core.openflow.miss_send_len = 0xffff



        log.debug('RoutingGraph')
        self.routing_graph = RoutingGraph(config['socket_path'], config['bgp_sessions'], config['as_numbers'],
                                          config['connected_networks'])

        log.debug('addListener')
        core.openflow.addListeners(self)

        core.Interactive.variables['graph'] = self.routing_graph._draw_graph
        core.Interactive.variables['bgpsession'] = self._print_bgp_sessions
        log.debug('init end')

    def _print_bgp_sessions(self):
        for switch in self.routing_graph.get_all_switches():
            print switch.dpid
            pprint.pprint(switch.bgp_sessions)

    def _handle_PortStatus(self, event):
        log.debug('PortStatus')
        log.debug('PortStatus dpid=' + str(event.dpid) + ' port=' + str(event.port) + ' ' + str(event))
        log.debug(str(event.ofp.desc.__dict__.iteritems()))
        switch = self.routing_graph.get_switch(event.dpid)
        if event.port not in switch.all_ports:
            switch.all_ports[event.port] = {}
        old_port_status_dict = copy.deepcopy(switch.all_ports[event.port])
        for (feature,value) in event.ofp.desc.__dict__.iteritems():
            switch.all_ports[event.port][feature] = value

        if event.modified:
            log.info('Port of dpid ' + str(event.dpid) + ' is modified')
            new_port_config = event.ofp.desc.config
            old_port_config = None
            if 'config' in old_port_status_dict:
                old_port_config = switch.all_ports[event.port]['config']
            if (old_port_config!=1) and (new_port_config==1):
                log.info('Port going down')
                #restart bgp session if external-facing port
                bgp_session_id, bgp_session = switch.get_bgp_session_by_port(event.port, include_session_id=True)
                if (bgp_session_id!=None):
                    log.info('Need to gracefully restart the bgp session with id ' + str(bgp_session_id))
                    #switch._remove_bgp_path(bgp_session['neighbor_ip'])
                    e = SDNToBGPPortIsDownEvent()
                    e.dpid = event.dpid
                    e.port = event.port
                    event_client.send(e)

    def _handle_PacketIn(self, event):
        switch = self.routing_graph.get_switch(event.dpid)
        if switch is None:
            log.debug('Packet received but switch not ready')
            return
        packet = event.parsed

        # do not care about IPv6
        if packet.type == packet.IPV6_TYPE:
            return

        # do not care about broadcast traffic
        if packet.type == packet.IP_TYPE:
            if str(packet.next.srcip) == '0.0.0.0' and str(packet.next.dstip) == '255.255.255.255':
                return

        log.warn('PacketIn: %s, src %s, dst %s dpid %d', str(packet), str(packet.src), str(packet.dst), event.dpid)

        if packet.type == packet.LLDP_TYPE:
            # handled by component openflow discovery -> do nothing
            return

        # try finding bgp switch ports
        if packet.type == packet.ARP_TYPE:
            if switch.try_learn_bgp_port(str(packet.next.protosrc), event.port, str(packet.src), packet):
                # packet has been flooded
                return

            log.debug('not found dst ip %s', str(packet.payload.protodst))

            if packet.payload.opcode == of.arp.REQUEST:
                if switch.handle_arp_request(event, packet):
                    return

        ip_packet = packet.find('ipv4')
        if ip_packet is not None:
            # try to learn it from IP packets, in case ARP is already known. (hint: only packets which don't have a
            # flow yet are forwarded to this controller/function)
            switch.try_learn_bgp_port(str(ip_packet.srcip), event.port, str(packet.src), packet, flood=False)

        # intercept bgp communication
        #bgp_session = switch.get_bgp_session_by_port(event.port)
        #if bgp_session is not None:
        #    is_bgp_packet = ip_packet is not None and ip_packet.dstip == bgp_session['local_ip'] \
        #                      and ip_packet.srcip == bgp_session['neighbor_ip'] \
        #                      and ip_packet.protocol == ip_packet.TCP_PROTOCOL \
        #        and (ip_packet.next.srcport == BGP_PORT or ip_packet.next.dstport == BGP_PORT)
        #
        #    icmp_packet = packet.find('icmp')
        #    is_icmp_packet = icmp_packet is not None
        #    if packet.type == packet.ARP_TYPE or is_bgp_packet or is_icmp_packet:
        #        self.pxp.inject(event.data)
        #        log.debug( "m->c: SUCC: forwarded packet to emulated bgp host")
        #        return

        if packet.type == packet.ARP_TYPE :
            log.warn('do not know what to do :( %s, src %s, dst %s dpid %d', str(packet), str(packet.next.protosrc), str(packet.next.protodst), event.dpid)
        if packet.type == packet.IP_TYPE:
            log.warn('do not know what to do :( %s, src %s, dst %s dpid %d', str(packet), str(packet.next.srcip), str(packet.next.dstip), event.dpid)

    def _bgp_neighbor_ip_to_switch_port(self, neighbor_ip):
        for switch in self.routing_graph.get_all_switches():
            session = switch.get_bgp_session_by_neighbor_ip(neighbor_ip)

            if session is not None:
                return switch, session['port']

        return None, None

    def _bgp_local_ip_to_switch(self, local_ip):
        for switch in self.routing_graph.get_all_switches():
            session = switch.get_bgp_session_by_local_ip(local_ip)

            if session is not None:
                return switch

        return None

    def _handle_PacketCapture(self, obj, data, sec, usec, length):
        packet = pkt.ethernet(data)

        ip_packet = packet.find('ipv4')
        if ip_packet is not None:
            switch, port = self._bgp_neighbor_ip_to_switch_port(ip_packet.dstip)

            if port is not None:
                switch.send_packet(data, port)
                log.debug('c->m: SUCC: forwarded msg')
            else:
                log.warn('Unable to forward BGP packet, as switch port is unknown')

        arp_packet = packet.find('arp')
        if arp_packet is not None:
            switch, port = self._bgp_neighbor_ip_to_switch_port(packet.next.protodst)

            if port is None:
                switch = self._bgp_local_ip_to_switch(packet.next.protosrc)
                if switch is not None:
                    log.debug('Port for ARP packet unknown, flood on switch (dpid %d)', switch.dpid)
                    switch.flood_packet(data)
                else:
                    log.warn('Can not forward ARP message as this src ip address (%s) is unknown', packet.next.protosrc)
            else:
                log.debug('Port for ARP known dpid %d port %d', switch.dpid, port)
                switch.send_packet(data, port)


# http://stackoverflow.com/a/6598286/2468685
def my_exception_hook(exctype, value, tb):
    event = ErrorEvent()
    event.notes = traceback.format_exception(exctype, value, tb)
    event_client.send(event)

    sys.__excepthook__(exctype, value, traceback)


@poxutil.eval_args
def launch(bgp_name):
    sys.excepthook = my_exception_hook
    try:
        core.registerNew(ClusterController, bgp_name)
    except:
        traceback.print_exc()
        event = ErrorEvent()
        event.notes = traceback.format_exc()
        event_client.send(event)


        # 1. arp packet warten emul bgp ip gesucht wird
        # 2. ganzer port nicht voll (es muessen ja auch packete durch) zu emul bgp umleiten auch rueckwaerts
        # 3.


