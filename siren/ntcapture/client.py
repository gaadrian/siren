from scapy.all import *
from scapy.contrib.bgp import *
import struct

from siren.eventcollector.client import EventClient
from siren.eventcollector.events import BGPUpdateCaptureEvent

BGP_PORT = 179


class NetworkTrafficCapture:

    def __init__(self):
        event_client = EventClient()

        def sniff_bgp(pkt):
            src_ip, dst_ip = None, None

            while True:
                if isinstance(pkt, IP):
                    src_ip = pkt.src
                    dst_ip = pkt.dst

                if isinstance(pkt, BGPUpdate):
                    as_sequence = []
                    for path in pkt.total_path:
                        if path.type == 2:
                            path_segment_type, path_segment_length = struct.unpack('!BB', path.value[:2])
                            data = path.value[2:]
                            as_sequence = []
                            for i in range(path_segment_length):
                                as_num, = struct.unpack('!I', data[i*4:(i+1)*4])
                                as_sequence.append(as_num)

                    event = BGPUpdateCaptureEvent()
                    event.as_sequence = as_sequence
                    event.src_ip = src_ip
                    event.dst_ip = dst_ip

                    for network in pkt.nlri:
                        event.network = '%s/%d' % (network[1], network[0])
                        event.action = BGPUpdateCaptureEvent.ACTION_ANNOUNCE

                        event_client.send(event)

                    for network in pkt.withdrawn:
                        event.network = '%s/%d' % (network[1], network[0])
                        event.action = BGPUpdateCaptureEvent.ACTION_WITHDRAW

                        event_client.send(event)

                pkt = pkt.payload
                if isinstance(pkt, NoPayload):
                    break

        sniff(prn=sniff_bgp, filter='port %d' % (BGP_PORT,), store=0)