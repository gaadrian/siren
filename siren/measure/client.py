from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor, task
import time

from siren.eventcollector.client import EventClient
from siren.eventcollector.events import MeasureStartEvent, MeasureErrorEvent

# adapted from
# https://twistedmatrix.com/documents/current/core/howto/udp.html

class Client(DatagramProtocol):
    def __init__(self, host, buffer_seconds):
        self.host = host
        self.buffer = buffer_seconds
        self.sequence_nr = 0
        self.session_name = None
        self.send_interval = 0.002
        self.num_ack = 0
        self.acks = []
        self._st = {}

        self.event_client = EventClient()
        self.server_replied = False
        self.start_event_uid = None
        self.timeout_call = None
        reactor.addSystemEventTrigger('before', 'shutdown', self.send_end)

    def startProtocol(self):
        port = 20000

        event = MeasureStartEvent()
        event.source = None  # TODO
        event.target = self.host
        self.event_client.send(event)

        self.start_event_uid = event.get_uid()

        print 'Start measuring %s' % (self.host,)
        self.transport.connect(self.host, port)
        self.transport.write('start;%s;%f;%f;%s' % (event.get_uid(), self.buffer, self.send_interval, self.host))

        # stop client when there has not been a reply after 5 seconds
        self.timeout_call = reactor.callLater(5, reactor.stop)

    def send_packet(self):
        self.transport.write('%s;%f;%d' % (self.session_name, time.time(), self.sequence_nr))
        self.sequence_nr += 1
        if self.sequence_nr % 1000 == 0:
            print '.', self.sequence_nr - self.num_ack, self.sequence_nr - len(self.acks), \
                self.sequence_nr - len(set(self.acks))
        try:
            self._st[int(time.time())] += 1
        except KeyError:
            self._st[int(time.time())] = 1

    def send_end(self):
        if not self.server_replied:
            event = MeasureErrorEvent()
            event.start_event = self.start_event_uid
            event.notes = 'Server never replied'
            self.event_client.send(event)

        self.transport.write('end;%s;%d' % (self.session_name, self.sequence_nr))

    def datagramReceived(self, data, (host, port)):
        if data.startswith('name;'):
            self.server_replied = True
            if not self.timeout_call is None:
                self.timeout_call.cancel()
                self.timeout_call = None

            name, self.session_name = data.split(';', 1)
            lc = task.LoopingCall(self.send_packet)
            lc.start(self.send_interval)

        elif data.startswith('ack;'):
            ack, sequence_nr = data.split(';', 1)
            self.num_ack += 1
            self.acks.append(sequence_nr)


def start_client(target, buffer_seconds):
    # 0 means any port, we don't care in this case
    reactor.listenUDP(0, Client(target, buffer_seconds))
    reactor.run()
