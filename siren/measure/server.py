from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor
import time

from siren.eventcollector.client import EventClient
from siren.eventcollector.events import MeasureResultEvent, MeasureErrorEvent, ErrorEvent


# parts adapted from
# https://twistedmatrix.com/documents/current/core/howto/udp.html

class Server(DatagramProtocol):
    def __init__(self):
        self.sessions = {}
        self.event_client = EventClient()

    def generate_session(self, uid, buffer_time, send_interval, target_address):
        name = len(self.sessions)
        self.sessions[name] = {'start_event_uid': uid, 'sequence_nr': 0, 'missing': [], 'start_time': time.time(),
                               'buffer_time': buffer_time, 'send_interval': send_interval, 'send_time': {},
                               'packets_discarded_buffer': 0, 'target_address': target_address}
        print self.sessions[name]['buffer_time']
        return name

    def datagramReceived(self, datagram, (host, port)):
        if datagram.startswith('start;'):
            print datagram
            start, uid, buffer_time, send_interval, target_address = datagram.split(';', 4)
            buffer_time = float(buffer_time)
            send_interval = float(send_interval)
            name = self.generate_session(uid, buffer_time, send_interval, target_address)
            self.transport.write('name;%d' % (name,), (host, port))
            return
        elif datagram.startswith('end;'):
            end, session_name, num_packets = datagram.split(';', 2)

            session_name = int(session_name)
            num_packets = int(num_packets)

            if not session_name in self.sessions:
                event = ErrorEvent()
                event.notes = 'Received measure stop, but never started this session (%s)' % (str(session_name),)
                self.event_client.send(event)
                return

            packets_discarded_buffer = self.sessions[session_name]['packets_discarded_buffer']
            packets_lost = len(self.sessions[session_name]['missing']) - packets_discarded_buffer
            end_time = time.time()

            if not self.sessions[session_name]['sequence_nr'] == num_packets:
                event = MeasureErrorEvent()
                event.notes = 'Sequence number not equal to number of packets'
            else:
                event = MeasureResultEvent()
                event.source = host
                event.target = self.sessions[session_name]['target_address']
                event.start_time = self.sessions[session_name]['start_time']
                event.end_time = end_time
                event.packets_sent = num_packets
                event.packets_discarded_buffer = packets_discarded_buffer
                event.packets_lost = packets_lost
                event.send_interval = self.sessions[session_name]['send_interval']
                event.buffer_time = self.sessions[session_name]['buffer_time']
                event.loss_start = None
                event.loss_end = None

                if not packets_lost == 0:
                    first_loss = min(self.sessions[session_name]['missing'])
                    last_loss = max(self.sessions[session_name]['missing'])

                    try:
                        event.loss_start = self.sessions[session_name]['send_time'][first_loss - 1]
                        event.loss_end = self.sessions[session_name]['send_time'][last_loss + 1]
                    except KeyError:
                        event = MeasureErrorEvent()
                        event.notes = 'Start and end of the measurement have to be successful'

            event.start_event = self.sessions[session_name]['start_event_uid']
            self.event_client.send(event)

            self.sessions[session_name] = None
        else:
            session_name, send_time, sequence_nr = datagram.split(';', 2)
            session_name = int(session_name)
            send_time = float(send_time)
            sequence_nr = int(sequence_nr)

            self.sessions[session_name]['send_time'][sequence_nr] = send_time

            if self.sessions[session_name]['buffer_time'] < time.time() - send_time:
                self.sessions[session_name]['packets_discarded_buffer'] += 1
                return

            if sequence_nr == self.sessions[session_name]['sequence_nr']:
                self.sessions[session_name]['sequence_nr'] = sequence_nr + 1
            elif sequence_nr < self.sessions[session_name]['sequence_nr']:
                self.sessions[session_name]['missing'].remove(sequence_nr)
            elif sequence_nr > self.sessions[session_name]['sequence_nr']:
                self.sessions[session_name]['missing'].extend(
                    range(self.sessions[session_name]['sequence_nr'], sequence_nr))
                self.sessions[session_name]['sequence_nr'] = sequence_nr + 1

            self.transport.write('ack;%d' % (sequence_nr,), (host, port))


def start_server():
    reactor.listenUDP(20000, Server())
    reactor.run()
