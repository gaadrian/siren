import json
import sys

from siren.eventcollector.client import EventClient
from siren.eventcollector.events import BGPUpdateEvent, BGPStateEvent


def launch():
    """
    BGP monitor: collects BGP messages from ExaBGP and logs them. The communication with ExaBGP
    is done using stdin/stdout.
    """

    event_client = EventClient()

    while True:
        line = sys.stdin.readline()
        if not line:
            print 'shutting down'
            break

        message = json.loads(line)

        neighbor = message['neighbor']
        neighbor_ip = neighbor['ip']

        if 'state' in neighbor:
             #{u'exabgp': u'2.0',
             # u'neighbor': {u'ip': u'11.2.0.1', u'state': u'up'},
             # u'time': 123123123}
            event = BGPStateEvent()
            event.neighbor_ip = str(neighbor_ip)
            event.state = str(neighbor['state'])

            event_client.send(event)
        elif 'update' in neighbor:
            #{u'exabgp': u'2.0',
            # u'neighbor': {u'ip': u'11.2.0.1',
            #               u'update': {u'announce': {u'ipv4 unicast': {u'12.0.3.0/24': {u'next-hop': u'11.2.0.1'}}},
            #                           u'attribute': {u'as-path': [[1, 4], []],
            #                                          u'atomic-aggregate': False,
            #                                          u'origin': u'igp'}}},
            # u'time': 123123123}

            update = neighbor['update']
            if 'announce' in update:
                next_hops = update['announce']
                attributes = update['attribute']
                for next_hop, nets in next_hops.iteritems():
                    for network, attr in nets.iteritems():
                        assert next_hop == neighbor_ip, 'next-hop %s, neighbor %s' % (next_hop, neighbor_ip)

                        event = BGPUpdateEvent()
                        event.neighbor_ip = str(neighbor_ip)
                        event.network = str(network)
                        event.action = BGPUpdateEvent.ACTION_ANNOUNCE
                        event.as_sequence = attributes['as-path']
                        event.as_set = attributes.get('as-set', [])
                        event_client.send(event)

            elif 'withdraw' in update:
                nets = update['withdraw']['ipv4 unicast']
                for network, attr in nets.iteritems():
                    event = BGPUpdateEvent()
                    event.neighbor_ip = str(neighbor_ip)
                    event.network = str(network)
                    event.action = BGPUpdateEvent.ACTION_WITHDRAW
                    event_client.send(event)
            else:
                print 'Unknown action'
