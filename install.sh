#!/bin/bash

cd "$(dirname "$0")"

# copied from http://www.cyberciti.biz/tips/shell-root-user-check-script.html
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
apt-get update
#apt-get dist-upgrade -y
apt-get install -y sudo net-tools help2man quagga xterm python-matplotlib libzmq-dev telnet python-pip python-dev pypy rsync
pip install Mako networkx pyzmq netaddr twisted tornado pysubnettree numpy setuptools-scm zmq tornado mako matplotlib

update-rc.d quagga disable
echo "VTYSH_PAGER=cat" >> /etc/environment
apt-get remove python-scapy

chmod +x libs/mininet/util/install.sh
ln -s ../examples libs/mininet/mininet/examples
cd libs/mininet
make develop
cd ../..

libs/mininet/util/install.sh -nv
./fixperm.sh
