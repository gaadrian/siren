from siren.network.cluster import BGPCluster, SDNCluster
from siren.network.internet import InterNetwork
from nwsetup import NetworkSetupAbstract, ExperimentAbstract
from siren.network.peering import Peering

DEFAULT_DELAY = 24


class SimpleExperiment(ExperimentAbstract):

    def do(self, tb):
        # start measuring loss between h5.0 and h9.0 and vice versa
        m39 = tb.measure_start('h5.0', 'h9.0')
        m93 = tb.measure_start('h9.0', 'h5.0')

        tb.cli_cmd('wait 10')  # wait a couple of seconds
        tb.cli_cmd('marker convergence_start')  # set marker for easier analysis
        tb.cli_cmd('link r1.0 r9.0 down')  # bring down the link
        tb.cli_cmd('wait 10')
        tb.cli_cmd('waitconverged')  # wait until the network converged
        tb.cli_cmd('marker convergence_end')

        # stop measuring
        tb.measure_stop(m39)
        tb.measure_stop(m93)


class NetworkSetup(NetworkSetupAbstract):

    def __init__(self, config_str):
        super(NetworkSetup, self).__init__(config_str)
        self.add_experiment('break19', SimpleExperiment())

    def get_network(self):
        # read configuration string
        sdn_members = self.config.get('sdn', '')
        rh_members = self.config.get('rh', '')

        net = InterNetwork()
        sdn = SDNCluster()
        bgp = BGPCluster()

        # add ASes to the network
        for i in range(1, 11):
            if str(i) in sdn_members:
                    net.add_as(i, sdn)
            else:
                net.add_as(i, bgp)

        # add peering links
        net.add_peering(1, 10, delay=DEFAULT_DELAY)
        net.add_peering(10, 2, delay=DEFAULT_DELAY)
        net.add_peering(2, 3, delay=DEFAULT_DELAY)
        net.add_peering(3, 4, delay=DEFAULT_DELAY)
        net.add_peering(4, 5, delay=DEFAULT_DELAY)
        net.add_peering(5, 1, delay=DEFAULT_DELAY)
        net.add_peering(2, 6, delay=DEFAULT_DELAY)
        net.add_peering(6, 7, delay=DEFAULT_DELAY)
        net.add_peering(7, 8, delay=DEFAULT_DELAY)
        net.add_peering(8, 9, delay=DEFAULT_DELAY)
        net.add_peering(1, 9, delay=DEFAULT_DELAY)

        # add hosts to the network
        if not '5' in rh_members:
            net.add_prefix(5, add_host=True)
        net.add_prefix(9, add_host=True, track=True)  # in visualization track this host
        return net
