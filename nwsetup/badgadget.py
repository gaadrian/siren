from nwsetup import NetworkSetupAbstract
from siren.network.cluster import BGPCluster
from siren.network.internet import InterNetwork
from siren.network.peering import Peering

DEFAULT_DELAY = 24


class NetworkSetup(NetworkSetupAbstract):

    def __init__(self, config_str):
        super(NetworkSetup, self).__init__(config_str)

    def get_network(self):
        net = InterNetwork()

        cluster = BGPCluster()

        for i in range(1, 5):
            net.add_as(i, cluster)

        net.add_peering(1, 2, relationship=Peering.REL_CUSTOMER_UPSTREAM, delay=DEFAULT_DELAY)
        net.add_peering(2, 3, relationship=Peering.REL_CUSTOMER_UPSTREAM, delay=DEFAULT_DELAY)
        net.add_peering(3, 1, relationship=Peering.REL_CUSTOMER_UPSTREAM, delay=DEFAULT_DELAY)

        net.add_peering(4, 1, delay=DEFAULT_DELAY)
        net.add_peering(4, 2, delay=DEFAULT_DELAY)
        net.add_peering(4, 3, delay=DEFAULT_DELAY)

        net.add_prefix(4, track=True)

        return net
