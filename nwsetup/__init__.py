class ExperimentAbstract(object):

    def do(self, toolbox):
        raise NotImplementedError()


class NetworkSetupAbstract(object):
    def __init__(self, config_str):
        self.config = NetworkSetupAbstract.analyse_config(config_str)
        self.experiments = {}

    def add_experiment(self, name, experiment):
        if isinstance(experiment, ExperimentAbstract):
            self.experiments[name] = experiment
        else:
            raise TypeError('Wrong type, experiments need to inherit from')

    def get_experiments(self, names):
        results = {}
        names = [_.strip() for _ in names.split(',')]
        for name in names:
            assert(name in self.experiments)
            results[name] = self.experiments[name]
        return results

    def get_network(self):
        raise NotImplementedError()

    @staticmethod
    def analyse_config(config):
        if config == 'default':
            return {}
        pairs = config.split(',')
        result = {}
        for pair in pairs:
            name, value = pair.split('-', 1)
            result[name] = value
        return result
