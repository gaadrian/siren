from siren.network.cluster import BGPCluster, SDNCluster
from siren.network.internet import InterNetwork
from nwsetup import NetworkSetupAbstract, ExperimentAbstract
from siren.network.peering import Peering
import networkx as nx
import matplotlib.pyplot as plt
import time
import random

DEFAULT_DELAY = 24

graph_types = [
                'random_regular',
                'erdos_renyi',
                'newman_watts_strogatz',
                'barabasi_albert'
]

node_numbers = [8,16,32]


class WithdrawalExperiment(ExperimentAbstract):
    def do(self, tb):
        tb.cli_cmd('marker convergence_start')
        tb.cli_cmd('link r1.0 r40000.0 down')
        tb.cli_cmd('wait 10')
        tb.cli_cmd('waitconverged')
        tb.cli_cmd('marker convergence_end')


class NetworkSetup(NetworkSetupAbstract):

    DESCRIPTION = "Setups with random graphs"
    CONFIG_PARAMETER = [('gt', 'type of random graph', 'erdos_renyi'),
                        ('gp', 'graph-specific parameters', ''),
                        ('n', 'number of nodes', 8),
                        ('sp', 'SDN percentage', 50),
                        ('u', 'upstream ases', 'f')]

    def __init__(self, config_str):
        super(NetworkSetup, self).__init__(config_str)
        self.add_experiment('wd', WithdrawalExperiment())

    def create_graph(self):
        G = None
        graph_type = str(self.config.get('gt'))
        graph_params = str(self.config.get('gp')).split('_')
        num_nodes = int(self.config.get('n'))
        seed = int(time.time())

        print 'graph type: %s' % (graph_type)
        print 'number of nodes: %d' % (num_nodes)
        print 'misc parameters: %s' % (graph_params)

        if (graph_type == 'random_regular'):
            degree = int(graph_params[0])
            G = nx.random_regular_graph(degree,num_nodes,seed)
            while (not nx.is_connected(G)):
                seed = int(time.time())
                G = nx.random_regular_graph(degree,num_nodes,seed)

        elif (graph_type == 'erdos_renyi'):
            p = float(graph_params[0])
            G = nx.erdos_renyi_graph(num_nodes,p,seed)
            while (not nx.is_connected(G)):
                seed = int(time.time())
                G = nx.erdos_renyi_graph(num_nodes,p,seed)

        elif (graph_type == 'newman_watts_strogatz'):
            k = int(graph_params[0])
            p = float(graph_params[1])
            G = nx.newman_watts_strogatz_graph(num_nodes,k,p,seed)
            while (not nx.is_connected(G)):
                seed = int(time.time())
                G = nx.newman_watts_strogatz_graph(num_nodes,k,p,seed)

        elif (graph_type == 'barabasi_albert'):
            m = int(graph_params[0])
            G = nx.barabasi_albert_graph(num_nodes,m,seed)
            while (not nx.is_connected(G)):
                seed = int(time.time())
                G = nx.barabasi_albert_graph(num_nodes,m,seed)

        else:
            print "Unknown graph type!"
            return None
        return G

    def plot_graph(self, G):
        nx.draw(G)
        plt.draw()
        plt.title('type=%s, n=%d, params=%s' % (str(self.config.get('gtype')),
                                                int(self.config.get('n')),
                                                str(self.config.get('gp'))))
        plt.show()

    def get_network(self):
        G = self.create_graph()
        #self.plot_graph(G)

        num_nodes = int(self.config.get('n'))
        num_sdn_members = num_nodes * float(int(self.config.get('sp', 50)))/100
        num_sdn_members = int(num_sdn_members)
        num_bgp_members = num_nodes - num_sdn_members
        print num_nodes, 'sdn:', num_sdn_members, 'bgp:', num_bgp_members

        net = InterNetwork()
        if (num_sdn_members>0):
            sdn_cluster = SDNCluster()
        #if (num_bgp_members>0):
        bgp_cluster = BGPCluster()
        for i in range(1, num_nodes+1):
            if i <= num_bgp_members: #the lowest numbered nodes are BGP
                net.add_as(i, bgp_cluster)
            else: #the highest numbered nodes are SDN
                net.add_as(i, sdn_cluster)
            net.add_prefix(i, add_host=True)

        graph_nodes = sorted(G.nodes())
        graph_edges = G.edges()

        start_from_0 = False
        if (graph_nodes[0]==0):
            start_from_0 = True

        for edge in graph_edges:
            if (start_from_0):
                net.add_peering(edge[0]+1, edge[1]+1, delay=DEFAULT_DELAY)
            else:
                net.add_peering(edge[0], edge[1], delay=DEFAULT_DELAY)

        net.add_as(40000, BGPCluster()) #the IP prefix "announcer"
        upstream_1 = 1
        candidate_upstreams = [i for i in range(2, num_nodes+1)]
        upstream_2 = random.choice(candidate_upstreams)
        #upstreams = self.config.get('u', 'f').split('+')
        #for upstream in upstreams:
        #    if upstream == 'f': #first upstream = BGP node
        #        upstream = 1
        #    elif upstream == 'l': #second upstream = SDN node
        #        upstream = num_nodes
        #    else:
        #        upstream = int(upstream)
        print 'upstream 1 (primary) : %d' % (upstream_1)
        print 'upstream 2 (backup) : %d' % (upstream_2)
        net.add_peering(40000, upstream_1, relationship=Peering.REL_CUSTOMER_UPSTREAM)
        net.add_peering(40000, upstream_2, path_prepending_a=True, relationship=Peering.REL_CUSTOMER_UPSTREAM)
        net.add_prefix(40000, add_host=True, track=True) #track forwarding towards this host
        return net