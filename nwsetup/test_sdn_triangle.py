from siren.network.cluster import BGPCluster, SDNCluster
from siren.network.internet import InterNetwork
from nwsetup import NetworkSetupAbstract, ExperimentAbstract
from siren.network.peering import Peering
import random


DEFAULT_DELAY = 24


class WithdrawalExperiment(ExperimentAbstract):
    def do(self, tb):
        tb.cli_cmd('marker convergence_start')
        tb.cli_cmd('link r1.0 r40000.0 down')
        tb.cli_cmd('wait 10')
        tb.cli_cmd('waitconverged')
        tb.cli_cmd('marker convergence_end')

class NetworkSetup(NetworkSetupAbstract):

    DESCRIPTION = "A test SDN triangle network setup"

    def __init__(self, config_str):
        super(NetworkSetup, self).__init__(config_str)
        self.add_experiment('wd', WithdrawalExperiment())

    def get_network(self):
        num_nodes = 5
        num_sdn_members = 3
        num_bgp_members = 2
        print num_nodes, 'sdn:', num_sdn_members, 'bgp:', num_bgp_members

        #ADD CLUSTERS
        net = InterNetwork()
        sdn_cluster = SDNCluster()
        bgp_cluster = BGPCluster()

        #ADD ASES
        net.add_as(1, bgp_cluster)
        net.add_as(2, bgp_cluster)
        net.add_as(3, sdn_cluster)
        net.add_as(4, sdn_cluster)
        net.add_as(5, sdn_cluster)

        #ADD PREFIXES
        net.add_prefix(1, add_host=True)
        net.add_prefix(2, add_host=True)
        net.add_prefix(3, add_host=True)
        net.add_prefix(4, add_host=True)
        net.add_prefix(5, add_host=True)

        #FORM PEERING TRIANGLE
        net.add_peering(1,2, delay=DEFAULT_DELAY)
        net.add_peering(1,3, delay=DEFAULT_DELAY)
        net.add_peering(2,4, delay=DEFAULT_DELAY)
        net.add_peering(3,4, delay=DEFAULT_DELAY)
        net.add_peering(3,5, delay=DEFAULT_DELAY)
        net.add_peering(4,5, delay=DEFAULT_DELAY)

        #ADD IP PREFIX ANNOUNCER RELATED TO EXPERIMENT
        net.add_as(40000, BGPCluster()) #the IP prefix "announcer"
        net.add_peering(40000, 1, relationship=Peering.REL_CUSTOMER_UPSTREAM)
        net.add_peering(40000, 2, path_prepending_a=True, relationship=Peering.REL_CUSTOMER_UPSTREAM)
        net.add_prefix(40000, add_host=True, track=True) #track forwarding towards this host

        return net
