from siren.network.cluster import BGPCluster, SDNCluster
from siren.network.internet import InterNetwork
from nwsetup import NetworkSetupAbstract, ExperimentAbstract
from siren.network.peering import Peering
import random


DEFAULT_DELAY = 24


class DemoExperiment(ExperimentAbstract):
    def do(self, tb):
        pass


class WithdrawalExperiment(ExperimentAbstract):
    def do(self, tb):
        tb.cli_cmd('marker convergence_start')
        tb.cli_cmd('link r1.0 r40000.0 down')
        tb.cli_cmd('wait 10')
        tb.cli_cmd('waitconverged')
        tb.cli_cmd('marker convergence_end')


class ReannounceExperiment(ExperimentAbstract):
    def do(self, tb):
        tb.cli_cmd('link r1.0 r40000.0 down')
        tb.cli_cmd('wait 20')
        tb.cli_cmd('marker convergence_start')
        tb.cli_cmd('link r1.0 r40000.0 up')
        tb.cli_cmd('wait 60')
        tb.cli_cmd('waitconverged')
        tb.cli_cmd('marker convergence_end')


class AnnouncementExperiment(ExperimentAbstract):
    def do(self, tb):
        tb.cli_cmd('marker convergence_start')
        tb.cli_cmd('bgpannounce r40000.0 9.0.0.0/24')
        tb.cli_cmd('wait 60')
        tb.cli_cmd('waitconverged')
        tb.cli_cmd('marker convergence_end')


class NetworkSetup(NetworkSetupAbstract):

    DESCRIPTION = "A clique network setup"
    CONFIG_PARAMETER = [('n', 'number of nodes', 8),
                        ('sp', 'SDN percentage', 50),
                        ('u', 'upstream ases', 'f')]

    def __init__(self, config_str):
        super(NetworkSetup, self).__init__(config_str)
        self.add_experiment('wd', WithdrawalExperiment())

    def get_network(self):
        num_nodes = int(self.config.get('n', 8))
        num_sdn_members = num_nodes * float(int(self.config.get('sp', 50)))/100
        num_sdn_members = int(num_sdn_members)
        num_bgp_members = num_nodes - num_sdn_members
        print num_nodes, 'sdn:', num_sdn_members, 'bgp:', num_bgp_members

        net = InterNetwork()
        if (num_sdn_members>0):
            sdn_cluster = SDNCluster()
        #if (num_bgp_members>0):
        bgp_cluster = BGPCluster()
        for i in range(1, num_nodes+1):
            if i <= num_bgp_members: #the lowest numbered nodes are BGP
                net.add_as(i, bgp_cluster)
            else: #the highest numbered nodes are SDN
                net.add_as(i, sdn_cluster)
            net.add_prefix(i, add_host=True)

            for u in range(1, i): #full mesh (clique) of nodes
            #    if not ((u == 1 and i == num_nodes) or (i == 1 and u == num_nodes)):
                net.add_peering(u, i, delay=DEFAULT_DELAY)


        net.add_as(40000, BGPCluster()) #the IP prefix "announcer"
        upstream_1 = 1
        candidate_upstreams = [i for i in range(2, num_nodes+1)]
        upstream_2 = random.choice(candidate_upstreams)
        #upstreams = self.config.get('u', 'f').split('+')
        #for upstream in upstreams:
        #    if upstream == 'f': #first upstream = BGP node
        #        upstream = 1
        #    elif upstream == 'l': #second upstream = SDN node
        #        upstream = num_nodes
        #    else:
        #        upstream = int(upstream)
        print 'upstream 1 (primary) : %d' % (upstream_1)
        print 'upstream 2 (backup) : %d' % (upstream_2)
        net.add_peering(40000, upstream_1, relationship=Peering.REL_CUSTOMER_UPSTREAM)
        net.add_peering(40000, upstream_2, path_prepending_a=True, relationship=Peering.REL_CUSTOMER_UPSTREAM)
        net.add_prefix(40000, add_host=True, track=True) #track forwarding towards this host
        return net