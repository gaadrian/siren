from nwsetup import NetworkSetupAbstract
from siren.network.cluster import BGPCluster, SDNCluster
from siren.network.internet import InterNetwork
from siren.network.peering import Peering

DEFAULT_DELAY = 24


class NetworkSetup(NetworkSetupAbstract):

    def __init__(self, config_str):
        super(NetworkSetup, self).__init__(config_str)

    def get_network(self):
        net = InterNetwork()

        bgp_cluster = BGPCluster()
        sdn_cluster = SDNCluster()
        for i in range(1, 6):
            if i is 1 or i is 5:
                net.add_as(i, bgp_cluster)
            else:
                net.add_as(i, sdn_cluster)


        net.add_peering(1, 2, delay=DEFAULT_DELAY)
        net.add_peering(2, 3, delay=DEFAULT_DELAY)
        net.add_peering(3, 4, delay=DEFAULT_DELAY)

        net.add_peering(4, 5, delay=DEFAULT_DELAY)
        net.add_prefix(1, add_host=True)
        net.add_prefix(5, add_host=True, track=True)

        return net
