from nwsetup import NetworkSetupAbstract
from siren.network.selector import ASRelationship, UpstreamASSelector, MajorASSelector
from siren.network.topo import ASTopology
from siren.common.path import get_data_path


class NetworkSetup(NetworkSetupAbstract):

    def __init__(self, config_str):
        super(NetworkSetup, self).__init__(config_str)


    def get_network(self):
        as_a, as_b = 3356, 13030  # Level 3, Init7

        as_a, as_b = 49171, 40127

        as_rel = ASRelationship(get_data_path('20131101.as-rel.txt'))
        as_a, as_b = 30985, 40127  # Mali, Boston Children's Hospital
        as_rel.add_selector(UpstreamASSelector(as_a))
        as_rel.add_selector(UpstreamASSelector(as_b))
        as_graph = as_rel.get_graph()

        as_topology = ASTopology(get_data_path('inter_pop_links-20131214.txt'), as_graph, policy=False)
        net = as_topology.get_network()
        net.add_prefix(as_a, add_host=True)
        net.add_prefix(as_b, add_host=True, track=True)
        return net