from siren.network.cluster import BGPCluster, SDNCluster
from siren.network.internet import InterNetwork
from nwsetup import NetworkSetupAbstract, ExperimentAbstract
from siren.network.peering import Peering

DEFAULT_DELAY = 24 #ms
DEFAULT_RELATIONSHIP = 'uc' #unconstrained

class NetworkSetup(NetworkSetupAbstract):

    def __init__(self, config_str):
        super(NetworkSetup, self).__init__(config_str)

    def get_network(self):
        sdn_members = set([1,2,3,6,7])
        bgp_members = set([4,5])

        net = InterNetwork()
        sdn = SDNCluster()
        bgp = BGPCluster()

        for i in sdn_members:
            net.add_as(i, sdn)
        for i in bgp_members:
            net.add_as(i, bgp)

        net.add_peering(1, 2, delay=DEFAULT_DELAY, relationship='p2c')
        net.add_peering(1, 3, delay=DEFAULT_DELAY, relationship='p2c')
        net.add_peering(2, 3, delay=DEFAULT_DELAY, relationship='p2p')
        net.add_peering(2, 4, delay=DEFAULT_DELAY, relationship='p2c')
        net.add_peering(3, 5, delay=DEFAULT_DELAY, relationship='p2c')
        net.add_peering(4, 5, delay=DEFAULT_DELAY, relationship='p2p')
        net.add_peering(4, 6, delay=DEFAULT_DELAY, relationship='p2c')
        net.add_peering(5, 7, delay=DEFAULT_DELAY, relationship='p2c')
        net.add_peering(6, 7, delay=DEFAULT_DELAY, relationship='p2p')

        net.add_prefix(4, add_host=True, track=True)
        for i in sdn_members:
            net.add_prefix(i, add_host=True)
        for i in bgp_members-set([4]):
            net.add_prefix(i, add_host=True)

        return net