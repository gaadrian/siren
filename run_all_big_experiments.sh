#!/bin/bash

#ExpGroup='MRAI0'
ExpGroup='MRAI30'

for i in $(seq 21 40); #elsa 4

do

#withdrawals with 1 backup upstream (2-homed)

#clique
./kill.sh
sleep 5
./bin/siren-network clique --group $ExpGroup --experiments wd --run $i --config n-32,sp-0
sleep 5
./kill.sh
sleep 5
./bin/siren-network clique --group $ExpGroup --experiments wd --run $i --config n-32,sp-25
sleep 5
./kill.sh
sleep 5
./bin/siren-network clique --group $ExpGroup --experiments wd --run $i --config n-32,sp-50
sleep 5
./kill.sh
sleep 5
./bin/siren-network clique --group $ExpGroup --experiments wd --run $i --config n-32,sp-75
sleep 5
./kill.sh
sleep 5


#erdos-renyi graph with probability p=0.5
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-erdos_renyi,gp-0.5,n-32,sp-0
sleep 5
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-erdos_renyi,gp-0.5,n-32,sp-25
sleep 5
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-erdos_renyi,gp-0.5,n-32,sp-50
sleep 5
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-erdos_renyi,gp-0.5,n-32,sp-75
sleep 5
./kill.sh
sleep 5


#newman watts strogatz with k=1/4 node number, p=0.5
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-newman_watts_strogatz,gp-8_0.5,n-32,sp-0
sleep 5
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-newman_watts_strogatz,gp-8_0.5,n-32,sp-25
sleep 5
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-newman_watts_strogatz,gp-8_0.5,n-32,sp-50
sleep 5
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-newman_watts_strogatz,gp-8_0.5,n-32,sp-75
sleep 5
./kill.sh
sleep 5


#barabasi albert preferential attachment model, m=1/4 num of nodes
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-barabasi_albert,gp-8,n-32,sp-0
sleep 5
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-barabasi_albert,gp-8,n-32,sp-25
sleep 5
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-barabasi_albert,gp-8,n-32,sp-50
sleep 5
./kill.sh
sleep 5
./bin/siren-network random_graphs --group $ExpGroup --experiments wd --run $i --config gt-barabasi_albert,gp-8,n-32,sp-75
sleep 5
./kill.sh
sleep 5


done