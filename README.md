#SIREN - SDN Inter-domain Routing EmulatioN

Copyright 2014 Adrian Gaemperli, Vasileios Kotronis

SIREN is a hybrid SDN BGP Inter-domain Routing Emulator. Some of its features are:

- Based on Mininet, Quagga, POX, and ExaBGP
- Automated configuration generation
- Experiment manager
- Live visualization of routing changes
- Log collection and analysis
- Packet loss measurements between end-points


##Installation

**ATTENTION**: Due to a compatibility problem with OVS and newer Ubuntu kernel versions (including the 14.04 release),
please use the following VM instead of installing SIREN from source:

[https://www.dropbox.com/s/w4mkotxiahjitm8/SDN-VM_32bit.ova?dl=0]

If the link is not working for you please contact [vkotronis<at>ics<dot>forth<dot>gr].

**Install from source (ignore for now)**
SIREN is tested on Ubuntu 14.04. It is recommended to run it on a newly installed instance,
using sudo rights.

Download the code and then run
```
./install.sh
```
This installs all dependencies of SIREN. Pay attention that SIREN installs a modified version of Mininet.

##General use
There are 3 different modes how you can use SIREN:

1. Command Line Interface (CLI) like in Mininet
2. Live visualization: Web interface for live interaction with the network
3. Experiment manager

Demo youtube video: https://www.youtube.com/watch?v=Cbc8XlIp_C0&feature=youtu.be

Commands you need to run emulations are all in the directory `bin`.

However, before you want to start the emulation you probably want to define your own topology. The topologies are stored
in the directory `nwsetup`.

Some notes:
An AS cluster is a group of multiple ASes.
In general you do not have to worry about IP addresses, they are assigned automatically.

###Topology definitions
To build artificial topologies it is easiest to look at some of the examples in the `nwsetup` directory.

A short example
```python
# the class has to have the name NetworkSetup and has to inherit from NetworkSetupAbstract
class NetworkSetup(NetworkSetupAbstract):
    def get_network(self):
        sdn = SDNCluster()  # create a SDN cluster
        bgp = BGPCluster()  # create a group of BGP routers
        net = InterNetwork()

        net.add_as(1, sdn)  # add a SDN AS with ASN 1
        net.add_as(2, bgp)  # add a BGP AS with ASN 2
        net.add_as(3, bgp)  # add a BGP AS with ASN 3
        net.add_as(4, bgp)  # add a BGP AS with ASN 3

        # add peering links, the named arguments are optional
        net.add_peering(1, 2, delay=24)
        net.add_peering(2, 3, delay=20, relationship=Peering.REL_CUSTOMER_UPSTREAM)
        net.add_peering(2, 4)
        net.add_peering(3, 4)
        
        # add prefixes to ASes, you can 
        net.add_prefix(1)  # add a prefix to AS 1
        net.add_prefix(2, add_host=True)  # add_host: add a prefix to AS 2 and add host attached to AS 2 with an IP within the prefix
        net.add_prefix(3, add_host=True, track=True)  # track: in live visualization track this prefix
        return net
```

The name of the topology is the filename. So let's assume we saved this class in `nwsetup/docexample.py` therefore the name is `docexample`.
It is also possible to use a configuration string. For that please see other examples.

Generally all routers (BGP and SDN) are called r[ASN].0, the same schema applies also for hosts where it is h[ASN].0. The log files are stored
in your home directory.

### Command Line Interface
The CLI is very similar to Mininet, so you are probably already familiar with that. You need to use the `bin/siren-network` command.
Possible arguments:  `-c`: configuration of the topology, `-g`: group name, use to group log files, `-r`: name of run, use to run an
experiment multiple times, `-e`: name experiment, which is run automatically (Omit if you want to use the command line interface)

Example:

```
./siren-network docexample
```

###Experiment manager
The experiment manager can be used to run experiments in parallel on different servers. However each experiment is still
run on a single instance and does NOT support that the network spans across multiple instances. To use the manager
start some servers and add your SSH public key to the servers root account. Make sure you connect to all
servers at once so that all fingerprints are known to the ssh command (e.g. do this by connecting to all servers manually).

The **main command** for the manager is `bin/siren-manager`. The mentioned arguments below use this command.

####Preparation
#####Servers
Copy all hostnames (or IP addresses) to a text file with a server one each line. This file is called serverfile.

Example:
```
host1.example.com
host2.example.com
host3.example.com
```
#####Experiment definitions
Experiments are defined in the same file as the topology, so go to your topology file in the directory `nwsetup`.

Experiment example:

```python
# Experiments need to inherit from ExperimentAbstract
class SimpleExperiment(ExperimentAbstract):

    def do(self, tb):
        # start measuring loss between h2.0 and h3.0 and vice versa
        m23 = tb.measure_start('h2.0', 'h3.0')
        m32 = tb.measure_start('h3.0', 'h2.0')

        tb.cli_cmd('wait 10')  # wait a couple of seconds
        tb.cli_cmd('marker convergence_start')  # set marker for easier analysis
        tb.cli_cmd('link r2.0 r3.0 down')  # bring down the link
        tb.cli_cmd('wait 10')
        tb.cli_cmd('waitconverged')  # wait until the network converged
        tb.cli_cmd('marker convergence_end')

        # stop measuring
        tb.measure_stop(m23)
        tb.measure_stop(m32)
```
Measurements only work if a connection is possible at the beginning and the end of the measurement. The Experiment then needs to be registred
in the topology init method.

```python
    def __init__(self, config_str):
        super(NetworkSetup, self).__init__(config_str)
        self.add_experiment('break23', SimpleExperiment())  # register SimpleExperiment with the name 'break23'
```

We now assume that our topology is still called `docexample`.
We also have to define which experiments have to run. This can be achieved by writing all arguments of `siren-network` to a file
with a line per different run.

Example experiment file:

```
docexample -g readmeexample -r run0 -e break23
docexample -g readmeexample -r run1 -e break23
docexample -g readmeexample -r run2 -e break23
docexample -g readmeexample -r run3 -e break23
docexample -g readmeexample -r run4 -e break23
docexample -g readmeexample -r run5 -e break23
```

####Installation
To install the SIREN framework on all servers use the arguments `install [pathtoserverfile]`. The local installation of
SIREN is used for the installation on the servers, therefore any changes you made to SIREN is also available on the
servers.

####Synchronize files
To sync the files between your local installation and the servers, you can use the arguments
`sync-files [pathtoserverfile]`. This can e.g. be used to sync new network setups to all the servers. This synchronization
is only one-way from your local installation to the servers. No log files are collected.


####Run experiments
To run the experiments use the arguments `run [pathtoserverfile] -e [pathtoexperimentfile]`

####See status
To poll the status of the running experiments use the arguments `status [pathtoserverfile]`.

####Collect results
To collect the results from the servers to your local installation use the arguments `collect [pathtoserverfile]`. Remember that
log files are stored in your home directory.

### Live Visualization
You can use the visualization by starting `bin/sc-network` with `-v`. This starts a websocket server. After the CLI
stops you can use the HTML file `public/vis.html` to connect to the websocket server. After you connected you need to
press Enter in the CLI.


#### Virtualbox Snapshots
To use VIrtualbox Snapshots of convergered networks you need to change 2 settings:

- /etc/init.d/virtualbox-guest-utils stop
- Time settings: Manual time (not automatically from the Internet)

More documentation:

- A. Gaemperli. "Evaluating the Effect of SDN Centralization on Internet Routing Convergence". Master�s thesis, ETH
Z�rich, 2014.

- A. Gaemperli, V. Kotronis, and X. Dimitropoulos. "Evaluating the Effect of Centralization on Routing Convergence on
a Hybrid BGP-SDN Emulation Framework". In Demo Session of ACM SIGCOMM, 2014.

- V. Kotronis, A. Gaemperli, and X. Dimitropoulos. "Routing centralization across domains via SDN: A model and emulation framework for BGP evolution". In Elsevier Computer Networks, 2015.

Copyright 2014 Adrian Gaemperli, Vasileios Kotronis
