#!/bin/bash

NumofRuns=20
#ExpGroup='MRAI0'
ExpGroup='MRAI30'
#MRAI='0'
MRAI='30'

#all convergence time

echo "Plotting the convergence time for the 32-node clique, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup clique --group $ExpGroup --experiment wd  --rc $NumofRuns --config n-32 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the convergence time for the 32-node Erdos-Renyi, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-erdos_renyi,gp-0.5,n-32 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the convergence time for the 32-node Newman-Watts-Strogatz, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-newman_watts_strogatz,gp-8_0.5,n-32 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the convergence time for the 32-node Barabasi-Albert, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-barabasi_albert,gp-8,n-32 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable


#all churn rates

echo "Plotting the churn rate for the 32-node clique, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue churn --sp --setup clique --group $ExpGroup --experiment wd  --rc $NumofRuns --config n-32 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the churn rate for the 32-node Erdos-Renyi, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue churn --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-erdos_renyi,gp-0.5,n-32 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the churn rate for the 32-node Newman-Watts-Strogatz, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue churn --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-newman_watts_strogatz,gp-8_0.5,n-32 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the churn rate for the 32-node Barabasi-Albert, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue churn --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-barabasi_albert,gp-8,n-32 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable