import sys
import os
import logging


logging.basicConfig(level=logging.ERROR, filename='/tmp/siren_exceptions')


def add_project_path():
    common_directory = os.path.dirname(os.path.realpath(__file__))
    path = os.path.realpath(os.path.join(common_directory, '..'))
    sys.path.append(path)

add_project_path()