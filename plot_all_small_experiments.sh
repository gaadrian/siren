#!/bin/bash

NumofRuns=40
#ExpGroup='MRAI0'
ExpGroup='MRAI30'
#MRAI='0'
MRAI='30'

#all convergence times

echo "Plotting the convergence time for the 8-node clique, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup clique --group $ExpGroup --experiment wd  --rc $NumofRuns --config n-8 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the convergence time for the 16-node clique, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup clique --group $ExpGroup --experiment wd  --rc $NumofRuns --config n-16 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the convergence time for the 8-node Erdos-Renyi, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-erdos_renyi,gp-0.5,n-8 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the convergence time for the 16-node Erdos-Renyi, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-erdos_renyi,gp-0.5,n-16 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the convergence time for the 8-node Newman-Watts-Strogatz, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-newman_watts_strogatz,gp-2_0.5,n-8 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the convergence time for the 16-node Newman-Watts-Strogatz, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-newman_watts_strogatz,gp-4_0.5,n-16 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the convergence time for the 8-node Barabasi-Albert, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-barabasi_albert,gp-2,n-8 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the convergence time for the 16-node Barabasi-Albert, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue time --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-barabasi_albert,gp-4,n-16 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable


#all churn rates

echo "Plotting the churn rate for the 8-node clique, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence  -r --yvalue churn --sp --setup clique --group $ExpGroup --experiment wd  --rc $NumofRuns --config n-8 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the churn rate for the 16-node clique, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue churn --sp --setup clique --group $ExpGroup --experiment wd  --rc $NumofRuns --config n-16 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the churn rate for the 8-node Erdos-Renyi, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue churn --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-erdos_renyi,gp-0.5,n-8 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the churn rate for the 16-node Erdos-Renyi, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue churn --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-erdos_renyi,gp-0.5,n-16 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the churn rate for the 8-node Newman-Watts-Strogatz, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue churn --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-newman_watts_strogatz,gp-2_0.5,n-8 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the churn rate for the 16-node Newman-Watts-Strogatz, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue churn --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-newman_watts_strogatz,gp-4_0.5,n-16 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the churn rate for the 8-node Barabasi-Albert, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue churn --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-barabasi_albert,gp-2,n-8 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable

echo "Plotting the churn rate for the 16-node Barabasi-Albert, MRAI=$MRAI, fail-over experiment"
./bin/siren-convergence -r --yvalue churn --sp --setup random_graphs --group $ExpGroup --experiment wd  --rc $NumofRuns --config gt-barabasi_albert,gp-4,n-16 --prefix 8.156.64.0/29 --write
echo "Press any key to continue"
read input_variable